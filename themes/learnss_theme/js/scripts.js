(function ($) {
  console.log('Initialize JS (scripts.js)');

  // Pagination AJAX
  if ($('.pagination').length) {

    // pagination function
    var paginate = function (url) {

      // modify returned url to prevent caching issues (Lesson 17)
      var param = '&ajax=1',
          ajaxUrl = (url.indexOf(param) === -1) ?
              url + '&ajax=1' :
              url,
          cleanUrl = url.replace(new RegExp(param+'$'),''); // provides url without '&ajax=1'

      $.ajax(ajaxUrl)
        .done(function (response) {
          $('.main').html(response);
          $('html, body').animate({
            scrollTop: $('.main').offset().top
          });
          window.history.pushState(
              {url: cleanUrl},
              document.title,
              cleanUrl
          );
        })
        .fail(function (xhr) {
          alert('Error: ' + xhr.responseText);
        });
    };

    // Call pagination ajax on click event
    $('.main').on('click', '.pagination a', function (e) {
      e.preventDefault();
      var url = $(this).attr('href');
      paginate(url);
    });

    // restore pagination position (history) if reloading page
    window.onpopstate = function (e) {
      if (e.state.url) {
        paginate(e.state.url);
      } else {
        e.preventDefault();
      }
    }
  }
})(jQuery);
