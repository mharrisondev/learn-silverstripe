<?php

namespace {

  use Page;
  use SilverStripe\AssetAdmin\Forms\UploadField;
  use SilverStripe\Assets\File;
  use SilverStripe\Assets\Image;
  use SilverStripe\Dev\Debug;
  use SilverStripe\Forms\CheckboxSetField;
  use SilverStripe\Forms\DateField;
  use SilverStripe\Forms\TextareaField;
  use SilverStripe\Forms\TextField;
  use SilverStripe\Forms\DropdownField;

  class ArticlePage extends Page
  {
    private static $can_be_root = false;
    private static $allowed_children = [];

    private static $db = [ /* maps a field name to a field type */
      'Date' => 'Date',
      'Teaser' => 'Text',
      'Author' => 'Varchar', /* Varchar default is limited to 100 characters */
    ];

    private static $has_one = [
      'Photo' => Image::class,
      'Attachment' => File::class,
      'Region' => Region::class,
    ];

    private static $has_many = [
      'Comments' => ArticleComment::class
    ];

    private static $many_many = [
      'Categories' => ArticleCategory::class
    ];

    /* $owns when the page gets published, it will also publish associated files simultaneously */
    private static $owns = [
      'Photo',
      'Attachment'
    ];

    public function getCMSFields()
    {
      $fields = parent::getCMSFields();
      $fields->addFieldsToTab('Root.Main', array(
        DateField::create('Date', 'Date of article'),
        TextareaField::create('Teaser')
          ->setDescription('This is the summary that appears on the article list page.'),
        TextField::create('Author', 'Author of article')
      ), 'Metadata');
      /* Third param of addFieldsToTab implies which field to add before, in this case 'Metadata' */

      $fields->addFieldToTab('Root.Attachments', $photo = UploadField::create('Photo'));
      $fields->addFieldToTab('Root.Attachments', $fileAttachment = UploadField::create('Attachment', 'Optional attachment (PDF only)'));
      $photo->setFolderName('photos');
      $fileAttachment
        ->setFolderName('file-attachments')
        ->getValidator()->setAllowedExtensions(['pdf']);

      $fields->addFieldToTab('Root.Categories', CheckboxSetField::create(
        'Categories',
        'Selected categories',
        $this->Parent()->Categories()->map('ID', 'Title')
      ));

      $fields->addFieldToTab('Root.Main', DropdownField::create(
        'RegionID',
        'Region',
        Region::get()->map('ID', 'Title')
      )->setEmptyString('-- None --'), 'Content');

      return $fields;
    }

    public function CategoriesList()
    {

      if ($this->Categories()->exists()) {
        return implode(', ', $this->Categories()->column('Title'));
      }

      return null;
    }

  }
}
