<?php

namespace {

    use SilverStripe\ORM\DataObject;
    use SilverStripe\Assets\Image;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\TextareaField;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Versioned\Versioned;
    use SilverStripe\Control\Controller;

    class Region extends DataObject
    {

        private static $db = [
            'Title' => 'Varchar',
            'Description' => 'HTMLText',
        ];

        private static $has_one = [
            'Photo' => Image::class,
            'RegionsPage' => RegionsPage::class
        ];

        private static $has_many = [
          'Articles' => ArticlePage::class,
        ];

        private static $owns = [
            'Photo',
        ];

        private static $extensions = [
            Versioned::class /* Allows content versioning & drafts for content */
        ];

        private static $summary_fields = [
            'GridThumbnail' => '',
            /*'Photo' => '', replaced by GridThumbnail*/
            'Title' => 'Title of region',
            'Description' => 'Short description'
        ];

        private static $versioned_gridfield_extensions = true;

        public function getGridThumbnail()
        {
            if ($this->Photo()->exists()) {
                return $this->Photo()->ScaleWidth(100);
                /* Alternatively, could use CMSThumbnail() or Photo.CMSThumbnail; however this would not allow the fallback text */
            }

            return '(No image)';
        }

        public function Link()
        {
            return $this->RegionsPage()->Link('show/' . $this->ID);
        }


        /* By default, missing the "current" state for the region. That's because the method $LinkingMode doesn't exist on a DataObject by default, so we need to write our own. */
        public function LinkingMode() {
            return Controller::curr()->getRequest()->param('ID') == $this->ID ? 'active' : 'link';
        }

        public function ArticlesLink() {
          $page = ArticleHolder::get()->first();

          if ($page) {
            return $page->Link('region/' . $this->ID);
          }
        }

        public function getCMSFields()
        {
            $fields = FieldList::create(
                TextField::create('Title'),
                HTMLEditorField::create('Description'),
                $uploader = UploadField::create('Photo')
            );

            $uploader->setFolderName('region-photos');
            $uploader->getValidator()->setAllowedExtensions(['png', 'gif', 'jpeg', 'jpg']);

            return $fields;
        }
    }
}

