<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DropdownField;
Use SilverStripe\Forms\DateField;
use SilverStripe\Forms\CurrencyField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\ORM\ArrayLib;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\TabSet;
use SilverStripe\Versioned\Versioned;

class Property extends DataObject
{

    private static $db = [
        'Title' => 'Varchar',
        'PricePerNight' => 'Currency',
        'Bedrooms' => 'Int',
        'Bathrooms' => 'Int',
        'FeaturedOnHomepage' => 'Boolean',
        'Description' => 'Text',
        'AvailableStart' => 'Date',
        'AvailableEnd' => 'Date'
    ];

    private static $has_one = [
        'Region' => Region::class,
        'PrimaryPhoto' => Image::class
    ];

    // Ensure photo gets published when property published
    private static $owns = [
        'PrimaryPhoto',
    ];

    // Enable 'draft' state for properties
    private static $extensions = [
        Versioned::class,
    ];

    private static $versioned_gridfield_extensions = true;

    // Customize CMS summary display table
    private static $summary_fields = [
        'Title' => 'Title',
        'Region.Title' => 'Region',
        'PricePerNight.Nice' => 'Price',
        'FeaturedOnHomepage.Nice' => 'Homepage?'
    ];

    // Out of the box Search Field Inputs
    //private static $searchable_fields = [
    //    'Title',
    //    'Region.Title',
    //    'FeaturedOnHomepage'
    //];


    /* TODO - Not covered in Lessons */
    public function Link() {
      return 'ArbitraryString';
    }

    // Customized searchable fields
    public function searchableFields()
    {
        return [
            'Title' => [
                'filter' => 'PartialMatchFilter',
                'title' => 'Title',
                'field' => TextField::class,
            ],
            'RegionID' => [
                'filter' => 'ExactMatchFilter',
                'title' => 'Region',
                'field' => DropdownField::create('RegionID')
                    ->setSource(
                        Region::get()->map('ID', 'Title')
                    )
                    ->setEmptyString('-- Any region --')
            ],
            'FeaturedOnHomepage' => [
                'filter' => 'ExactMatchFilter',
                'title' => 'Only featured'
            ]
        ];
    }

    public function getCMSfields()
    {
        $fields = FieldList::create(TabSet::create('Root'));
        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title'),
            TextField::create('Description'),
            CurrencyField::create('PricePerNight', 'Price (per night)'),
            DropdownField::create('Bedrooms')
                ->setSource(ArrayLib::valuekey(range(1, 10))),
            DropdownField::create('Bathrooms')
                ->setSource(ArrayLib::valuekey(range(1, 10))),
            DropdownField::create('RegionID', 'Region')
                ->setSource(Region::get()->map('ID', 'Title'))
                ->setEmptyString('Not specified'),
            CheckboxField::create('FeaturedOnHomepage', 'Featured on homepage?'),
            DateField::create('AvailableStart', 'Date available (start date)'),
            DateField::create('AvailableEnd', 'Date available (end date)')
        ]);

        $fields->addFieldToTab('Root.Photos',
            $upload = UploadField::create('PrimaryPhoto', 'Primary Photo')
        );

        $upload->getValidator()->setAllowedExtensions(array(
            'png', 'jpeg', 'jpg', 'gif'
        ));

        $upload->setFolderName('property-photos');

        return $fields;
    }

}
