<?php

namespace {

  use Page;
  use SilverStripe\Forms\GridField\GridField;
  use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

  class RegionsPage extends Page {

    private static $has_many = [
      'Regions' => Region::class
    ];

    private static $owns = [
      'Regions'
      /*  declare ownership between RegionsPage and Region so that changes to
      publication cascades from parent to child, and vice-versa. */
    ];

    public function getCMSFields() {
      $fields = parent::getCMSFields();
      $fields->addFieldToTab('Root.Regions', GridField::create(
        'Regions',         /* Arbitrary name for the gridfield */
        'Regions on this page',   /* User friendly title for gridfield */
        $this->Regions(),         /* Populates GridField with data associated with the page */
        GridFieldConfig_RecordEditor::create() /* provides all the basic UI you'd expect to have for managing data */
      ));

      return $fields;
    }
  }
}