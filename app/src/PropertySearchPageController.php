<?php

use PageController;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\DateField;
use SilverStripe\ORM\ArrayLib;
use SilverStripe\ORM\PaginatedList;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\HTTP;

class PropertySearchPageController extends PageController
{

  /* Every controller fires the index action by default, so it is not necessary to add this to the $allowed_actions array. */
  public function index(HTTPRequest $request)
  {
    $properties = Property::get();
    $activeFilters = ArrayList::create();

    if ($search = $request->getVar('Keywords')) {
      $properties = $properties->filter([
        'Title:PartialMatch' => $search
      ]);

      $activeFilters->push(ArrayData::create([
        'Label' => "Keywords: '$search'",
        'RemoveLink' => HTTP::setGetVar('Keywords', null, null, '&')
      ]));
    }

    $filters = [
      ['Bedrooms', 'Bedrooms', 'GreaterThanOrEqual', '%s bedrooms'],
      ['Bathrooms', 'Bathrooms', 'GreaterThanOrEqual', '%s bathrooms'],
      ['MinPrice', 'PricePerNight', 'GreaterThanOrEqual', 'Min. $%s'],
      ['MaxPrice', 'PricePerNight', 'LessThanOrEqual', 'Max. $%s'],
    ];

    foreach ($filters as $filterKeys) {
      list($getVar, $field, $filter, $labelTemplate) = $filterKeys;
      if ($value = $request->getVar($getVar)) {
        $activeFilters->push(ArrayData::create([
          'Label' => sprintf($labelTemplate, $value),
          'RemoveLink' => HTTP::setGetVar($getVar, null, null, '&'),
        ]));

        $properties = $properties->filter([
          "{$field}:{$filter}" => $value
        ]);
      }
    }

    if ($arrival = $request->getVar('ArrivalDate')) {
      $arrivalStamp = strtotime($arrival);
      $nightAdder = '+' . $request->getVar('Nights') . ' days';
      $startDate = date('Y-m-d', $arrivalStamp);
      $endDate = date('Y-m-d', strtotime($nightAdder, $arrivalStamp));

      $properties = $properties->filter([
        'AvailableStart:LessThanOrEqual' => $startDate,
        'AvailableEnd:GreaterThanOrEqual' => $endDate
      ]);
    }

    $filters = [
      ['Bedrooms', 'Bedrooms', 'GreaterThanOrEqual'],
      ['Bathrooms', 'Bathrooms', 'GreaterThanOrEqual'],
      ['MinPrice', 'PricePerNight', 'GreaterThanOrEqual'],
      ['MaxPrice', 'PricePerNight', 'LessThanOrEqual'],
    ];

    foreach ($filters as $filterKeys) {
      list($getVar, $field, $filter) = $filterKeys;
      if ($value = $request->getVar($getVar)) {
        $properties = $properties->filter([
          "{$field}:{$filter}" => $value
        ]);
      }
    }

    $paginatedProperties = PaginatedList::create(
      $properties,
      $request
    )->setPageLength(2)
      //customize get var for pagination
      ->setPaginationGetVar('p');

    $AjaxData = array(
      'Results' => $paginatedProperties,
    );

    if ($request->isAjax()) {
      return $this->customise($AjaxData)
        ->renderWith('Includes/PropertySearchResults');
    }

    return array(
      'Results' => $paginatedProperties,
      'ActiveFilters' => $activeFilters
    );
  }

  public function PropertySearchForm()
  {
    $nights = [];
    foreach (range(1, 14) as $i) {
      $nights[$i] = "$i night" . (($i > 1) ? 's' : '');
    }
    $prices = [];
    foreach (range(100, 1000, 50) as $i) {
      $prices[$i] = '$' . $i;
    }

    $form = Form::create(
      $this,
      'PropertySearchForm',
      FieldList::create(
        TextField::create('Keywords')
          ->setAttribute('placeholder', 'City, State, Country, etc...')
          ->addExtraClass('form-control'),
        DateField::create('ArrivalDate', 'Arrive on...')
          ->setAttribute('data-datepicker', true)
          ->setAttribute('data-date-format', 'DD-MM-YYYY')
          ->addExtraClass('form-control'),
        DropdownField::create('Nights', 'Stay for...')
          ->setSource($nights)
          ->addExtraClass('form-control'),
        DropdownField::create('Bedrooms')
          ->setSource(ArrayLib::valuekey(range(1, 5)))
          ->addExtraClass('form-control'),
        DropdownField::create('Bathrooms')
          ->setSource(ArrayLib::valuekey(range(1, 5)))
          ->addExtraClass('form-control'),
        DropdownField::create('MinPrice', 'Min. price')
          ->setEmptyString('-- any --')
          ->setSource($prices)
          ->addExtraClass('form-control'),
        DropdownField::create('MaxPrice', 'Max. price')
          ->setEmptyString('-- any --')
          ->setSource($prices)
          ->addExtraClass('form-control')
      ),
      FieldList::create(
        FormAction::create('doPropertySearch', 'Search')
          ->addExtraClass('btn-lg btn-fullcolor')
      )
    );


    /* By default, forms submit through the POST method, which works great for handling user input that mutates
    data on the backend, but all we really want from our form here is a glorified URL builder. All the form
    really has to do is redirect us off to a URL that passes all of its parameters into a query string (GET request),
    and allow the controller to take it from there. Therefore, for this form, we'll use the GET method.  */
    $form->setFormMethod('GET')
      ->setFormAction($this->Link())
      ->disableSecurityToken()
      // populate form input values with get request var values, as request params and inputs share the same name
      ->loadDataFrom($this->request->getVars());

    return $form;

  }
}
