<?php

namespace {

  use PageController;
  use SilverStripe\Forms\EmailField;
  use SilverStripe\Forms\FieldList;
  use SilverStripe\Forms\Form;
  use SilverStripe\Forms\FormAction;
  use SilverStripe\Forms\RequiredFields;
  use SilverStripe\Forms\TextareaField;
  use SilverStripe\Forms\TextField;

  class ArticlePageController extends PageController {

    private static $allowed_actions = [
      'CommentForm'
    ];

    private static $db = [
        'Name' => 'Varchar',
        'Email' => 'Varchar',
        'Comment' => 'Text'
    ];

    private static $has_one = [
        'ArticlePage' => ArticlePage::class
    ];

    public function CommentForm() {
      $form = Form::create(
        $this, // 1st Arg: which Controller
        __FUNCTION__, /* 2nd Arg: A string representing the name of the method that creates the form.
        Because this argument always describes the name of the function we're in, you can simply use the PHP constant __FUNCTION__ here */
        FieldList::create(
          TextField::create('Name', ''),
          EmailField::create('Email', ''),
          TextareaField::create('Comment', '')
        ),
        FieldList::create(
          FormAction::create('handleComment', 'Post Comment')
            ->setUseButtonTag(true)
            ->addExtraClass('btn btn-primary')
        ),
        RequiredFields::create('Name', 'Email', 'Comment')
      )->setTemplate('custom_Form');

      foreach($form->Fields() as $field) {
          $field->addExtraClass('form-control')->setAttribute('placeholder', $field->getName().'*');
      }

      // fetch data from session if form failed validation/submission
        $data = $this->getRequest()->getSession()->get("FormData.{$form->getName()}.data");

      return $data ? $form->loadDataFrom($data) : $form;
    }

    public function handleComment($data, $form) {
        // preserve comment on submit incase it fails validation
        $session = $this->getRequest()->getSession();
        $session->set("FormData.{$form->getName()}.data", $data);

        $existing = $this->Comments()->filter([
            'Comment' => $data['Comment']
        ]);

        if ($existing->exists() && strlen($data['Comment']) > 20) {
            $form->sessionMessage('That comment already exists!', 'text-danger');

            return $this->redirectBack();
        }


        // Setup comment data and save
      $comment = ArticleComment::create();
      $comment->ArticlePageID = $this->ID;
      $form->saveInto($comment); // replaces manually defining each field to save (eg. $comment->Comment = $data['Comment'];)
      $comment->write();

      $session->clear("FormData.{$form->getName()}.data");
      $form->sessionMessage('Thanks for your comment!','text-success');

      return $this->redirectBack();
    }

  }
}
