<?php

namespace {

    use PageController;
    use SilverStripe\Control\HTTPRequest;
    use SilverStripe\Dev\Debug;

    class RegionsPageController extends PageController
    {

        //Allow action(s) to be invoked via URL
        private static $allowed_actions = [
            'show'
        ];

        public function show(HTTPRequest $request)
        {
            /*
            A controller action will try to render a template following the naming
            convention [PageType]_[actionName].ss. In our case, that gives us RegionsPage_show.ss. Let's create that template.
            */

            $region = Region::get()->byID($request->param('ID'));

            if (!$region) {
                return $this->httpError(404, 'That region could not be found');
            }

            return [
                'Region' => $region,
                'Title' => $region->Title
            ];
        }
    }
}
