-- MySQL dump 10.16  Distrib 10.1.35-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: learnss
-- ------------------------------------------------------
-- Server version	10.1.35-MariaDB-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ArticleCategory`
--

DROP TABLE IF EXISTS `ArticleCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ArticleCategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('ArticleCategory') CHARACTER SET utf8 DEFAULT 'ArticleCategory',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ArticleHolderID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `ArticleHolderID` (`ArticleHolderID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArticleCategory`
--

LOCK TABLES `ArticleCategory` WRITE;
/*!40000 ALTER TABLE `ArticleCategory` DISABLE KEYS */;
INSERT INTO `ArticleCategory` VALUES (1,'ArticleCategory','2018-10-12 13:58:00','2018-10-12 13:57:46','Beautiful',8),(2,'ArticleCategory','2018-10-12 13:58:14','2018-10-12 13:58:14','Exiciting',8),(3,'ArticleCategory','2018-10-12 13:58:36','2018-10-12 13:58:36','Interesting',8);
/*!40000 ALTER TABLE `ArticleCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ArticleComment`
--

DROP TABLE IF EXISTS `ArticleComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ArticleComment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('ArticleComment') CHARACTER SET utf8 DEFAULT 'ArticleComment',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Comment` mediumtext CHARACTER SET utf8,
  `ArticlePageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `ArticlePageID` (`ArticlePageID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArticleComment`
--

LOCK TABLES `ArticleComment` WRITE;
/*!40000 ALTER TABLE `ArticleComment` DISABLE KEYS */;
INSERT INTO `ArticleComment` VALUES (1,'ArticleComment','2018-10-12 17:24:49','2018-10-12 17:24:49','Joe Smith','joe@example.com','Hey, this is a great read.',9),(2,'ArticleComment','2018-11-26 16:50:25','2018-11-26 16:50:25','Joe Bloggs','joe@bloggs.com','Neato!',9),(3,'ArticleComment','2018-11-26 17:07:01','2018-11-26 17:07:01','Person1','Person1@example.com','This is a long comment to ensure the > 20 character messages get checked for duplicates',9),(4,'ArticleComment','2018-11-26 17:07:19','2018-11-26 17:07:19','Person2','Person2@example.com','This is a long comment to ensure the > 20 character messages get checked for duplicates',9),(5,'ArticleComment','2018-11-26 17:08:32','2018-11-26 17:08:32','Person2','Person2@example.com','This is a long comment to ensure the > 20 character messages get checked for duplicates',9),(6,'ArticleComment','2018-11-26 17:09:07','2018-11-26 17:09:07','Person2','Person2@example.com','This is a long comment to ensure the > 20 character messages get checked for duplicates',9),(7,'ArticleComment','2018-11-26 17:09:30','2018-11-26 17:09:30','Person2','Person2@example.com','This is a long comment to ensure the > 20 character messages get checked for duplicates',9),(8,'ArticleComment','2018-11-26 17:21:00','2018-11-26 17:21:00','asd','asd@asd.com','This is a long comment to ensure the > 20 character messages get checked for duplicates\r\n\r\n',9),(9,'ArticleComment','2018-11-26 17:25:03','2018-11-26 17:25:03','dsa','dsa@dsa.com','New message',9),(10,'ArticleComment','2018-11-29 13:27:23','2018-11-29 13:27:23','asd','asd@asd.com','This is a long comment to ensure the > 20 character messages get checked for duplicates\r\n',9),(11,'ArticleComment','2018-11-29 13:28:03','2018-11-29 13:28:03','asd','asd@asd.com','This is a long comment to ensure the > 20 character messages get checked for duplicates!',9);
/*!40000 ALTER TABLE `ArticleComment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ArticlePage`
--

DROP TABLE IF EXISTS `ArticlePage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ArticlePage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `Teaser` mediumtext CHARACTER SET utf8,
  `Author` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `BrochureID` int(11) NOT NULL DEFAULT '0',
  `AttachmentID` int(11) NOT NULL DEFAULT '0',
  `RegionID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PhotoID` (`PhotoID`),
  KEY `BrochureID` (`BrochureID`),
  KEY `AttachmentID` (`AttachmentID`),
  KEY `RegionID` (`RegionID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArticlePage`
--

LOCK TABLES `ArticlePage` WRITE;
/*!40000 ALTER TABLE `ArticlePage` DISABLE KEYS */;
INSERT INTO `ArticlePage` VALUES (9,'2018-10-11','This is summary preview text for the article within','Matthew',6,7,7,1),(14,'2018-10-11','This is summary preview text for the article within','Matthew',8,0,0,2),(15,'2018-10-11','This is summary preview text for the article within','Matthew',9,0,7,0),(16,'2018-10-11','This is summary preview text for the article within','Matthew',10,0,0,0);
/*!40000 ALTER TABLE `ArticlePage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ArticlePage_Categories`
--

DROP TABLE IF EXISTS `ArticlePage_Categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ArticlePage_Categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ArticlePageID` int(11) NOT NULL DEFAULT '0',
  `ArticleCategoryID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ArticlePageID` (`ArticlePageID`),
  KEY `ArticleCategoryID` (`ArticleCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArticlePage_Categories`
--

LOCK TABLES `ArticlePage_Categories` WRITE;
/*!40000 ALTER TABLE `ArticlePage_Categories` DISABLE KEYS */;
INSERT INTO `ArticlePage_Categories` VALUES (1,9,1),(2,9,3),(3,14,1),(4,14,2),(5,15,3),(6,16,1),(7,16,3);
/*!40000 ALTER TABLE `ArticlePage_Categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ArticlePage_Live`
--

DROP TABLE IF EXISTS `ArticlePage_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ArticlePage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `Teaser` mediumtext CHARACTER SET utf8,
  `Author` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `BrochureID` int(11) NOT NULL DEFAULT '0',
  `AttachmentID` int(11) NOT NULL DEFAULT '0',
  `RegionID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PhotoID` (`PhotoID`),
  KEY `BrochureID` (`BrochureID`),
  KEY `AttachmentID` (`AttachmentID`),
  KEY `RegionID` (`RegionID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArticlePage_Live`
--

LOCK TABLES `ArticlePage_Live` WRITE;
/*!40000 ALTER TABLE `ArticlePage_Live` DISABLE KEYS */;
INSERT INTO `ArticlePage_Live` VALUES (9,'2018-10-11','This is summary preview text for the article within','Matthew',6,7,7,1),(14,'2018-10-11','This is summary preview text for the article within','Matthew',8,0,0,2),(15,'2018-10-11','This is summary preview text for the article within','Matthew',9,0,7,0),(16,'2018-10-11','This is summary preview text for the article within','Matthew',10,0,0,0);
/*!40000 ALTER TABLE `ArticlePage_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ArticlePage_Versions`
--

DROP TABLE IF EXISTS `ArticlePage_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ArticlePage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Date` date DEFAULT NULL,
  `Teaser` mediumtext CHARACTER SET utf8,
  `Author` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `BrochureID` int(11) NOT NULL DEFAULT '0',
  `AttachmentID` int(11) NOT NULL DEFAULT '0',
  `RegionID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `PhotoID` (`PhotoID`),
  KEY `BrochureID` (`BrochureID`),
  KEY `AttachmentID` (`AttachmentID`),
  KEY `RegionID` (`RegionID`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArticlePage_Versions`
--

LOCK TABLES `ArticlePage_Versions` WRITE;
/*!40000 ALTER TABLE `ArticlePage_Versions` DISABLE KEYS */;
INSERT INTO `ArticlePage_Versions` VALUES (1,9,10,'2018-10-11','This is summary preview text for the article within','Matthew',0,0,0,0),(2,9,11,'2018-10-11','This is summary preview text for the article within','Matthew',0,0,0,0),(3,9,12,'2018-10-11','This is summary preview text for the article within','Matthew',2,3,0,0),(4,9,13,'2018-10-11','This is summary preview text for the article within','Matthew',2,3,0,0),(5,9,14,'2018-10-11','This is summary preview text for the article within','Matthew',6,7,0,0),(6,9,15,'2018-10-11','This is summary preview text for the article within','Matthew',6,7,0,0),(7,9,16,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(8,9,17,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(9,14,1,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(10,14,2,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(11,14,3,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(12,15,1,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(13,15,2,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(14,15,3,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(15,16,1,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(16,16,2,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(17,16,3,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(18,14,4,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(19,14,5,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(20,15,4,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(21,15,5,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(22,16,4,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(23,16,5,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,0),(24,14,6,'2018-10-11','This is summary preview text for the article within','Matthew',8,0,7,0),(25,14,7,'2018-10-11','This is summary preview text for the article within','Matthew',8,0,7,0),(26,15,6,'2018-10-11','This is summary preview text for the article within','Matthew',9,0,7,0),(27,15,7,'2018-10-11','This is summary preview text for the article within','Matthew',9,0,7,0),(28,16,6,'2018-10-11','This is summary preview text for the article within','Matthew',10,0,7,0),(29,16,7,'2018-10-11','This is summary preview text for the article within','Matthew',10,0,7,0),(30,16,8,'2018-10-11','This is summary preview text for the article within','Matthew',10,0,0,0),(31,16,9,'2018-10-11','This is summary preview text for the article within','Matthew',10,0,0,0),(32,14,8,'2018-10-11','This is summary preview text for the article within','Matthew',8,0,0,0),(33,14,9,'2018-10-11','This is summary preview text for the article within','Matthew',8,0,0,0),(34,9,18,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,1),(35,9,19,'2018-10-11','This is summary preview text for the article within','Matthew',6,0,7,1),(36,14,10,'2018-10-11','This is summary preview text for the article within','Matthew',8,0,0,2),(37,14,11,'2018-10-11','This is summary preview text for the article within','Matthew',8,0,0,2);
/*!40000 ALTER TABLE `ArticlePage_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChangeSet`
--

DROP TABLE IF EXISTS `ChangeSet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChangeSet` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Versioned\\ChangeSet') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Versioned\\ChangeSet',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `State` enum('open','published','reverted') CHARACTER SET utf8 DEFAULT 'open',
  `IsInferred` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Description` mediumtext CHARACTER SET utf8,
  `PublishDate` datetime DEFAULT NULL,
  `LastSynced` datetime DEFAULT NULL,
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `State` (`State`),
  KEY `ID` (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `OwnerID` (`OwnerID`),
  KEY `PublisherID` (`PublisherID`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChangeSet`
--

LOCK TABLES `ChangeSet` WRITE;
/*!40000 ALTER TABLE `ChangeSet` DISABLE KEYS */;
INSERT INTO `ChangeSet` VALUES (1,'SilverStripe\\Versioned\\ChangeSet','2018-10-10 16:28:52','2018-10-10 16:28:51','Generated by publish of \'Detail About Us\' at 10 Oct 2018, 16:28:51','published',1,NULL,'2018-10-10 16:28:52','2018-10-10 16:28:51',0,1),(2,'SilverStripe\\Versioned\\ChangeSet','2018-10-10 16:47:24','2018-10-10 16:47:24','Generated by publish of \'Home\' at 10 Oct 2018, 16:47:24','published',1,NULL,'2018-10-10 16:47:24','2018-10-10 16:47:24',0,1),(3,'SilverStripe\\Versioned\\ChangeSet','2018-10-10 17:24:23','2018-10-10 17:24:23','Generated by publish of \'Gallery\' at 10 Oct 2018, 17:24:23','published',1,NULL,'2018-10-10 17:24:23','2018-10-10 17:24:23',0,1),(4,'SilverStripe\\Versioned\\ChangeSet','2018-10-10 17:25:39','2018-10-10 17:25:39','Generated by publish of \'Gallery Item 1\' at 10 Oct 2018, 17:25:39','published',1,NULL,'2018-10-10 17:25:39','2018-10-10 17:25:39',0,1),(5,'SilverStripe\\Versioned\\ChangeSet','2018-10-10 17:26:15','2018-10-10 17:26:14','Generated by publish of \'Gallery Item 2\' at 10 Oct 2018, 17:26:14','published',1,NULL,'2018-10-10 17:26:15','2018-10-10 17:26:14',0,1),(6,'SilverStripe\\Versioned\\ChangeSet','2018-10-10 17:26:47','2018-10-10 17:26:47','Generated by publish of \'Gallery Item 3\' at 10 Oct 2018, 17:26:47','published',1,NULL,'2018-10-10 17:26:47','2018-10-10 17:26:47',0,1),(7,'SilverStripe\\Versioned\\ChangeSet','2018-10-10 17:40:42','2018-10-10 17:40:42','Generated by publish of \'Gallery Item 1\' at 10 Oct 2018, 17:40:42','published',1,NULL,'2018-10-10 17:40:42','2018-10-10 17:40:42',0,1),(8,'SilverStripe\\Versioned\\ChangeSet','2018-10-10 17:41:00','2018-10-10 17:41:00','Generated by publish of \'Gallery Item 2\' at 10 Oct 2018, 17:41:00','published',1,NULL,'2018-10-10 17:41:00','2018-10-10 17:41:00',0,1),(9,'SilverStripe\\Versioned\\ChangeSet','2018-10-10 17:41:18','2018-10-10 17:41:18','Generated by publish of \'Gallery Item 3\' at 10 Oct 2018, 17:41:18','published',1,NULL,'2018-10-10 17:41:18','2018-10-10 17:41:18',0,1),(10,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 10:04:27','2018-10-11 10:04:27','Generated by publish of \'More Details 1\' at 11 Oct 2018, 10:04:27','published',1,NULL,'2018-10-11 10:04:27','2018-10-11 10:04:27',0,1),(11,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 10:04:55','2018-10-11 10:04:55','Generated by publish of \'More Details 2\' at 11 Oct 2018, 10:04:55','published',1,NULL,'2018-10-11 10:04:55','2018-10-11 10:04:55',0,1),(12,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 10:48:21','2018-10-11 10:48:21','Generated by publish of \'Articles\' at 11 Oct 2018, 10:48:21','published',1,NULL,'2018-10-11 10:48:21','2018-10-11 10:48:21',0,1),(13,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 10:48:43','2018-10-11 10:48:43','Generated by publish of \'Articles\' at 11 Oct 2018, 10:48:43','published',1,NULL,'2018-10-11 10:48:43','2018-10-11 10:48:43',0,1),(14,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 10:49:48','2018-10-11 10:49:48','Generated by publish of \'Gallery Item 1\' at 11 Oct 2018, 10:49:48','published',1,NULL,'2018-10-11 10:49:48','2018-10-11 10:49:48',0,1),(15,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 10:50:12','2018-10-11 10:50:12','Generated by publish of \'Gallery Item 1\' at 11 Oct 2018, 10:50:12','published',1,NULL,'2018-10-11 10:50:12','2018-10-11 10:50:12',0,1),(16,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 10:50:35','2018-10-11 10:50:35','Generated by publish of \'Article Item 1\' at 11 Oct 2018, 10:50:35','published',1,NULL,'2018-10-11 10:50:35','2018-10-11 10:50:35',0,1),(17,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 11:35:37','2018-10-11 11:35:37','Generated by publish of \'Article Item 1\' at 11 Oct 2018, 11:35:37','published',1,NULL,'2018-10-11 11:35:37','2018-10-11 11:35:37',0,1),(18,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 15:24:02','2018-10-11 15:24:02','Generated by publish of \'Article Item 1\' at 11 Oct 2018, 15:24:02','published',1,NULL,'2018-10-11 15:24:02','2018-10-11 15:24:02',0,1),(19,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 15:24:13','2018-10-11 15:24:13','Generated by publish of \'Article Item 1\' at 11 Oct 2018, 15:24:13','published',1,NULL,'2018-10-11 15:24:13','2018-10-11 15:24:13',0,1),(20,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 15:24:35','2018-10-11 15:24:35','Generated by publish of \'Articles\' at 11 Oct 2018, 15:24:35','published',1,NULL,'2018-10-11 15:24:35','2018-10-11 15:24:35',0,1),(21,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 15:31:47','2018-10-11 15:31:47','Generated by publish of \'Article Item 1\' at 11 Oct 2018, 15:31:47','published',1,NULL,'2018-10-11 15:31:47','2018-10-11 15:31:47',0,1),(22,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 15:39:54','2018-10-11 15:39:53','Generated by publish of \'Article Item 1\' at 11 Oct 2018, 15:39:53','published',1,NULL,'2018-10-11 15:39:54','2018-10-11 15:39:53',0,1),(23,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 15:41:28','2018-10-11 15:41:28','Generated by publish of \'Reference Card For Mac\' at 11 Oct 2018, 15:41:28','published',1,NULL,'2018-10-11 15:41:28','2018-10-11 15:41:28',0,1),(24,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 15:41:55','2018-10-11 15:41:55','Generated by publish of \'ethan weil 1093611 unsplash\' at 11 Oct 2018, 15:41:55','published',1,NULL,'2018-10-11 15:41:55','2018-10-11 15:41:55',0,1),(25,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 15:48:39','2018-10-11 15:48:39','Generated by publish of \'Article Item 1\' at 11 Oct 2018, 15:48:39','published',1,NULL,'2018-10-11 15:48:39','2018-10-11 15:48:39',0,1),(26,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 16:19:16','2018-10-11 16:19:16','Generated by publish of \'Article Item 1\' at 11 Oct 2018, 16:19:16','published',1,NULL,'2018-10-11 16:19:16','2018-10-11 16:19:16',0,1),(27,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 16:19:39','2018-10-11 16:19:39','Generated by publish of \'Article Item 1\' at 11 Oct 2018, 16:19:39','published',1,NULL,'2018-10-11 16:19:39','2018-10-11 16:19:39',0,1),(28,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 16:20:01','2018-10-11 16:20:00','Generated by publish of \'Article Item 1\' at 11 Oct 2018, 16:20:00','published',1,NULL,'2018-10-11 16:20:01','2018-10-11 16:20:00',0,1),(29,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 16:26:12','2018-10-11 16:26:12','Generated by publish of \'Article Item 2\' at 11 Oct 2018, 16:26:12','published',1,NULL,'2018-10-11 16:26:12','2018-10-11 16:26:12',0,1),(30,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 16:26:22','2018-10-11 16:26:22','Generated by publish of \'Article Item 3\' at 11 Oct 2018, 16:26:22','published',1,NULL,'2018-10-11 16:26:22','2018-10-11 16:26:22',0,1),(31,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 16:26:34','2018-10-11 16:26:34','Generated by publish of \'Article Item 4\' at 11 Oct 2018, 16:26:34','published',1,NULL,'2018-10-11 16:26:34','2018-10-11 16:26:34',0,1),(32,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 16:42:30','2018-10-11 16:42:30','Generated by publish of \'Article Item 2\' at 11 Oct 2018, 16:42:30','published',1,NULL,'2018-10-11 16:42:30','2018-10-11 16:42:30',0,1),(33,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 16:42:46','2018-10-11 16:42:46','Generated by publish of \'Article Item 3\' at 11 Oct 2018, 16:42:46','published',1,NULL,'2018-10-11 16:42:46','2018-10-11 16:42:46',0,1),(34,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 16:43:03','2018-10-11 16:43:03','Generated by publish of \'Article Item 4\' at 11 Oct 2018, 16:43:03','published',1,NULL,'2018-10-11 16:43:03','2018-10-11 16:43:03',0,1),(35,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 16:57:48','2018-10-11 16:57:48','Generated by publish of \'Article Item 4\' at 11 Oct 2018, 16:57:48','published',1,NULL,'2018-10-11 16:57:48','2018-10-11 16:57:48',0,1),(36,'SilverStripe\\Versioned\\ChangeSet','2018-10-11 16:58:00','2018-10-11 16:58:00','Generated by publish of \'Article Item 2\' at 11 Oct 2018, 16:58:00','published',1,NULL,'2018-10-11 16:58:00','2018-10-11 16:58:00',0,1),(37,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 10:47:47','2018-10-12 10:47:47','Generated by publish of \'Regions\' at 12 Oct 2018, 10:47:47','published',1,NULL,'2018-10-12 10:47:47','2018-10-12 10:47:47',0,1),(38,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 10:50:07','2018-10-12 10:50:07','Generated by publish of \'Regions\' at 12 Oct 2018, 10:50:07','published',1,NULL,'2018-10-12 10:50:07','2018-10-12 10:50:07',0,1),(39,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 11:39:57','2018-10-12 11:39:57','Generated by publish of \'Ellerslie\' at 12 Oct 2018, 11:39:57','published',1,NULL,'2018-10-12 11:39:57','2018-10-12 11:39:57',0,1),(40,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 11:51:33','2018-10-12 11:51:33','Generated by publish of \'Regions\' at 12 Oct 2018, 11:51:33','published',1,NULL,'2018-10-12 11:51:33','2018-10-12 11:51:33',0,1),(41,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 13:11:24','2018-10-12 13:11:24','Generated by publish of \'Remuera\' at 12 Oct 2018, 13:11:24','published',1,NULL,'2018-10-12 13:11:24','2018-10-12 13:11:24',0,1),(42,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 13:57:46','2018-10-12 13:57:46','Generated by publish of \'Exiciting\' at 12 Oct 2018, 13:57:46','published',1,NULL,'2018-10-12 13:57:46','2018-10-12 13:57:46',0,1),(43,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 13:58:00','2018-10-12 13:58:00','Generated by publish of \'Beautiful\' at 12 Oct 2018, 13:58:00','published',1,NULL,'2018-10-12 13:58:00','2018-10-12 13:58:00',0,1),(44,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 13:58:14','2018-10-12 13:58:14','Generated by publish of \'Exiciting\' at 12 Oct 2018, 13:58:14','published',1,NULL,'2018-10-12 13:58:14','2018-10-12 13:58:14',0,1),(45,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 13:58:36','2018-10-12 13:58:36','Generated by publish of \'Interesting\' at 12 Oct 2018, 13:58:36','published',1,NULL,'2018-10-12 13:58:36','2018-10-12 13:58:36',0,1),(46,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 14:27:32','2018-10-12 14:27:32','Generated by publish of \'Article Item 1\' at 12 Oct 2018, 14:27:32','published',1,NULL,'2018-10-12 14:27:32','2018-10-12 14:27:32',0,1),(47,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 14:27:55','2018-10-12 14:27:55','Generated by publish of \'Article Item 2\' at 12 Oct 2018, 14:27:55','published',1,NULL,'2018-10-12 14:27:55','2018-10-12 14:27:55',0,1),(48,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 14:28:09','2018-10-12 14:28:09','Generated by publish of \'Article Item 3\' at 12 Oct 2018, 14:28:09','published',1,NULL,'2018-10-12 14:28:09','2018-10-12 14:28:09',0,1),(49,'SilverStripe\\Versioned\\ChangeSet','2018-10-12 14:28:22','2018-10-12 14:28:22','Generated by publish of \'Article Item 4\' at 12 Oct 2018, 14:28:22','published',1,NULL,'2018-10-12 14:28:22','2018-10-12 14:28:22',0,1),(50,'SilverStripe\\Versioned\\ChangeSet','2018-11-29 14:22:29','2018-11-29 14:22:29','Generated by publish of \'Your Site Name\' at 29 Nov 2018, 14:22:29','published',1,NULL,'2018-11-29 14:22:29','2018-11-29 14:22:29',0,1),(51,'SilverStripe\\Versioned\\ChangeSet','2018-11-29 14:22:45','2018-11-29 14:22:45','Generated by publish of \'Your Site Name\' at 29 Nov 2018, 14:22:45','published',1,NULL,'2018-11-29 14:22:45','2018-11-29 14:22:45',0,1),(52,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 14:25:19','2018-12-06 14:25:19','Generated by publish of \'Home\' at 6 Dec 2018, 14:25:19','published',1,NULL,'2018-12-06 14:25:19','2018-12-06 14:25:19',0,1),(53,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 14:25:48','2018-12-06 14:25:48','Generated by publish of \'About Us\' at 6 Dec 2018, 14:25:48','published',1,NULL,'2018-12-06 14:25:48','2018-12-06 14:25:48',0,1),(54,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 15:49:14','2018-12-06 15:49:14','Generated by publish of \'First Property\' at 6 Dec 2018, 15:49:14','published',1,NULL,'2018-12-06 15:49:14','2018-12-06 15:49:14',0,1),(55,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 15:49:52','2018-12-06 15:49:51','Generated by publish of \'Second Property\' at 6 Dec 2018, 15:49:51','published',1,NULL,'2018-12-06 15:49:52','2018-12-06 15:49:51',0,1),(56,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 15:50:18','2018-12-06 15:50:18','Generated by publish of \'Third Property\' at 6 Dec 2018, 15:50:18','published',1,NULL,'2018-12-06 15:50:18','2018-12-06 15:50:18',0,1),(57,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 15:53:28','2018-12-06 15:53:28','Generated by publish of \'Third Property\' at 6 Dec 2018, 15:53:28','published',1,NULL,'2018-12-06 15:53:28','2018-12-06 15:53:28',0,1),(58,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 15:53:32','2018-12-06 15:53:32','Generated by publish of \'Third Property\' at 6 Dec 2018, 15:53:32','published',1,NULL,'2018-12-06 15:53:32','2018-12-06 15:53:32',0,1),(59,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 15:54:00','2018-12-06 15:54:00','Generated by publish of \'Second Property\' at 6 Dec 2018, 15:54:00','published',1,NULL,'2018-12-06 15:54:00','2018-12-06 15:54:00',0,1),(60,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 15:54:26','2018-12-06 15:54:26','Generated by publish of \'First Property\' at 6 Dec 2018, 15:54:26','published',1,NULL,'2018-12-06 15:54:26','2018-12-06 15:54:26',0,1),(61,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 15:54:33','2018-12-06 15:54:33','Generated by publish of \'First Property\' at 6 Dec 2018, 15:54:33','published',1,NULL,'2018-12-06 15:54:33','2018-12-06 15:54:33',0,1),(62,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 16:06:19','2018-12-06 16:06:19','Generated by publish of \'First Property\' at 6 Dec 2018, 16:06:19','published',1,NULL,'2018-12-06 16:06:19','2018-12-06 16:06:19',0,1),(63,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 16:06:33','2018-12-06 16:06:32','Generated by publish of \'Third Property\' at 6 Dec 2018, 16:06:32','published',1,NULL,'2018-12-06 16:06:33','2018-12-06 16:06:33',0,1),(64,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 16:33:37','2018-12-06 16:33:36','Generated by publish of \'First Property\' at 6 Dec 2018, 16:33:36','published',1,NULL,'2018-12-06 16:33:37','2018-12-06 16:33:37',0,1),(65,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 16:33:58','2018-12-06 16:33:58','Generated by publish of \'Second Property\' at 6 Dec 2018, 16:33:58','published',1,NULL,'2018-12-06 16:33:58','2018-12-06 16:33:58',0,1),(66,'SilverStripe\\Versioned\\ChangeSet','2018-12-06 16:34:11','2018-12-06 16:34:11','Generated by publish of \'Third Property\' at 6 Dec 2018, 16:34:11','published',1,NULL,'2018-12-06 16:34:11','2018-12-06 16:34:11',0,1),(67,'SilverStripe\\Versioned\\ChangeSet','2018-12-07 12:25:19','2018-12-07 12:25:19','Generated by publish of \'Property 1\' at 7 Dec 2018, 12:25:19','published',1,NULL,'2018-12-07 12:25:19','2018-12-07 12:25:19',0,1),(68,'SilverStripe\\Versioned\\ChangeSet','2018-12-07 12:25:39','2018-12-07 12:25:39','Generated by publish of \'Property 1\' at 7 Dec 2018, 12:25:39','published',1,NULL,'2018-12-07 12:25:39','2018-12-07 12:25:39',0,1),(69,'SilverStripe\\Versioned\\ChangeSet','2018-12-07 12:26:25','2018-12-07 12:26:25','Generated by publish of \'Property 2\' at 7 Dec 2018, 12:26:25','published',1,NULL,'2018-12-07 12:26:25','2018-12-07 12:26:25',0,1),(70,'SilverStripe\\Versioned\\ChangeSet','2018-12-07 12:26:39','2018-12-07 12:26:39','Generated by publish of \'Property 2\' at 7 Dec 2018, 12:26:39','published',1,NULL,'2018-12-07 12:26:39','2018-12-07 12:26:39',0,1),(71,'SilverStripe\\Versioned\\ChangeSet','2018-12-07 12:27:06','2018-12-07 12:27:06','Generated by publish of \'Property 3\' at 7 Dec 2018, 12:27:06','published',1,NULL,'2018-12-07 12:27:06','2018-12-07 12:27:06',0,1),(72,'SilverStripe\\Versioned\\ChangeSet','2018-12-07 12:27:17','2018-12-07 12:27:17','Generated by publish of \'Property 3\' at 7 Dec 2018, 12:27:17','published',1,NULL,'2018-12-07 12:27:17','2018-12-07 12:27:17',0,1),(73,'SilverStripe\\Versioned\\ChangeSet','2018-12-07 12:27:45','2018-12-07 12:27:45','Generated by publish of \'Property 4\' at 7 Dec 2018, 12:27:45','published',1,NULL,'2018-12-07 12:27:45','2018-12-07 12:27:45',0,1),(74,'SilverStripe\\Versioned\\ChangeSet','2018-12-07 12:28:13','2018-12-07 12:28:13','Generated by publish of \'Property 4\' at 7 Dec 2018, 12:28:13','published',1,NULL,'2018-12-07 12:28:13','2018-12-07 12:28:13',0,1),(75,'SilverStripe\\Versioned\\ChangeSet','2018-12-07 14:13:32','2018-12-07 14:13:32','Generated by publish of \'Newmarket\' at 7 Dec 2018, 14:13:32','published',1,NULL,'2018-12-07 14:13:32','2018-12-07 14:13:32',0,1),(76,'SilverStripe\\Versioned\\ChangeSet','2018-12-07 14:54:36','2018-12-07 14:54:35','Generated by publish of \'Ellerslie\' at 7 Dec 2018, 14:54:35','published',1,NULL,'2018-12-07 14:54:36','2018-12-07 14:54:35',0,1),(77,'SilverStripe\\Versioned\\ChangeSet','2019-01-17 10:27:50','2019-01-17 10:27:50','Generated by publish of \'Property 1\' at 17 Jan 2019, 10:27:50','published',1,NULL,'2019-01-17 10:27:50','2019-01-17 10:27:50',0,1),(78,'SilverStripe\\Versioned\\ChangeSet','2019-01-17 10:28:13','2019-01-17 10:28:13','Generated by publish of \'Property 2\' at 17 Jan 2019, 10:28:13','published',1,NULL,'2019-01-17 10:28:13','2019-01-17 10:28:13',0,1),(79,'SilverStripe\\Versioned\\ChangeSet','2019-01-17 10:28:27','2019-01-17 10:28:27','Generated by publish of \'Property 2\' at 17 Jan 2019, 10:28:27','published',1,NULL,'2019-01-17 10:28:27','2019-01-17 10:28:27',0,1),(80,'SilverStripe\\Versioned\\ChangeSet','2019-01-17 10:28:56','2019-01-17 10:28:56','Generated by publish of \'Property 3\' at 17 Jan 2019, 10:28:56','published',1,NULL,'2019-01-17 10:28:56','2019-01-17 10:28:56',0,1),(81,'SilverStripe\\Versioned\\ChangeSet','2019-01-17 10:29:22','2019-01-17 10:29:22','Generated by publish of \'Property 4\' at 17 Jan 2019, 10:29:22','published',1,NULL,'2019-01-17 10:29:22','2019-01-17 10:29:22',0,1),(82,'SilverStripe\\Versioned\\ChangeSet','2019-01-17 10:36:18','2019-01-17 10:36:18','Generated by publish of \'Find a Rental\' at 17 Jan 2019, 10:36:18','published',1,NULL,'2019-01-17 10:36:18','2019-01-17 10:36:18',0,1),(83,'SilverStripe\\Versioned\\ChangeSet','2019-01-18 10:24:24','2019-01-18 10:24:24','Generated by publish of \'Travel Guides\' at 18 Jan 2019, 10:24:24','published',1,NULL,'2019-01-18 10:24:24','2019-01-18 10:24:24',0,1),(84,'SilverStripe\\Versioned\\ChangeSet','2019-01-18 10:30:15','2019-01-18 10:30:15','Generated by publish of \'Travel Guides\' at 18 Jan 2019, 10:30:15','published',1,NULL,'2019-01-18 10:30:15','2019-01-18 10:30:15',0,1),(85,'SilverStripe\\Versioned\\ChangeSet','2019-01-18 13:38:24','2019-01-18 13:38:24','Generated by publish of \'Ellerslie\' at 18 Jan 2019, 13:38:24','published',1,NULL,'2019-01-18 13:38:24','2019-01-18 13:38:24',0,1),(86,'SilverStripe\\Versioned\\ChangeSet','2019-01-18 14:14:15','2019-01-18 14:14:15','Generated by publish of \'Article Item 1\' at 18 Jan 2019, 14:14:15','published',1,NULL,'2019-01-18 14:14:15','2019-01-18 14:14:15',0,1),(87,'SilverStripe\\Versioned\\ChangeSet','2019-01-18 14:14:31','2019-01-18 14:14:31','Generated by publish of \'Article Item 2\' at 18 Jan 2019, 14:14:31','published',1,NULL,'2019-01-18 14:14:31','2019-01-18 14:14:31',0,1);
/*!40000 ALTER TABLE `ChangeSet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChangeSetItem`
--

DROP TABLE IF EXISTS `ChangeSetItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChangeSetItem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Versioned\\ChangeSetItem') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Versioned\\ChangeSetItem',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `VersionBefore` int(11) NOT NULL DEFAULT '0',
  `VersionAfter` int(11) NOT NULL DEFAULT '0',
  `Added` enum('explicitly','implicitly') CHARACTER SET utf8 DEFAULT 'implicitly',
  `ChangeSetID` int(11) NOT NULL DEFAULT '0',
  `ObjectID` int(11) NOT NULL DEFAULT '0',
  `ObjectClass` enum('ArticleCategory','ArticleComment','Property','Region','SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSet','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\Member','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Page','ArticleHolder','ArticlePage','HomePage','PropertySearchPage','RegionsPage','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage') CHARACTER SET utf8 DEFAULT 'ArticleCategory',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ObjectUniquePerChangeSet` (`ObjectID`,`ObjectClass`,`ChangeSetID`),
  KEY `ClassName` (`ClassName`),
  KEY `ChangeSetID` (`ChangeSetID`),
  KEY `Object` (`ObjectID`,`ObjectClass`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChangeSetItem`
--

LOCK TABLES `ChangeSetItem` WRITE;
/*!40000 ALTER TABLE `ChangeSetItem` DISABLE KEYS */;
INSERT INTO `ChangeSetItem` VALUES (1,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-10 16:28:52','2018-10-10 16:28:51',0,3,'explicitly',1,6,'SilverStripe\\CMS\\Model\\SiteTree'),(2,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-10 16:47:24','2018-10-10 16:47:24',2,4,'explicitly',2,1,'SilverStripe\\CMS\\Model\\SiteTree'),(3,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-10 17:24:23','2018-10-10 17:24:23',0,3,'explicitly',3,8,'SilverStripe\\CMS\\Model\\SiteTree'),(4,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-10 17:25:39','2018-10-10 17:25:39',0,3,'explicitly',4,9,'SilverStripe\\CMS\\Model\\SiteTree'),(7,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-10 17:40:42','2018-10-10 17:40:42',3,5,'explicitly',7,9,'SilverStripe\\CMS\\Model\\SiteTree'),(10,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 10:04:27','2018-10-11 10:04:27',0,3,'explicitly',10,12,'SilverStripe\\CMS\\Model\\SiteTree'),(11,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 10:04:55','2018-10-11 10:04:55',0,3,'explicitly',11,13,'SilverStripe\\CMS\\Model\\SiteTree'),(12,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 10:48:21','2018-10-11 10:48:21',3,5,'explicitly',12,8,'SilverStripe\\CMS\\Model\\SiteTree'),(13,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 10:48:43','2018-10-11 10:48:43',5,7,'explicitly',13,8,'SilverStripe\\CMS\\Model\\SiteTree'),(14,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 10:49:48','2018-10-11 10:49:48',5,7,'explicitly',14,9,'SilverStripe\\CMS\\Model\\SiteTree'),(15,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 10:50:12','2018-10-11 10:50:12',7,7,'explicitly',15,9,'SilverStripe\\CMS\\Model\\SiteTree'),(16,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 10:50:35','2018-10-11 10:50:35',7,9,'explicitly',16,9,'SilverStripe\\CMS\\Model\\SiteTree'),(17,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 11:35:37','2018-10-11 11:35:37',9,11,'explicitly',17,9,'SilverStripe\\CMS\\Model\\SiteTree'),(18,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 15:24:02','2018-10-11 15:24:02',11,13,'explicitly',18,9,'SilverStripe\\CMS\\Model\\SiteTree'),(19,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 15:24:13','2018-10-11 15:24:13',13,13,'explicitly',19,9,'SilverStripe\\CMS\\Model\\SiteTree'),(20,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 15:24:35','2018-10-11 15:24:35',7,9,'explicitly',20,8,'SilverStripe\\CMS\\Model\\SiteTree'),(21,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 15:31:47','2018-10-11 15:31:47',13,15,'explicitly',21,9,'SilverStripe\\CMS\\Model\\SiteTree'),(22,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 15:39:54','2018-10-11 15:39:53',15,15,'explicitly',22,9,'SilverStripe\\CMS\\Model\\SiteTree'),(23,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 15:41:28','2018-10-11 15:41:28',0,3,'explicitly',23,7,'SilverStripe\\Assets\\File'),(24,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 15:41:55','2018-10-11 15:41:55',0,3,'explicitly',24,6,'SilverStripe\\Assets\\File'),(25,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 15:48:39','2018-10-11 15:48:39',15,17,'explicitly',25,9,'SilverStripe\\CMS\\Model\\SiteTree'),(26,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:19:16','2018-10-11 16:19:16',0,3,'explicitly',26,14,'SilverStripe\\CMS\\Model\\SiteTree'),(27,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:19:39','2018-10-11 16:19:39',0,3,'explicitly',27,15,'SilverStripe\\CMS\\Model\\SiteTree'),(28,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:20:01','2018-10-11 16:20:00',0,3,'explicitly',28,16,'SilverStripe\\CMS\\Model\\SiteTree'),(29,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:26:12','2018-10-11 16:26:12',3,5,'explicitly',29,14,'SilverStripe\\CMS\\Model\\SiteTree'),(30,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:26:22','2018-10-11 16:26:22',3,5,'explicitly',30,15,'SilverStripe\\CMS\\Model\\SiteTree'),(31,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:26:34','2018-10-11 16:26:34',3,5,'explicitly',31,16,'SilverStripe\\CMS\\Model\\SiteTree'),(32,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:42:30','2018-10-11 16:42:30',5,7,'explicitly',32,14,'SilverStripe\\CMS\\Model\\SiteTree'),(33,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:42:30','2018-10-11 16:42:30',0,2,'implicitly',32,8,'SilverStripe\\Assets\\File'),(34,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:42:30','2018-10-11 16:42:30',3,3,'implicitly',32,7,'SilverStripe\\Assets\\File'),(35,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:42:46','2018-10-11 16:42:46',5,7,'explicitly',33,15,'SilverStripe\\CMS\\Model\\SiteTree'),(36,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:42:46','2018-10-11 16:42:46',0,2,'implicitly',33,9,'SilverStripe\\Assets\\File'),(37,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:42:46','2018-10-11 16:42:46',3,3,'implicitly',33,7,'SilverStripe\\Assets\\File'),(38,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:43:03','2018-10-11 16:43:03',5,7,'explicitly',34,16,'SilverStripe\\CMS\\Model\\SiteTree'),(39,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:43:03','2018-10-11 16:43:03',0,2,'implicitly',34,10,'SilverStripe\\Assets\\File'),(40,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:43:03','2018-10-11 16:43:03',3,3,'implicitly',34,7,'SilverStripe\\Assets\\File'),(41,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:57:48','2018-10-11 16:57:48',7,9,'explicitly',35,16,'SilverStripe\\CMS\\Model\\SiteTree'),(42,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:57:48','2018-10-11 16:57:48',2,2,'implicitly',35,10,'SilverStripe\\Assets\\File'),(43,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:58:00','2018-10-11 16:58:00',7,9,'explicitly',36,14,'SilverStripe\\CMS\\Model\\SiteTree'),(44,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-11 16:58:00','2018-10-11 16:58:00',2,2,'implicitly',36,8,'SilverStripe\\Assets\\File'),(45,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 10:47:47','2018-10-12 10:47:47',0,3,'explicitly',37,17,'SilverStripe\\CMS\\Model\\SiteTree'),(46,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 10:50:07','2018-10-12 10:50:07',3,5,'explicitly',38,17,'SilverStripe\\CMS\\Model\\SiteTree'),(47,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 11:39:57','2018-10-12 11:39:57',0,2,'explicitly',39,1,'Region'),(48,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 11:39:57','2018-10-12 11:39:57',0,2,'implicitly',39,12,'SilverStripe\\Assets\\File'),(49,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 11:51:33','2018-10-12 11:51:33',5,5,'explicitly',40,17,'SilverStripe\\CMS\\Model\\SiteTree'),(50,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 11:51:33','2018-10-12 11:51:33',2,2,'implicitly',40,1,'Region'),(51,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 11:51:33','2018-10-12 11:51:33',2,2,'implicitly',40,12,'SilverStripe\\Assets\\File'),(52,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 13:11:24','2018-10-12 13:11:24',0,2,'explicitly',41,2,'Region'),(53,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 13:11:24','2018-10-12 13:11:24',0,2,'implicitly',41,13,'SilverStripe\\Assets\\File'),(54,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 13:57:46','2018-10-12 13:57:46',0,0,'explicitly',42,1,'ArticleCategory'),(55,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 13:58:00','2018-10-12 13:58:00',0,0,'explicitly',43,1,'ArticleCategory'),(56,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 13:58:14','2018-10-12 13:58:14',0,0,'explicitly',44,2,'ArticleCategory'),(57,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 13:58:36','2018-10-12 13:58:36',0,0,'explicitly',45,3,'ArticleCategory'),(58,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 14:27:32','2018-10-12 14:27:32',17,17,'explicitly',46,9,'SilverStripe\\CMS\\Model\\SiteTree'),(59,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 14:27:32','2018-10-12 14:27:32',3,3,'implicitly',46,6,'SilverStripe\\Assets\\File'),(60,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 14:27:32','2018-10-12 14:27:32',3,3,'implicitly',46,7,'SilverStripe\\Assets\\File'),(61,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 14:27:55','2018-10-12 14:27:55',9,9,'explicitly',47,14,'SilverStripe\\CMS\\Model\\SiteTree'),(62,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 14:27:55','2018-10-12 14:27:55',2,2,'implicitly',47,8,'SilverStripe\\Assets\\File'),(63,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 14:28:09','2018-10-12 14:28:09',7,7,'explicitly',48,15,'SilverStripe\\CMS\\Model\\SiteTree'),(64,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 14:28:09','2018-10-12 14:28:09',2,2,'implicitly',48,9,'SilverStripe\\Assets\\File'),(65,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 14:28:09','2018-10-12 14:28:09',3,3,'implicitly',48,7,'SilverStripe\\Assets\\File'),(66,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 14:28:22','2018-10-12 14:28:22',9,9,'explicitly',49,16,'SilverStripe\\CMS\\Model\\SiteTree'),(67,'SilverStripe\\Versioned\\ChangeSetItem','2018-10-12 14:28:22','2018-10-12 14:28:22',2,2,'implicitly',49,10,'SilverStripe\\Assets\\File'),(68,'SilverStripe\\Versioned\\ChangeSetItem','2018-11-29 14:22:29','2018-11-29 14:22:29',0,0,'explicitly',50,1,'SilverStripe\\SiteConfig\\SiteConfig'),(69,'SilverStripe\\Versioned\\ChangeSetItem','2018-11-29 14:22:45','2018-11-29 14:22:45',0,0,'explicitly',51,1,'SilverStripe\\SiteConfig\\SiteConfig'),(70,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 14:25:19','2018-12-06 14:25:19',4,6,'explicitly',52,1,'SilverStripe\\CMS\\Model\\SiteTree'),(71,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 14:25:48','2018-12-06 14:25:48',2,4,'explicitly',53,2,'SilverStripe\\CMS\\Model\\SiteTree'),(72,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 15:49:14','2018-12-06 15:49:14',0,0,'explicitly',54,1,'Property'),(73,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 15:49:52','2018-12-06 15:49:51',0,0,'explicitly',55,2,'Property'),(74,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 15:50:18','2018-12-06 15:50:18',0,0,'explicitly',56,3,'Property'),(75,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 15:53:28','2018-12-06 15:53:28',0,0,'explicitly',57,3,'Property'),(76,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 15:53:32','2018-12-06 15:53:32',0,0,'explicitly',58,3,'Property'),(77,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 15:54:00','2018-12-06 15:54:00',0,0,'explicitly',59,2,'Property'),(78,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 15:54:26','2018-12-06 15:54:26',0,0,'explicitly',60,1,'Property'),(79,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 15:54:33','2018-12-06 15:54:33',0,0,'explicitly',61,1,'Property'),(80,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 16:06:19','2018-12-06 16:06:19',0,0,'explicitly',62,1,'Property'),(81,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 16:06:33','2018-12-06 16:06:32',0,0,'explicitly',63,3,'Property'),(82,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 16:33:37','2018-12-06 16:33:36',0,0,'explicitly',64,1,'Property'),(83,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 16:33:37','2018-12-06 16:33:37',0,2,'implicitly',64,15,'SilverStripe\\Assets\\File'),(84,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 16:33:58','2018-12-06 16:33:58',0,0,'explicitly',65,2,'Property'),(85,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 16:33:58','2018-12-06 16:33:58',0,2,'implicitly',65,17,'SilverStripe\\Assets\\File'),(86,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 16:34:11','2018-12-06 16:34:11',0,0,'explicitly',66,3,'Property'),(87,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-06 16:34:11','2018-12-06 16:34:11',2,2,'implicitly',66,17,'SilverStripe\\Assets\\File'),(88,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 12:25:19','2018-12-07 12:25:19',0,2,'explicitly',67,1,'Property'),(89,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 12:25:39','2018-12-07 12:25:39',2,4,'explicitly',68,1,'Property'),(90,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 12:25:39','2018-12-07 12:25:39',2,2,'implicitly',68,15,'SilverStripe\\Assets\\File'),(91,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 12:26:25','2018-12-07 12:26:25',0,2,'explicitly',69,2,'Property'),(92,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 12:26:39','2018-12-07 12:26:39',2,4,'explicitly',70,2,'Property'),(93,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 12:26:39','2018-12-07 12:26:39',2,2,'implicitly',70,17,'SilverStripe\\Assets\\File'),(94,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 12:27:06','2018-12-07 12:27:06',0,2,'explicitly',71,3,'Property'),(95,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 12:27:17','2018-12-07 12:27:17',2,4,'explicitly',72,3,'Property'),(96,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 12:27:17','2018-12-07 12:27:17',0,2,'implicitly',72,18,'SilverStripe\\Assets\\File'),(97,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 12:27:45','2018-12-07 12:27:45',0,2,'explicitly',73,4,'Property'),(98,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 12:28:13','2018-12-07 12:28:13',2,4,'explicitly',74,4,'Property'),(99,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 12:28:13','2018-12-07 12:28:13',0,2,'implicitly',74,19,'SilverStripe\\Assets\\File'),(100,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 14:13:32','2018-12-07 14:13:32',0,2,'explicitly',75,3,'Region'),(101,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 14:13:32','2018-12-07 14:13:32',0,2,'implicitly',75,20,'SilverStripe\\Assets\\File'),(102,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 14:54:35','2018-12-07 14:54:35',2,5,'explicitly',76,1,'Region'),(103,'SilverStripe\\Versioned\\ChangeSetItem','2018-12-07 14:54:35','2018-12-07 14:54:35',2,2,'implicitly',76,12,'SilverStripe\\Assets\\File'),(104,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-17 10:27:50','2019-01-17 10:27:50',4,6,'explicitly',77,1,'Property'),(105,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-17 10:27:50','2019-01-17 10:27:50',2,2,'implicitly',77,15,'SilverStripe\\Assets\\File'),(106,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-17 10:28:13','2019-01-17 10:28:13',4,6,'explicitly',78,2,'Property'),(107,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-17 10:28:13','2019-01-17 10:28:13',2,2,'implicitly',78,17,'SilverStripe\\Assets\\File'),(108,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-17 10:28:27','2019-01-17 10:28:27',6,8,'explicitly',79,2,'Property'),(109,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-17 10:28:27','2019-01-17 10:28:27',2,2,'implicitly',79,17,'SilverStripe\\Assets\\File'),(110,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-17 10:28:56','2019-01-17 10:28:56',4,6,'explicitly',80,3,'Property'),(111,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-17 10:28:56','2019-01-17 10:28:56',2,2,'implicitly',80,18,'SilverStripe\\Assets\\File'),(112,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-17 10:29:22','2019-01-17 10:29:22',4,6,'explicitly',81,4,'Property'),(113,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-17 10:29:22','2019-01-17 10:29:22',2,2,'implicitly',81,19,'SilverStripe\\Assets\\File'),(114,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-17 10:36:18','2019-01-17 10:36:18',0,3,'explicitly',82,18,'SilverStripe\\CMS\\Model\\SiteTree'),(116,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-18 10:30:15','2019-01-18 10:30:15',9,11,'explicitly',84,8,'SilverStripe\\CMS\\Model\\SiteTree'),(117,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-18 13:38:24','2019-01-18 13:38:24',5,8,'explicitly',85,1,'Region'),(118,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-18 13:38:24','2019-01-18 13:38:24',2,2,'implicitly',85,12,'SilverStripe\\Assets\\File'),(119,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-18 14:14:15','2019-01-18 14:14:15',17,19,'explicitly',86,9,'SilverStripe\\CMS\\Model\\SiteTree'),(120,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-18 14:14:15','2019-01-18 14:14:15',3,3,'implicitly',86,6,'SilverStripe\\Assets\\File'),(121,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-18 14:14:15','2019-01-18 14:14:15',3,3,'implicitly',86,7,'SilverStripe\\Assets\\File'),(122,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-18 14:14:31','2019-01-18 14:14:31',9,11,'explicitly',87,14,'SilverStripe\\CMS\\Model\\SiteTree'),(123,'SilverStripe\\Versioned\\ChangeSetItem','2019-01-18 14:14:31','2019-01-18 14:14:31',2,2,'implicitly',87,8,'SilverStripe\\Assets\\File');
/*!40000 ALTER TABLE `ChangeSetItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChangeSetItem_ReferencedBy`
--

DROP TABLE IF EXISTS `ChangeSetItem_ReferencedBy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChangeSetItem_ReferencedBy` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ChangeSetItemID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ChangeSetItemID` (`ChangeSetItemID`),
  KEY `ChildID` (`ChildID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChangeSetItem_ReferencedBy`
--

LOCK TABLES `ChangeSetItem_ReferencedBy` WRITE;
/*!40000 ALTER TABLE `ChangeSetItem_ReferencedBy` DISABLE KEYS */;
INSERT INTO `ChangeSetItem_ReferencedBy` VALUES (1,33,32),(2,34,32),(3,36,35),(4,37,35),(5,39,38),(6,40,38),(7,42,41),(8,44,43),(9,48,47),(10,50,49),(11,51,49),(12,53,52),(13,59,58),(14,60,58),(15,62,61),(16,64,63),(17,65,63),(18,67,66),(19,83,82),(20,85,84),(21,87,86),(22,90,89),(23,93,92),(24,96,95),(25,99,98),(26,101,100),(27,103,102),(28,105,104),(29,107,106),(30,109,108),(31,111,110),(32,113,112),(33,118,117),(34,120,119),(35,121,119),(36,123,122);
/*!40000 ALTER TABLE `ChangeSetItem_ReferencedBy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ErrorPage`
--

DROP TABLE IF EXISTS `ErrorPage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ErrorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ErrorPage`
--

LOCK TABLES `ErrorPage` WRITE;
/*!40000 ALTER TABLE `ErrorPage` DISABLE KEYS */;
INSERT INTO `ErrorPage` VALUES (4,404),(5,500);
/*!40000 ALTER TABLE `ErrorPage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ErrorPage_Live`
--

DROP TABLE IF EXISTS `ErrorPage_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ErrorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ErrorPage_Live`
--

LOCK TABLES `ErrorPage_Live` WRITE;
/*!40000 ALTER TABLE `ErrorPage_Live` DISABLE KEYS */;
INSERT INTO `ErrorPage_Live` VALUES (4,404),(5,500);
/*!40000 ALTER TABLE `ErrorPage_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ErrorPage_Versions`
--

DROP TABLE IF EXISTS `ErrorPage_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ErrorPage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ErrorPage_Versions`
--

LOCK TABLES `ErrorPage_Versions` WRITE;
/*!40000 ALTER TABLE `ErrorPage_Versions` DISABLE KEYS */;
INSERT INTO `ErrorPage_Versions` VALUES (1,4,1,404),(2,4,2,404),(3,5,1,500),(4,5,2,500);
/*!40000 ALTER TABLE `ErrorPage_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `File`
--

DROP TABLE IF EXISTS `File`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `File` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Assets\\File','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\File',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `FileHash` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileFilename` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileVariant` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Name` (`Name`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `File`
--

LOCK TABLES `File` WRITE;
/*!40000 ALTER TABLE `File` DISABLE KEYS */;
INSERT INTO `File` VALUES (1,'SilverStripe\\Assets\\Folder','2018-10-11 15:22:42','2018-10-11 15:22:42',2,'Inherit','Inherit','Uploads','Uploads',1,0,1,NULL,NULL,NULL),(4,'SilverStripe\\Assets\\Folder','2018-10-11 15:31:07','2018-10-11 15:31:07',2,'Inherit','Inherit','photos','photos',1,0,1,NULL,NULL,NULL),(5,'SilverStripe\\Assets\\Folder','2018-10-11 15:31:07','2018-10-11 15:31:07',2,'Inherit','Inherit','file-attachments','file-attachments',1,0,1,NULL,NULL,NULL),(6,'SilverStripe\\Assets\\Image','2018-10-11 15:41:55','2018-10-11 15:31:23',3,'Inherit','Inherit','ethan-weil-1093611-unsplash.jpg','ethan weil 1093611 unsplash',1,4,1,'92e724c0f55f3151088f8c531214de7ccc567ecf','photos/ethan-weil-1093611-unsplash.jpg',NULL),(7,'SilverStripe\\Assets\\File','2018-10-11 15:41:28','2018-10-11 15:31:42',3,'Inherit','Inherit','ReferenceCardForMac.pdf','Reference Card For Mac',1,5,1,'bb6db3a1ca64e8b5882880d053aecb770b4da6ac','file-attachments/ReferenceCardForMac.pdf',NULL),(8,'SilverStripe\\Assets\\Image','2018-10-11 16:42:30','2018-10-11 16:42:26',2,'Inherit','Inherit','tristan-pineda-1093539-unsplash.jpg','tristan pineda 1093539 unsplash',1,4,1,'3a35edbac3a4852bdb8e962362704f68a8ca3e38','photos/tristan-pineda-1093539-unsplash.jpg',NULL),(9,'SilverStripe\\Assets\\Image','2018-10-11 16:42:46','2018-10-11 16:42:43',2,'Inherit','Inherit','roman-fox-1093593-unsplash.jpg','roman fox 1093593 unsplash',1,4,1,'d0a986a80a23bbf0f115ac042cbe938539d2f1d2','photos/roman-fox-1093593-unsplash.jpg',NULL),(10,'SilverStripe\\Assets\\Image','2018-10-11 16:43:03','2018-10-11 16:42:59',2,'Inherit','Inherit','leif-niemczik-1093522-unsplash.jpg','leif niemczik 1093522 unsplash',1,4,1,'2d043a3e0334924ea371a46b040a45657125c450','photos/leif-niemczik-1093522-unsplash.jpg',NULL),(11,'SilverStripe\\Assets\\Folder','2018-10-12 11:38:55','2018-10-12 11:38:55',2,'Inherit','Inherit','region-photos','region-photos',1,0,1,NULL,NULL,NULL),(12,'SilverStripe\\Assets\\Image','2018-10-12 11:39:57','2018-10-12 11:39:52',2,'Inherit','Inherit','ellerslieHall.jpg','ellerslieHall',1,11,1,'cecb016a8b6e236dcebb7bb8477da1e304e0ab54','region-photos/ellerslieHall.jpg',NULL),(13,'SilverStripe\\Assets\\Image','2018-10-12 13:11:24','2018-10-12 13:11:20',2,'Inherit','Inherit','remuera.jpg','remuera',1,11,1,'18fbca242d4c31398f1e5e75bb8a608011a4e398','region-photos/remuera.jpg',NULL),(14,'SilverStripe\\Assets\\Folder','2018-12-06 15:48:49','2018-12-06 15:48:49',2,'Inherit','Inherit','property-photos','property-photos',1,0,1,NULL,NULL,NULL),(15,'SilverStripe\\Assets\\Image','2018-12-06 16:33:37','2018-12-06 15:53:05',2,'Inherit','Inherit','house1.jpeg','house1',1,14,1,'565a188fcfc4bfc77c422e62cc560879d4e8867b','property-photos/house1.jpeg',NULL),(16,'SilverStripe\\Assets\\Image','2018-12-06 15:53:07','2018-12-06 15:53:07',1,'Inherit','Inherit','house5.jpeg','house5',1,14,1,'add674d79689c3405aedd8525a6a0490bfac4533','property-photos/house5.jpeg',NULL),(17,'SilverStripe\\Assets\\Image','2018-12-06 16:33:58','2018-12-06 15:53:08',2,'Inherit','Inherit','house2.jpeg','house2',1,14,1,'3f53c4c51295590278ac8689c1168f8b1f4aa93b','property-photos/house2.jpeg',NULL),(18,'SilverStripe\\Assets\\Image','2018-12-07 12:27:17','2018-12-06 15:53:10',2,'Inherit','Inherit','house3.jpeg','house3',1,14,1,'a64ab10558f93db039eb6bf8e5ae0663be3e4578','property-photos/house3.jpeg',NULL),(19,'SilverStripe\\Assets\\Image','2018-12-07 12:28:13','2018-12-06 15:53:12',2,'Inherit','Inherit','house4.jpeg','house4',1,14,1,'975afe32693d267b743b2ec4819d2454390bd535','property-photos/house4.jpeg',NULL),(20,'SilverStripe\\Assets\\Image','2018-12-07 14:13:32','2018-12-07 14:13:20',2,'Inherit','Inherit','newmarket.jpg','newmarket',1,11,1,'51bc7d83306f7d14523aeb1539df2fb31bfb0a4c','region-photos/newmarket.jpg',NULL);
/*!40000 ALTER TABLE `File` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FileLink`
--

DROP TABLE IF EXISTS `FileLink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FileLink` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Assets\\Shortcodes\\FileLink') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\Shortcodes\\FileLink',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `LinkedID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ParentClass` enum('ArticleCategory','ArticleComment','Property','Region','SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSet','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\Member','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Page','ArticleHolder','ArticlePage','HomePage','PropertySearchPage','RegionsPage','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage') CHARACTER SET utf8 DEFAULT 'ArticleCategory',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `LinkedID` (`LinkedID`),
  KEY `Parent` (`ParentID`,`ParentClass`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FileLink`
--

LOCK TABLES `FileLink` WRITE;
/*!40000 ALTER TABLE `FileLink` DISABLE KEYS */;
/*!40000 ALTER TABLE `FileLink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `File_EditorGroups`
--

DROP TABLE IF EXISTS `File_EditorGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `File_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FileID` (`FileID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `File_EditorGroups`
--

LOCK TABLES `File_EditorGroups` WRITE;
/*!40000 ALTER TABLE `File_EditorGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `File_EditorGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `File_Live`
--

DROP TABLE IF EXISTS `File_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `File_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Assets\\File','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\File',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `FileHash` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileFilename` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileVariant` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Name` (`Name`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `File_Live`
--

LOCK TABLES `File_Live` WRITE;
/*!40000 ALTER TABLE `File_Live` DISABLE KEYS */;
INSERT INTO `File_Live` VALUES (1,'SilverStripe\\Assets\\Folder','2018-10-11 15:22:42','2018-10-11 15:22:42',2,'Inherit','Inherit','Uploads','Uploads',1,0,1,NULL,NULL,NULL),(4,'SilverStripe\\Assets\\Folder','2018-10-11 15:31:07','2018-10-11 15:31:07',2,'Inherit','Inherit','photos','photos',1,0,1,NULL,NULL,NULL),(5,'SilverStripe\\Assets\\Folder','2018-10-11 15:31:07','2018-10-11 15:31:07',2,'Inherit','Inherit','file-attachments','file-attachments',1,0,1,NULL,NULL,NULL),(6,'SilverStripe\\Assets\\Image','2018-10-11 15:41:55','2018-10-11 15:31:23',3,'Inherit','Inherit','ethan-weil-1093611-unsplash.jpg','ethan weil 1093611 unsplash',1,4,1,'92e724c0f55f3151088f8c531214de7ccc567ecf','photos/ethan-weil-1093611-unsplash.jpg',NULL),(7,'SilverStripe\\Assets\\File','2018-10-11 15:41:28','2018-10-11 15:31:42',3,'Inherit','Inherit','ReferenceCardForMac.pdf','Reference Card For Mac',1,5,1,'bb6db3a1ca64e8b5882880d053aecb770b4da6ac','file-attachments/ReferenceCardForMac.pdf',NULL),(8,'SilverStripe\\Assets\\Image','2018-10-11 16:42:30','2018-10-11 16:42:26',2,'Inherit','Inherit','tristan-pineda-1093539-unsplash.jpg','tristan pineda 1093539 unsplash',1,4,1,'3a35edbac3a4852bdb8e962362704f68a8ca3e38','photos/tristan-pineda-1093539-unsplash.jpg',NULL),(9,'SilverStripe\\Assets\\Image','2018-10-11 16:42:46','2018-10-11 16:42:43',2,'Inherit','Inherit','roman-fox-1093593-unsplash.jpg','roman fox 1093593 unsplash',1,4,1,'d0a986a80a23bbf0f115ac042cbe938539d2f1d2','photos/roman-fox-1093593-unsplash.jpg',NULL),(10,'SilverStripe\\Assets\\Image','2018-10-11 16:43:03','2018-10-11 16:42:59',2,'Inherit','Inherit','leif-niemczik-1093522-unsplash.jpg','leif niemczik 1093522 unsplash',1,4,1,'2d043a3e0334924ea371a46b040a45657125c450','photos/leif-niemczik-1093522-unsplash.jpg',NULL),(11,'SilverStripe\\Assets\\Folder','2018-10-12 11:38:55','2018-10-12 11:38:55',2,'Inherit','Inherit','region-photos','region-photos',1,0,1,NULL,NULL,NULL),(12,'SilverStripe\\Assets\\Image','2018-10-12 11:39:57','2018-10-12 11:39:52',2,'Inherit','Inherit','ellerslieHall.jpg','ellerslieHall',1,11,1,'cecb016a8b6e236dcebb7bb8477da1e304e0ab54','region-photos/ellerslieHall.jpg',NULL),(13,'SilverStripe\\Assets\\Image','2018-10-12 13:11:24','2018-10-12 13:11:20',2,'Inherit','Inherit','remuera.jpg','remuera',1,11,1,'18fbca242d4c31398f1e5e75bb8a608011a4e398','region-photos/remuera.jpg',NULL),(14,'SilverStripe\\Assets\\Folder','2018-12-06 15:48:49','2018-12-06 15:48:49',2,'Inherit','Inherit','property-photos','property-photos',1,0,1,NULL,NULL,NULL),(15,'SilverStripe\\Assets\\Image','2018-12-06 16:33:37','2018-12-06 15:53:05',2,'Inherit','Inherit','house1.jpeg','house1',1,14,1,'565a188fcfc4bfc77c422e62cc560879d4e8867b','property-photos/house1.jpeg',NULL),(17,'SilverStripe\\Assets\\Image','2018-12-06 16:33:58','2018-12-06 15:53:08',2,'Inherit','Inherit','house2.jpeg','house2',1,14,1,'3f53c4c51295590278ac8689c1168f8b1f4aa93b','property-photos/house2.jpeg',NULL),(18,'SilverStripe\\Assets\\Image','2018-12-07 12:27:17','2018-12-06 15:53:10',2,'Inherit','Inherit','house3.jpeg','house3',1,14,1,'a64ab10558f93db039eb6bf8e5ae0663be3e4578','property-photos/house3.jpeg',NULL),(19,'SilverStripe\\Assets\\Image','2018-12-07 12:28:13','2018-12-06 15:53:12',2,'Inherit','Inherit','house4.jpeg','house4',1,14,1,'975afe32693d267b743b2ec4819d2454390bd535','property-photos/house4.jpeg',NULL),(20,'SilverStripe\\Assets\\Image','2018-12-07 14:13:32','2018-12-07 14:13:20',2,'Inherit','Inherit','newmarket.jpg','newmarket',1,11,1,'51bc7d83306f7d14523aeb1539df2fb31bfb0a4c','region-photos/newmarket.jpg',NULL);
/*!40000 ALTER TABLE `File_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `File_Versions`
--

DROP TABLE IF EXISTS `File_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `File_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SilverStripe\\Assets\\File','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Assets\\File',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  `FileHash` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileFilename` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FileVariant` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `Name` (`Name`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `File_Versions`
--

LOCK TABLES `File_Versions` WRITE;
/*!40000 ALTER TABLE `File_Versions` DISABLE KEYS */;
INSERT INTO `File_Versions` VALUES (1,1,1,0,0,1,1,0,'SilverStripe\\Assets\\Folder','2018-10-11 15:22:42','2018-10-11 15:22:42','Inherit','Inherit','Uploads','Uploads',1,0,1,NULL,NULL,NULL),(2,1,2,1,0,1,1,1,'SilverStripe\\Assets\\Folder','2018-10-11 15:22:42','2018-10-11 15:22:42','Inherit','Inherit','Uploads','Uploads',1,0,1,NULL,NULL,NULL),(3,2,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-10-11 15:23:41','2018-10-11 15:23:41','Inherit','Inherit','ethan-weil-1093611-unsplash.jpg','ethan weil 1093611 unsplash',1,1,1,'92e724c0f55f3151088f8c531214de7ccc567ecf','Uploads/ethan-weil-1093611-unsplash.jpg',NULL),(4,3,1,0,0,1,1,0,'SilverStripe\\Assets\\File','2018-10-11 15:24:00','2018-10-11 15:24:00','Inherit','Inherit','ReferenceCardForMac.pdf','ReferenceCardForMac',1,1,1,'bb6db3a1ca64e8b5882880d053aecb770b4da6ac','Uploads/ReferenceCardForMac.pdf',NULL),(5,2,2,1,1,1,1,1,'SilverStripe\\Assets\\Image','2018-10-11 15:30:56','2018-10-11 15:23:41','Inherit','Inherit',NULL,NULL,1,0,0,NULL,NULL,NULL),(6,3,2,1,1,1,1,1,'SilverStripe\\Assets\\File','2018-10-11 15:30:56','2018-10-11 15:24:00','Inherit','Inherit',NULL,NULL,1,0,0,NULL,NULL,NULL),(7,4,1,0,0,1,1,0,'SilverStripe\\Assets\\Folder','2018-10-11 15:31:07','2018-10-11 15:31:07','Inherit','Inherit','photos','photos',1,0,1,NULL,NULL,NULL),(8,4,2,1,0,1,1,1,'SilverStripe\\Assets\\Folder','2018-10-11 15:31:07','2018-10-11 15:31:07','Inherit','Inherit','photos','photos',1,0,1,NULL,NULL,NULL),(9,5,1,0,0,1,1,0,'SilverStripe\\Assets\\Folder','2018-10-11 15:31:07','2018-10-11 15:31:07','Inherit','Inherit','file-attachments','file-attachments',1,0,1,NULL,NULL,NULL),(10,5,2,1,0,1,1,1,'SilverStripe\\Assets\\Folder','2018-10-11 15:31:07','2018-10-11 15:31:07','Inherit','Inherit','file-attachments','file-attachments',1,0,1,NULL,NULL,NULL),(11,6,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-10-11 15:31:23','2018-10-11 15:31:23','Inherit','Inherit','ethan-weil-1093611-unsplash.jpg','ethan weil 1093611 unsplash',1,4,1,'92e724c0f55f3151088f8c531214de7ccc567ecf','photos/ethan-weil-1093611-unsplash.jpg',NULL),(12,7,1,0,0,1,1,0,'SilverStripe\\Assets\\File','2018-10-11 15:31:42','2018-10-11 15:31:42','Inherit','Inherit','ReferenceCardForMac.pdf','ReferenceCardForMac',1,5,1,'bb6db3a1ca64e8b5882880d053aecb770b4da6ac','file-attachments/ReferenceCardForMac.pdf',NULL),(13,7,2,0,0,1,1,0,'SilverStripe\\Assets\\File','2018-10-11 15:41:28','2018-10-11 15:31:42','Inherit','Inherit','ReferenceCardForMac.pdf','Reference Card For Mac',1,5,1,'bb6db3a1ca64e8b5882880d053aecb770b4da6ac','file-attachments/ReferenceCardForMac.pdf',NULL),(14,7,3,1,0,1,1,1,'SilverStripe\\Assets\\File','2018-10-11 15:41:28','2018-10-11 15:31:42','Inherit','Inherit','ReferenceCardForMac.pdf','Reference Card For Mac',1,5,1,'bb6db3a1ca64e8b5882880d053aecb770b4da6ac','file-attachments/ReferenceCardForMac.pdf',NULL),(15,6,2,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-10-11 15:41:55','2018-10-11 15:31:23','Inherit','Inherit','ethan-weil-1093611-unsplash.jpg','ethan weil 1093611 unsplash',1,4,1,'92e724c0f55f3151088f8c531214de7ccc567ecf','photos/ethan-weil-1093611-unsplash.jpg',NULL),(16,6,3,1,0,1,1,1,'SilverStripe\\Assets\\Image','2018-10-11 15:41:55','2018-10-11 15:31:23','Inherit','Inherit','ethan-weil-1093611-unsplash.jpg','ethan weil 1093611 unsplash',1,4,1,'92e724c0f55f3151088f8c531214de7ccc567ecf','photos/ethan-weil-1093611-unsplash.jpg',NULL),(17,8,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-10-11 16:42:26','2018-10-11 16:42:26','Inherit','Inherit','tristan-pineda-1093539-unsplash.jpg','tristan pineda 1093539 unsplash',1,4,1,'3a35edbac3a4852bdb8e962362704f68a8ca3e38','photos/tristan-pineda-1093539-unsplash.jpg',NULL),(18,8,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2018-10-11 16:42:30','2018-10-11 16:42:26','Inherit','Inherit','tristan-pineda-1093539-unsplash.jpg','tristan pineda 1093539 unsplash',1,4,1,'3a35edbac3a4852bdb8e962362704f68a8ca3e38','photos/tristan-pineda-1093539-unsplash.jpg',NULL),(19,9,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-10-11 16:42:43','2018-10-11 16:42:43','Inherit','Inherit','roman-fox-1093593-unsplash.jpg','roman fox 1093593 unsplash',1,4,1,'d0a986a80a23bbf0f115ac042cbe938539d2f1d2','photos/roman-fox-1093593-unsplash.jpg',NULL),(20,9,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2018-10-11 16:42:46','2018-10-11 16:42:43','Inherit','Inherit','roman-fox-1093593-unsplash.jpg','roman fox 1093593 unsplash',1,4,1,'d0a986a80a23bbf0f115ac042cbe938539d2f1d2','photos/roman-fox-1093593-unsplash.jpg',NULL),(21,10,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-10-11 16:42:59','2018-10-11 16:42:59','Inherit','Inherit','leif-niemczik-1093522-unsplash.jpg','leif niemczik 1093522 unsplash',1,4,1,'2d043a3e0334924ea371a46b040a45657125c450','photos/leif-niemczik-1093522-unsplash.jpg',NULL),(22,10,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2018-10-11 16:43:03','2018-10-11 16:42:59','Inherit','Inherit','leif-niemczik-1093522-unsplash.jpg','leif niemczik 1093522 unsplash',1,4,1,'2d043a3e0334924ea371a46b040a45657125c450','photos/leif-niemczik-1093522-unsplash.jpg',NULL),(23,11,1,0,0,1,1,0,'SilverStripe\\Assets\\Folder','2018-10-12 11:38:55','2018-10-12 11:38:55','Inherit','Inherit','region-photos','region-photos',1,0,1,NULL,NULL,NULL),(24,11,2,1,0,1,1,1,'SilverStripe\\Assets\\Folder','2018-10-12 11:38:55','2018-10-12 11:38:55','Inherit','Inherit','region-photos','region-photos',1,0,1,NULL,NULL,NULL),(25,12,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-10-12 11:39:52','2018-10-12 11:39:52','Inherit','Inherit','ellerslieHall.jpg','ellerslieHall',1,11,1,'cecb016a8b6e236dcebb7bb8477da1e304e0ab54','region-photos/ellerslieHall.jpg',NULL),(26,12,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2018-10-12 11:39:57','2018-10-12 11:39:52','Inherit','Inherit','ellerslieHall.jpg','ellerslieHall',1,11,1,'cecb016a8b6e236dcebb7bb8477da1e304e0ab54','region-photos/ellerslieHall.jpg',NULL),(27,13,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-10-12 13:11:20','2018-10-12 13:11:20','Inherit','Inherit','remuera.jpg','remuera',1,11,1,'18fbca242d4c31398f1e5e75bb8a608011a4e398','region-photos/remuera.jpg',NULL),(28,13,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2018-10-12 13:11:24','2018-10-12 13:11:20','Inherit','Inherit','remuera.jpg','remuera',1,11,1,'18fbca242d4c31398f1e5e75bb8a608011a4e398','region-photos/remuera.jpg',NULL),(29,14,1,0,0,1,1,0,'SilverStripe\\Assets\\Folder','2018-12-06 15:48:49','2018-12-06 15:48:49','Inherit','Inherit','property-photos','property-photos',1,0,1,NULL,NULL,NULL),(30,14,2,1,0,1,1,1,'SilverStripe\\Assets\\Folder','2018-12-06 15:48:49','2018-12-06 15:48:49','Inherit','Inherit','property-photos','property-photos',1,0,1,NULL,NULL,NULL),(31,15,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-12-06 15:53:05','2018-12-06 15:53:05','Inherit','Inherit','house1.jpeg','house1',1,14,1,'565a188fcfc4bfc77c422e62cc560879d4e8867b','property-photos/house1.jpeg',NULL),(32,16,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-12-06 15:53:07','2018-12-06 15:53:07','Inherit','Inherit','house5.jpeg','house5',1,14,1,'add674d79689c3405aedd8525a6a0490bfac4533','property-photos/house5.jpeg',NULL),(33,17,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-12-06 15:53:08','2018-12-06 15:53:08','Inherit','Inherit','house2.jpeg','house2',1,14,1,'3f53c4c51295590278ac8689c1168f8b1f4aa93b','property-photos/house2.jpeg',NULL),(34,18,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-12-06 15:53:10','2018-12-06 15:53:10','Inherit','Inherit','house3.jpeg','house3',1,14,1,'a64ab10558f93db039eb6bf8e5ae0663be3e4578','property-photos/house3.jpeg',NULL),(35,19,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-12-06 15:53:12','2018-12-06 15:53:12','Inherit','Inherit','house4.jpeg','house4',1,14,1,'975afe32693d267b743b2ec4819d2454390bd535','property-photos/house4.jpeg',NULL),(36,15,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2018-12-06 16:33:37','2018-12-06 15:53:05','Inherit','Inherit','house1.jpeg','house1',1,14,1,'565a188fcfc4bfc77c422e62cc560879d4e8867b','property-photos/house1.jpeg',NULL),(37,17,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2018-12-06 16:33:58','2018-12-06 15:53:08','Inherit','Inherit','house2.jpeg','house2',1,14,1,'3f53c4c51295590278ac8689c1168f8b1f4aa93b','property-photos/house2.jpeg',NULL),(38,18,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2018-12-07 12:27:17','2018-12-06 15:53:10','Inherit','Inherit','house3.jpeg','house3',1,14,1,'a64ab10558f93db039eb6bf8e5ae0663be3e4578','property-photos/house3.jpeg',NULL),(39,19,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2018-12-07 12:28:13','2018-12-06 15:53:12','Inherit','Inherit','house4.jpeg','house4',1,14,1,'975afe32693d267b743b2ec4819d2454390bd535','property-photos/house4.jpeg',NULL),(40,20,1,0,0,1,1,0,'SilverStripe\\Assets\\Image','2018-12-07 14:13:20','2018-12-07 14:13:20','Inherit','Inherit','newmarket.jpg','newmarket',1,11,1,'51bc7d83306f7d14523aeb1539df2fb31bfb0a4c','region-photos/newmarket.jpg',NULL),(41,20,2,1,0,1,1,1,'SilverStripe\\Assets\\Image','2018-12-07 14:13:32','2018-12-07 14:13:20','Inherit','Inherit','newmarket.jpg','newmarket',1,11,1,'51bc7d83306f7d14523aeb1539df2fb31bfb0a4c','region-photos/newmarket.jpg',NULL);
/*!40000 ALTER TABLE `File_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `File_ViewerGroups`
--

DROP TABLE IF EXISTS `File_ViewerGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `File_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FileID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FileID` (`FileID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `File_ViewerGroups`
--

LOCK TABLES `File_ViewerGroups` WRITE;
/*!40000 ALTER TABLE `File_ViewerGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `File_ViewerGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Group`
--

DROP TABLE IF EXISTS `Group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\Group') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\Group',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HtmlEditorConfig` mediumtext CHARACTER SET utf8,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Group`
--

LOCK TABLES `Group` WRITE;
/*!40000 ALTER TABLE `Group` DISABLE KEYS */;
INSERT INTO `Group` VALUES (1,'SilverStripe\\Security\\Group','2018-10-10 15:08:06','2018-10-10 15:08:06','Content Authors',NULL,'content-authors',0,1,NULL,0),(2,'SilverStripe\\Security\\Group','2018-10-10 15:08:06','2018-10-10 15:08:06','Administrators',NULL,'administrators',0,0,NULL,0);
/*!40000 ALTER TABLE `Group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Group_Members`
--

DROP TABLE IF EXISTS `Group_Members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Group_Members` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Group_Members`
--

LOCK TABLES `Group_Members` WRITE;
/*!40000 ALTER TABLE `Group_Members` DISABLE KEYS */;
INSERT INTO `Group_Members` VALUES (1,2,1);
/*!40000 ALTER TABLE `Group_Members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Group_Roles`
--

DROP TABLE IF EXISTS `Group_Roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Group_Roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `PermissionRoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `PermissionRoleID` (`PermissionRoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Group_Roles`
--

LOCK TABLES `Group_Roles` WRITE;
/*!40000 ALTER TABLE `Group_Roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `Group_Roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LoginAttempt`
--

DROP TABLE IF EXISTS `LoginAttempt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LoginAttempt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\LoginAttempt') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\LoginAttempt',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `EmailHashed` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Status` enum('Success','Failure') CHARACTER SET utf8 DEFAULT 'Success',
  `IP` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LoginAttempt`
--

LOCK TABLES `LoginAttempt` WRITE;
/*!40000 ALTER TABLE `LoginAttempt` DISABLE KEYS */;
INSERT INTO `LoginAttempt` VALUES (1,'SilverStripe\\Security\\LoginAttempt','2018-10-10 15:08:42','2018-10-10 15:08:42',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.17.0.1',1),(2,'SilverStripe\\Security\\LoginAttempt','2018-10-10 16:27:26','2018-10-10 16:27:26',NULL,'da39a3ee5e6b4b0d3255bfef95601890afd80709','Success','172.17.0.1',1),(3,'SilverStripe\\Security\\LoginAttempt','2018-10-10 16:31:43','2018-10-10 16:31:43',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.17.0.1',1),(4,'SilverStripe\\Security\\LoginAttempt','2018-10-11 16:18:05','2018-10-11 16:18:05',NULL,'da39a3ee5e6b4b0d3255bfef95601890afd80709','Success','172.17.0.1',1),(5,'SilverStripe\\Security\\LoginAttempt','2018-10-12 10:38:51','2018-10-12 10:38:51',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.17.0.1',1),(6,'SilverStripe\\Security\\LoginAttempt','2018-10-17 15:35:51','2018-10-17 15:35:51',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.17.0.1',1),(7,'SilverStripe\\Security\\LoginAttempt','2018-11-26 16:43:16','2018-11-26 16:43:16',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.17.0.1',1),(8,'SilverStripe\\Security\\LoginAttempt','2018-11-29 13:58:51','2018-11-29 13:58:51',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.17.0.1',1),(9,'SilverStripe\\Security\\LoginAttempt','2018-12-06 14:24:37','2018-12-06 14:24:37',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.17.0.1',1),(10,'SilverStripe\\Security\\LoginAttempt','2018-12-07 12:21:47','2018-12-07 12:21:47',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.17.0.1',1),(11,'SilverStripe\\Security\\LoginAttempt','2018-12-07 14:11:26','2018-12-07 14:11:26',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.17.0.1',1),(12,'SilverStripe\\Security\\LoginAttempt','2019-01-15 16:18:32','2019-01-15 16:18:32',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.17.0.1',1),(13,'SilverStripe\\Security\\LoginAttempt','2019-01-17 10:25:16','2019-01-17 10:25:16',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.17.0.1',1),(14,'SilverStripe\\Security\\LoginAttempt','2019-01-18 13:10:29','2019-01-18 13:10:29',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','Success','172.17.0.1',1);
/*!40000 ALTER TABLE `LoginAttempt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Member`
--

DROP TABLE IF EXISTS `Member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Member` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\Member') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\Member',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `FirstName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Surname` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `TempIDHash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `TempIDExpired` datetime DEFAULT NULL,
  `Password` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `AutoLoginHash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `AutoLoginExpired` datetime DEFAULT NULL,
  `PasswordEncryption` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Salt` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PasswordExpiry` date DEFAULT NULL,
  `LockedOutUntil` datetime DEFAULT NULL,
  `Locale` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `FailedLoginCount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Surname` (`Surname`),
  KEY `FirstName` (`FirstName`),
  KEY `ClassName` (`ClassName`),
  KEY `Email` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Member`
--

LOCK TABLES `Member` WRITE;
/*!40000 ALTER TABLE `Member` DISABLE KEYS */;
INSERT INTO `Member` VALUES (1,'SilverStripe\\Security\\Member','2019-01-18 13:10:30','2018-10-10 15:08:07','Default Admin',NULL,'admin','37e83a5b072606b89236160f0a45f0b76ca45894','2019-01-21 13:10:30','$2y$10$c5cac4111f61236ba7138OjmUmARpFsQg2EVwqz/cv3t7N/M5wr32',NULL,NULL,'blowfish','10$c5cac4111f61236ba7138a',NULL,NULL,'en_GB',0);
/*!40000 ALTER TABLE `Member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MemberPassword`
--

DROP TABLE IF EXISTS `MemberPassword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MemberPassword` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\MemberPassword') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\MemberPassword',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Password` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `Salt` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PasswordEncryption` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MemberPassword`
--

LOCK TABLES `MemberPassword` WRITE;
/*!40000 ALTER TABLE `MemberPassword` DISABLE KEYS */;
INSERT INTO `MemberPassword` VALUES (1,'SilverStripe\\Security\\MemberPassword','2018-10-10 15:08:07','2018-10-10 15:08:07',NULL,NULL,'none',1),(2,'SilverStripe\\Security\\MemberPassword','2018-10-10 15:08:07','2018-10-10 15:08:07','$2y$10$c5cac4111f61236ba7138OjmUmARpFsQg2EVwqz/cv3t7N/M5wr32','10$c5cac4111f61236ba7138a','blowfish',1);
/*!40000 ALTER TABLE `MemberPassword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Permission`
--

DROP TABLE IF EXISTS `Permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\Permission') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\Permission',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Arg` int(11) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL DEFAULT '1',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `GroupID` (`GroupID`),
  KEY `Code` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Permission`
--

LOCK TABLES `Permission` WRITE;
/*!40000 ALTER TABLE `Permission` DISABLE KEYS */;
INSERT INTO `Permission` VALUES (1,'SilverStripe\\Security\\Permission','2018-10-10 15:08:06','2018-10-10 15:08:06','CMS_ACCESS_CMSMain',0,1,1),(2,'SilverStripe\\Security\\Permission','2018-10-10 15:08:06','2018-10-10 15:08:06','CMS_ACCESS_AssetAdmin',0,1,1),(3,'SilverStripe\\Security\\Permission','2018-10-10 15:08:06','2018-10-10 15:08:06','CMS_ACCESS_ReportAdmin',0,1,1),(4,'SilverStripe\\Security\\Permission','2018-10-10 15:08:06','2018-10-10 15:08:06','SITETREE_REORGANISE',0,1,1),(5,'SilverStripe\\Security\\Permission','2018-10-10 15:08:06','2018-10-10 15:08:06','ADMIN',0,1,2);
/*!40000 ALTER TABLE `Permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PermissionRole`
--

DROP TABLE IF EXISTS `PermissionRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PermissionRole` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\PermissionRole') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\PermissionRole',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `OnlyAdminCanApply` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Title` (`Title`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PermissionRole`
--

LOCK TABLES `PermissionRole` WRITE;
/*!40000 ALTER TABLE `PermissionRole` DISABLE KEYS */;
/*!40000 ALTER TABLE `PermissionRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PermissionRoleCode`
--

DROP TABLE IF EXISTS `PermissionRoleCode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PermissionRoleCode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\PermissionRoleCode') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\PermissionRoleCode',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `RoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `RoleID` (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PermissionRoleCode`
--

LOCK TABLES `PermissionRoleCode` WRITE;
/*!40000 ALTER TABLE `PermissionRoleCode` DISABLE KEYS */;
/*!40000 ALTER TABLE `PermissionRoleCode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Property`
--

DROP TABLE IF EXISTS `Property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Property` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Property') CHARACTER SET utf8 DEFAULT 'Property',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PricePerNight` decimal(9,2) NOT NULL DEFAULT '0.00',
  `Bedrooms` int(11) NOT NULL DEFAULT '0',
  `Bathrooms` int(11) NOT NULL DEFAULT '0',
  `FeaturedOnHomepage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `RegionID` int(11) NOT NULL DEFAULT '0',
  `PrimaryPhotoID` int(11) NOT NULL DEFAULT '0',
  `AvailableStart` date DEFAULT NULL,
  `AvailableEnd` date DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `RegionID` (`RegionID`),
  KEY `PrimaryPhotoID` (`PrimaryPhotoID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Property`
--

LOCK TABLES `Property` WRITE;
/*!40000 ALTER TABLE `Property` DISABLE KEYS */;
INSERT INTO `Property` VALUES (1,'Property','2019-01-17 10:27:50','2018-12-07 12:25:19',6,'Property 1',100.00,1,1,1,1,15,'2019-01-01','2019-01-29',NULL),(2,'Property','2019-01-17 10:28:27','2018-12-07 12:26:25',8,'Property 2',150.00,2,1,1,2,17,'2019-02-01','2019-02-28',NULL),(3,'Property','2019-01-17 10:28:56','2018-12-07 12:27:06',6,'Property 3',200.00,3,2,1,1,18,'2019-01-10','2019-01-20',NULL),(4,'Property','2019-01-17 10:29:22','2018-12-07 12:27:45',6,'Property 4',175.00,2,2,1,2,19,'2019-02-05','2019-02-15',NULL);
/*!40000 ALTER TABLE `Property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Property_Live`
--

DROP TABLE IF EXISTS `Property_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Property_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Property') CHARACTER SET utf8 DEFAULT 'Property',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PricePerNight` decimal(9,2) NOT NULL DEFAULT '0.00',
  `Bedrooms` int(11) NOT NULL DEFAULT '0',
  `Bathrooms` int(11) NOT NULL DEFAULT '0',
  `RegionID` int(11) NOT NULL DEFAULT '0',
  `PrimaryPhotoID` int(11) NOT NULL DEFAULT '0',
  `FeaturedOnHomepage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AvailableStart` date DEFAULT NULL,
  `AvailableEnd` date DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `RegionID` (`RegionID`),
  KEY `PrimaryPhotoID` (`PrimaryPhotoID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Property_Live`
--

LOCK TABLES `Property_Live` WRITE;
/*!40000 ALTER TABLE `Property_Live` DISABLE KEYS */;
INSERT INTO `Property_Live` VALUES (1,'Property','2019-01-17 10:27:50','2018-12-07 12:25:19',6,'Property 1',100.00,1,1,1,15,1,'2019-01-01','2019-01-29',NULL),(2,'Property','2019-01-17 10:28:27','2018-12-07 12:26:25',8,'Property 2',150.00,2,1,2,17,1,'2019-02-01','2019-02-28',NULL),(3,'Property','2019-01-17 10:28:56','2018-12-07 12:27:06',6,'Property 3',200.00,3,2,1,18,1,'2019-01-10','2019-01-20',NULL),(4,'Property','2019-01-17 10:29:22','2018-12-07 12:27:45',6,'Property 4',175.00,2,2,2,19,1,'2019-02-05','2019-02-15',NULL);
/*!40000 ALTER TABLE `Property_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Property_Versions`
--

DROP TABLE IF EXISTS `Property_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Property_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('Property') CHARACTER SET utf8 DEFAULT 'Property',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `PricePerNight` decimal(9,2) NOT NULL DEFAULT '0.00',
  `Bedrooms` int(11) NOT NULL DEFAULT '0',
  `Bathrooms` int(11) NOT NULL DEFAULT '0',
  `RegionID` int(11) NOT NULL DEFAULT '0',
  `PrimaryPhotoID` int(11) NOT NULL DEFAULT '0',
  `FeaturedOnHomepage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AvailableStart` date DEFAULT NULL,
  `AvailableEnd` date DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ClassName` (`ClassName`),
  KEY `RegionID` (`RegionID`),
  KEY `PrimaryPhotoID` (`PrimaryPhotoID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Property_Versions`
--

LOCK TABLES `Property_Versions` WRITE;
/*!40000 ALTER TABLE `Property_Versions` DISABLE KEYS */;
INSERT INTO `Property_Versions` VALUES (1,1,1,0,0,1,1,0,'Property','2018-12-07 12:25:19','2018-12-07 12:25:19','Property 1',100.00,1,1,1,0,1,NULL,NULL,NULL),(2,1,2,1,0,1,1,1,'Property','2018-12-07 12:25:19','2018-12-07 12:25:19','Property 1',100.00,1,1,1,0,1,NULL,NULL,NULL),(3,1,3,0,0,1,1,0,'Property','2018-12-07 12:25:39','2018-12-07 12:25:19','Property 1',100.00,1,1,1,15,1,NULL,NULL,NULL),(4,1,4,1,0,1,1,1,'Property','2018-12-07 12:25:39','2018-12-07 12:25:19','Property 1',100.00,1,1,1,15,1,NULL,NULL,NULL),(5,2,1,0,0,1,1,0,'Property','2018-12-07 12:26:25','2018-12-07 12:26:25','Property 2',150.00,2,1,2,0,1,NULL,NULL,NULL),(6,2,2,1,0,1,1,1,'Property','2018-12-07 12:26:25','2018-12-07 12:26:25','Property 2',150.00,2,1,2,0,1,NULL,NULL,NULL),(7,2,3,0,0,1,1,0,'Property','2018-12-07 12:26:39','2018-12-07 12:26:25','Property 2',150.00,2,1,2,17,1,NULL,NULL,NULL),(8,2,4,1,0,1,1,1,'Property','2018-12-07 12:26:39','2018-12-07 12:26:25','Property 2',150.00,2,1,2,17,1,NULL,NULL,NULL),(9,3,1,0,0,1,1,0,'Property','2018-12-07 12:27:06','2018-12-07 12:27:06','Property 3',200.00,3,2,1,0,1,NULL,NULL,NULL),(10,3,2,1,0,1,1,1,'Property','2018-12-07 12:27:06','2018-12-07 12:27:06','Property 3',200.00,3,2,1,0,1,NULL,NULL,NULL),(11,3,3,0,0,1,1,0,'Property','2018-12-07 12:27:17','2018-12-07 12:27:06','Property 3',200.00,3,2,1,18,1,NULL,NULL,NULL),(12,3,4,1,0,1,1,1,'Property','2018-12-07 12:27:17','2018-12-07 12:27:06','Property 3',200.00,3,2,1,18,1,NULL,NULL,NULL),(13,4,1,0,0,1,1,0,'Property','2018-12-07 12:27:45','2018-12-07 12:27:45','Property 4',175.00,2,2,2,0,1,NULL,NULL,NULL),(14,4,2,1,0,1,1,1,'Property','2018-12-07 12:27:45','2018-12-07 12:27:45','Property 4',175.00,2,2,2,0,1,NULL,NULL,NULL),(15,4,3,0,0,1,1,0,'Property','2018-12-07 12:28:13','2018-12-07 12:27:45','Property 4',175.00,2,2,2,19,1,NULL,NULL,NULL),(16,4,4,1,0,1,1,1,'Property','2018-12-07 12:28:13','2018-12-07 12:27:45','Property 4',175.00,2,2,2,19,1,NULL,NULL,NULL),(17,1,5,0,0,1,1,0,'Property','2019-01-17 10:27:50','2018-12-07 12:25:19','Property 1',100.00,1,1,1,15,1,'2019-01-01','2019-01-29',NULL),(18,1,6,1,0,1,1,1,'Property','2019-01-17 10:27:50','2018-12-07 12:25:19','Property 1',100.00,1,1,1,15,1,'2019-01-01','2019-01-29',NULL),(19,2,5,0,0,1,1,0,'Property','2019-01-17 10:28:13','2018-12-07 12:26:25','Property 2',150.00,2,1,2,17,1,'2019-02-01',NULL,NULL),(20,2,6,1,0,1,1,1,'Property','2019-01-17 10:28:13','2018-12-07 12:26:25','Property 2',150.00,2,1,2,17,1,'2019-02-01',NULL,NULL),(21,2,7,0,0,1,1,0,'Property','2019-01-17 10:28:27','2018-12-07 12:26:25','Property 2',150.00,2,1,2,17,1,'2019-02-01','2019-02-28',NULL),(22,2,8,1,0,1,1,1,'Property','2019-01-17 10:28:27','2018-12-07 12:26:25','Property 2',150.00,2,1,2,17,1,'2019-02-01','2019-02-28',NULL),(23,3,5,0,0,1,1,0,'Property','2019-01-17 10:28:56','2018-12-07 12:27:06','Property 3',200.00,3,2,1,18,1,'2019-01-10','2019-01-20',NULL),(24,3,6,1,0,1,1,1,'Property','2019-01-17 10:28:56','2018-12-07 12:27:06','Property 3',200.00,3,2,1,18,1,'2019-01-10','2019-01-20',NULL),(25,4,5,0,0,1,1,0,'Property','2019-01-17 10:29:22','2018-12-07 12:27:45','Property 4',175.00,2,2,2,19,1,'2019-02-05','2019-02-15',NULL),(26,4,6,1,0,1,1,1,'Property','2019-01-17 10:29:22','2018-12-07 12:27:45','Property 4',175.00,2,2,2,19,1,'2019-02-05','2019-02-15',NULL);
/*!40000 ALTER TABLE `Property_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RedirectorPage`
--

DROP TABLE IF EXISTS `RedirectorPage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RedirectorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RedirectorPage`
--

LOCK TABLES `RedirectorPage` WRITE;
/*!40000 ALTER TABLE `RedirectorPage` DISABLE KEYS */;
/*!40000 ALTER TABLE `RedirectorPage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RedirectorPage_Live`
--

DROP TABLE IF EXISTS `RedirectorPage_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RedirectorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RedirectorPage_Live`
--

LOCK TABLES `RedirectorPage_Live` WRITE;
/*!40000 ALTER TABLE `RedirectorPage_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `RedirectorPage_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RedirectorPage_Versions`
--

DROP TABLE IF EXISTS `RedirectorPage_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RedirectorPage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RedirectorPage_Versions`
--

LOCK TABLES `RedirectorPage_Versions` WRITE;
/*!40000 ALTER TABLE `RedirectorPage_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `RedirectorPage_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Region`
--

DROP TABLE IF EXISTS `Region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Region` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Region') CHARACTER SET utf8 DEFAULT 'Region',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `RegionsPageID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `PhotoID` (`PhotoID`),
  KEY `RegionsPageID` (`RegionsPageID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Region`
--

LOCK TABLES `Region` WRITE;
/*!40000 ALTER TABLE `Region` DISABLE KEYS */;
INSERT INTO `Region` VALUES (1,'Region','2019-01-18 13:38:24','2018-10-12 11:39:57','Ellerslie','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt, est quis finibus posuere, nunc felis egestas ex, in facilisis ipsum lectus a massa. Aliquam mattis eros sed pellentesque tincidunt. Vivamus interdum fringilla ultricies.</p><p>Maecenas maximus augue eu elit ultricies sagittis. Curabitur at est eleifend, scelerisque risus sed, ullamcorper nisl. Integer vitae suscipit sapien. Morbi vel arcu sapien. Pellentesque in dapibus purus, ut facilisis odio. Pellentesque commodo felis et lectus molestie, sed lacinia dolor mollis. Integer vitae erat et ante consequat efficitur. Integer et velit vel ipsum fringilla interdum. Ut vel quam sed elit luctus aliquam. Cras a lectus at turpis pretium auctor. Phasellus porta tristique urna. Duis porttitor et quam at cursus.</p><p>Nulla eleifend, metus id consequat pharetra, dui orci rutrum urna, eget imperdiet risus nisl sit amet odio. Mauris gravida vel enim et bibendum. Ut luctus euismod nunc, vitae cursus sem. Vivamus vehicula vel elit feugiat dictum. Pellentesque vestibulum dictum magna nec aliquet. Suspendisse in nisi quis justo ornare scelerisque. Nulla facilisi. Praesent maximus sed arcu vitae dictum. Cras euismod lacus eu mollis ultrices. Vivamus laoreet nisi rutrum odio malesuada porta. Curabitur lacinia mollis diam, eget luctus sapien cursus eu. Phasellus ac pharetra nunc. Curabitur porttitor massa risus, eu vulputate lacus pharetra nec. Quisque aliquam iaculis ultricies.</p>',12,17,8),(2,'Region','2018-10-12 13:11:24','2018-10-12 13:11:24','Remuera','Generic Text',13,17,2),(3,'Region','2018-12-07 14:13:32','2018-12-07 14:13:32','Newmarket','Generic text about Newmarket',20,17,2);
/*!40000 ALTER TABLE `Region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Region_Live`
--

DROP TABLE IF EXISTS `Region_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Region_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Region') CHARACTER SET utf8 DEFAULT 'Region',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `RegionsPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `PhotoID` (`PhotoID`),
  KEY `RegionsPageID` (`RegionsPageID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Region_Live`
--

LOCK TABLES `Region_Live` WRITE;
/*!40000 ALTER TABLE `Region_Live` DISABLE KEYS */;
INSERT INTO `Region_Live` VALUES (1,'Region','2019-01-18 13:38:24','2018-10-12 11:39:57',8,'Ellerslie','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt, est quis finibus posuere, nunc felis egestas ex, in facilisis ipsum lectus a massa. Aliquam mattis eros sed pellentesque tincidunt. Vivamus interdum fringilla ultricies.</p><p>Maecenas maximus augue eu elit ultricies sagittis. Curabitur at est eleifend, scelerisque risus sed, ullamcorper nisl. Integer vitae suscipit sapien. Morbi vel arcu sapien. Pellentesque in dapibus purus, ut facilisis odio. Pellentesque commodo felis et lectus molestie, sed lacinia dolor mollis. Integer vitae erat et ante consequat efficitur. Integer et velit vel ipsum fringilla interdum. Ut vel quam sed elit luctus aliquam. Cras a lectus at turpis pretium auctor. Phasellus porta tristique urna. Duis porttitor et quam at cursus.</p><p>Nulla eleifend, metus id consequat pharetra, dui orci rutrum urna, eget imperdiet risus nisl sit amet odio. Mauris gravida vel enim et bibendum. Ut luctus euismod nunc, vitae cursus sem. Vivamus vehicula vel elit feugiat dictum. Pellentesque vestibulum dictum magna nec aliquet. Suspendisse in nisi quis justo ornare scelerisque. Nulla facilisi. Praesent maximus sed arcu vitae dictum. Cras euismod lacus eu mollis ultrices. Vivamus laoreet nisi rutrum odio malesuada porta. Curabitur lacinia mollis diam, eget luctus sapien cursus eu. Phasellus ac pharetra nunc. Curabitur porttitor massa risus, eu vulputate lacus pharetra nec. Quisque aliquam iaculis ultricies.</p>',12,17),(2,'Region','2018-10-12 13:11:24','2018-10-12 13:11:24',2,'Remuera','Generic Text',13,17),(3,'Region','2018-12-07 14:13:32','2018-12-07 14:13:32',2,'Newmarket','Generic text about Newmarket',20,17);
/*!40000 ALTER TABLE `Region_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Region_Versions`
--

DROP TABLE IF EXISTS `Region_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Region_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('Region') CHARACTER SET utf8 DEFAULT 'Region',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `RegionsPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ClassName` (`ClassName`),
  KEY `PhotoID` (`PhotoID`),
  KEY `RegionsPageID` (`RegionsPageID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Region_Versions`
--

LOCK TABLES `Region_Versions` WRITE;
/*!40000 ALTER TABLE `Region_Versions` DISABLE KEYS */;
INSERT INTO `Region_Versions` VALUES (1,1,1,0,0,1,1,0,'Region','2018-10-12 11:39:57','2018-10-12 11:39:57','Ellerslie','This is description text for the DataObject Region Ellerslie',12,17),(2,1,2,1,0,1,1,1,'Region','2018-10-12 11:39:57','2018-10-12 11:39:57','Ellerslie','This is description text for the DataObject Region Ellerslie',12,17),(3,2,1,0,0,1,1,0,'Region','2018-10-12 13:11:24','2018-10-12 13:11:24','Remuera','Generic Text',13,17),(4,2,2,1,0,1,1,1,'Region','2018-10-12 13:11:24','2018-10-12 13:11:24','Remuera','Generic Text',13,17),(5,3,1,0,0,1,1,0,'Region','2018-12-07 14:13:32','2018-12-07 14:13:32','Newmarket','Generic text about Newmarket',20,17),(6,3,2,1,0,1,1,1,'Region','2018-12-07 14:13:32','2018-12-07 14:13:32','Newmarket','Generic text about Newmarket',20,17),(7,1,3,0,0,1,1,0,'Region','2018-12-07 14:54:35','2018-10-12 11:39:57','Ellerslie','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt, est quis finibus posuere, nunc felis egestas ex, in facilisis ipsum lectus a massa. Aliquam mattis eros sed pellentesque tincidunt. Vivamus interdum fringilla ultricies.</p><p>Maecenas maximus augue eu elit ultricies sagittis. Curabitur at est eleifend, scelerisque risus sed, ullamcorper nisl. Integer vitae suscipit sapien. Morbi vel arcu sapien. Pellentesque in dapibus purus, ut facilisis odio. Pellentesque commodo felis et lectus molestie, sed lacinia dolor mollis. Integer vitae erat et ante consequat efficitur. Integer et velit vel ipsum fringilla interdum. Ut vel quam sed elit luctus aliquam. Cras a lectus at turpis pretium auctor. Phasellus porta tristique urna. Duis porttitor et quam at cursus.</p><p>Nulla eleifend, metus id consequat pharetra, dui orci rutrum urna, eget imperdiet risus nisl sit amet odio. Mauris gravida vel enim et bibendum. Ut luctus euismod nunc, vitae cursus sem. Vivamus vehicula vel elit feugiat dictum. Pellentesque vestibulum dictum magna nec aliquet. Suspendisse in nisi quis justo ornare scelerisque. Nulla facilisi. Praesent maximus sed arcu vitae dictum. Cras euismod lacus eu mollis ultrices. Vivamus laoreet nisi rutrum odio malesuada porta. Curabitur lacinia mollis diam, eget luctus sapien cursus eu. Phasellus ac pharetra nunc. Curabitur porttitor massa risus, eu vulputate lacus pharetra nec. Quisque aliquam iaculis ultricies.</p>',12,17),(8,1,4,0,0,1,1,0,'Region','2018-12-07 14:54:35','2018-10-12 11:39:57','Ellerslie','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt, est quis finibus posuere, nunc felis egestas ex, in facilisis ipsum lectus a massa. Aliquam mattis eros sed pellentesque tincidunt. Vivamus interdum fringilla ultricies.</p><p>Maecenas maximus augue eu elit ultricies sagittis. Curabitur at est eleifend, scelerisque risus sed, ullamcorper nisl. Integer vitae suscipit sapien. Morbi vel arcu sapien. Pellentesque in dapibus purus, ut facilisis odio. Pellentesque commodo felis et lectus molestie, sed lacinia dolor mollis. Integer vitae erat et ante consequat efficitur. Integer et velit vel ipsum fringilla interdum. Ut vel quam sed elit luctus aliquam. Cras a lectus at turpis pretium auctor. Phasellus porta tristique urna. Duis porttitor et quam at cursus.</p><p>Nulla eleifend, metus id consequat pharetra, dui orci rutrum urna, eget imperdiet risus nisl sit amet odio. Mauris gravida vel enim et bibendum. Ut luctus euismod nunc, vitae cursus sem. Vivamus vehicula vel elit feugiat dictum. Pellentesque vestibulum dictum magna nec aliquet. Suspendisse in nisi quis justo ornare scelerisque. Nulla facilisi. Praesent maximus sed arcu vitae dictum. Cras euismod lacus eu mollis ultrices. Vivamus laoreet nisi rutrum odio malesuada porta. Curabitur lacinia mollis diam, eget luctus sapien cursus eu. Phasellus ac pharetra nunc. Curabitur porttitor massa risus, eu vulputate lacus pharetra nec. Quisque aliquam iaculis ultricies.</p>',12,17),(9,1,5,1,0,1,1,1,'Region','2018-12-07 14:54:35','2018-10-12 11:39:57','Ellerslie','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt, est quis finibus posuere, nunc felis egestas ex, in facilisis ipsum lectus a massa. Aliquam mattis eros sed pellentesque tincidunt. Vivamus interdum fringilla ultricies.</p><p>Maecenas maximus augue eu elit ultricies sagittis. Curabitur at est eleifend, scelerisque risus sed, ullamcorper nisl. Integer vitae suscipit sapien. Morbi vel arcu sapien. Pellentesque in dapibus purus, ut facilisis odio. Pellentesque commodo felis et lectus molestie, sed lacinia dolor mollis. Integer vitae erat et ante consequat efficitur. Integer et velit vel ipsum fringilla interdum. Ut vel quam sed elit luctus aliquam. Cras a lectus at turpis pretium auctor. Phasellus porta tristique urna. Duis porttitor et quam at cursus.</p><p>Nulla eleifend, metus id consequat pharetra, dui orci rutrum urna, eget imperdiet risus nisl sit amet odio. Mauris gravida vel enim et bibendum. Ut luctus euismod nunc, vitae cursus sem. Vivamus vehicula vel elit feugiat dictum. Pellentesque vestibulum dictum magna nec aliquet. Suspendisse in nisi quis justo ornare scelerisque. Nulla facilisi. Praesent maximus sed arcu vitae dictum. Cras euismod lacus eu mollis ultrices. Vivamus laoreet nisi rutrum odio malesuada porta. Curabitur lacinia mollis diam, eget luctus sapien cursus eu. Phasellus ac pharetra nunc. Curabitur porttitor massa risus, eu vulputate lacus pharetra nec. Quisque aliquam iaculis ultricies.</p>',12,17),(10,1,6,0,0,1,1,0,'Region','2019-01-18 13:38:24','2018-10-12 11:39:57','Ellerslie','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt, est quis finibus posuere, nunc felis egestas ex, in facilisis ipsum lectus a massa. Aliquam mattis eros sed pellentesque tincidunt. Vivamus interdum fringilla ultricies.</p><p>Maecenas maximus augue eu elit ultricies sagittis. Curabitur at est eleifend, scelerisque risus sed, ullamcorper nisl. Integer vitae suscipit sapien. Morbi vel arcu sapien. Pellentesque in dapibus purus, ut facilisis odio. Pellentesque commodo felis et lectus molestie, sed lacinia dolor mollis. Integer vitae erat et ante consequat efficitur. Integer et velit vel ipsum fringilla interdum. Ut vel quam sed elit luctus aliquam. Cras a lectus at turpis pretium auctor. Phasellus porta tristique urna. Duis porttitor et quam at cursus.</p><p>Nulla eleifend, metus id consequat pharetra, dui orci rutrum urna, eget imperdiet risus nisl sit amet odio. Mauris gravida vel enim et bibendum. Ut luctus euismod nunc, vitae cursus sem. Vivamus vehicula vel elit feugiat dictum. Pellentesque vestibulum dictum magna nec aliquet. Suspendisse in nisi quis justo ornare scelerisque. Nulla facilisi. Praesent maximus sed arcu vitae dictum. Cras euismod lacus eu mollis ultrices. Vivamus laoreet nisi rutrum odio malesuada porta. Curabitur lacinia mollis diam, eget luctus sapien cursus eu. Phasellus ac pharetra nunc. Curabitur porttitor massa risus, eu vulputate lacus pharetra nec. Quisque aliquam iaculis ultricies.</p>',12,17),(11,1,7,0,0,1,1,0,'Region','2019-01-18 13:38:24','2018-10-12 11:39:57','Ellerslie','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt, est quis finibus posuere, nunc felis egestas ex, in facilisis ipsum lectus a massa. Aliquam mattis eros sed pellentesque tincidunt. Vivamus interdum fringilla ultricies.</p><p>Maecenas maximus augue eu elit ultricies sagittis. Curabitur at est eleifend, scelerisque risus sed, ullamcorper nisl. Integer vitae suscipit sapien. Morbi vel arcu sapien. Pellentesque in dapibus purus, ut facilisis odio. Pellentesque commodo felis et lectus molestie, sed lacinia dolor mollis. Integer vitae erat et ante consequat efficitur. Integer et velit vel ipsum fringilla interdum. Ut vel quam sed elit luctus aliquam. Cras a lectus at turpis pretium auctor. Phasellus porta tristique urna. Duis porttitor et quam at cursus.</p><p>Nulla eleifend, metus id consequat pharetra, dui orci rutrum urna, eget imperdiet risus nisl sit amet odio. Mauris gravida vel enim et bibendum. Ut luctus euismod nunc, vitae cursus sem. Vivamus vehicula vel elit feugiat dictum. Pellentesque vestibulum dictum magna nec aliquet. Suspendisse in nisi quis justo ornare scelerisque. Nulla facilisi. Praesent maximus sed arcu vitae dictum. Cras euismod lacus eu mollis ultrices. Vivamus laoreet nisi rutrum odio malesuada porta. Curabitur lacinia mollis diam, eget luctus sapien cursus eu. Phasellus ac pharetra nunc. Curabitur porttitor massa risus, eu vulputate lacus pharetra nec. Quisque aliquam iaculis ultricies.</p>',12,17),(12,1,8,1,0,1,1,1,'Region','2019-01-18 13:38:24','2018-10-12 11:39:57','Ellerslie','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt, est quis finibus posuere, nunc felis egestas ex, in facilisis ipsum lectus a massa. Aliquam mattis eros sed pellentesque tincidunt. Vivamus interdum fringilla ultricies.</p><p>Maecenas maximus augue eu elit ultricies sagittis. Curabitur at est eleifend, scelerisque risus sed, ullamcorper nisl. Integer vitae suscipit sapien. Morbi vel arcu sapien. Pellentesque in dapibus purus, ut facilisis odio. Pellentesque commodo felis et lectus molestie, sed lacinia dolor mollis. Integer vitae erat et ante consequat efficitur. Integer et velit vel ipsum fringilla interdum. Ut vel quam sed elit luctus aliquam. Cras a lectus at turpis pretium auctor. Phasellus porta tristique urna. Duis porttitor et quam at cursus.</p><p>Nulla eleifend, metus id consequat pharetra, dui orci rutrum urna, eget imperdiet risus nisl sit amet odio. Mauris gravida vel enim et bibendum. Ut luctus euismod nunc, vitae cursus sem. Vivamus vehicula vel elit feugiat dictum. Pellentesque vestibulum dictum magna nec aliquet. Suspendisse in nisi quis justo ornare scelerisque. Nulla facilisi. Praesent maximus sed arcu vitae dictum. Cras euismod lacus eu mollis ultrices. Vivamus laoreet nisi rutrum odio malesuada porta. Curabitur lacinia mollis diam, eget luctus sapien cursus eu. Phasellus ac pharetra nunc. Curabitur porttitor massa risus, eu vulputate lacus pharetra nec. Quisque aliquam iaculis ultricies.</p>',12,17);
/*!40000 ALTER TABLE `Region_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RememberLoginHash`
--

DROP TABLE IF EXISTS `RememberLoginHash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RememberLoginHash` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Security\\RememberLoginHash') CHARACTER SET utf8 DEFAULT 'SilverStripe\\Security\\RememberLoginHash',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `DeviceID` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `Hash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `ExpiryDate` datetime DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `MemberID` (`MemberID`),
  KEY `DeviceID` (`DeviceID`),
  KEY `Hash` (`Hash`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RememberLoginHash`
--

LOCK TABLES `RememberLoginHash` WRITE;
/*!40000 ALTER TABLE `RememberLoginHash` DISABLE KEYS */;
INSERT INTO `RememberLoginHash` VALUES (1,'SilverStripe\\Security\\RememberLoginHash','2018-12-07 14:11:27','2018-12-07 14:11:27','bd27bd4eb413e1fd6f3b1ff60386387b21e0281f','$2y$10$c5cac4111f61236ba7138OPIxx7OTm2uapY0T2lzpz6eotb88v.16','2019-03-07 14:11:27',1);
/*!40000 ALTER TABLE `RememberLoginHash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SilverStripe_Lessons_ArticleCategory`
--

DROP TABLE IF EXISTS `SilverStripe_Lessons_ArticleCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SilverStripe_Lessons_ArticleCategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Lessons\\ArticleCategory') DEFAULT 'SilverStripe\\Lessons\\ArticleCategory',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `ArticleHolderID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `ArticleHolderID` (`ArticleHolderID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SilverStripe_Lessons_ArticleCategory`
--

LOCK TABLES `SilverStripe_Lessons_ArticleCategory` WRITE;
/*!40000 ALTER TABLE `SilverStripe_Lessons_ArticleCategory` DISABLE KEYS */;
INSERT INTO `SilverStripe_Lessons_ArticleCategory` VALUES (1,'SilverStripe\\Lessons\\ArticleCategory','2017-09-14 16:13:52','2017-09-14 16:13:52','Properties',9),(2,'SilverStripe\\Lessons\\ArticleCategory','2017-09-14 16:14:01','2017-09-14 16:14:01','Prices',9),(3,'SilverStripe\\Lessons\\ArticleCategory','2017-09-14 16:14:08','2017-09-14 16:14:08','Best deals',9);
/*!40000 ALTER TABLE `SilverStripe_Lessons_ArticleCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SilverStripe_Lessons_ArticleComment`
--

DROP TABLE IF EXISTS `SilverStripe_Lessons_ArticleComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SilverStripe_Lessons_ArticleComment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Lessons\\ArticleComment') DEFAULT 'SilverStripe\\Lessons\\ArticleComment',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Comment` mediumtext,
  `ArticlePageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `ArticlePageID` (`ArticlePageID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SilverStripe_Lessons_ArticleComment`
--

LOCK TABLES `SilverStripe_Lessons_ArticleComment` WRITE;
/*!40000 ALTER TABLE `SilverStripe_Lessons_ArticleComment` DISABLE KEYS */;
INSERT INTO `SilverStripe_Lessons_ArticleComment` VALUES (1,'SilverStripe\\Lessons\\ArticleComment','2017-09-14 16:52:11','2017-09-14 16:52:11','Test Commenter','tester@example.com','This is a test comment.',10);
/*!40000 ALTER TABLE `SilverStripe_Lessons_ArticleComment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SilverStripe_Lessons_ArticlePage`
--

DROP TABLE IF EXISTS `SilverStripe_Lessons_ArticlePage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SilverStripe_Lessons_ArticlePage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `Teaser` mediumtext,
  `Author` varchar(255) DEFAULT NULL,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `BrochureID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PhotoID` (`PhotoID`),
  KEY `BrochureID` (`BrochureID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SilverStripe_Lessons_ArticlePage`
--

LOCK TABLES `SilverStripe_Lessons_ArticlePage` WRITE;
/*!40000 ALTER TABLE `SilverStripe_Lessons_ArticlePage` DISABLE KEYS */;
INSERT INTO `SilverStripe_Lessons_ArticlePage` VALUES (10,'2017-06-14','Summary of the first article.','Uncle Cheese',3,4),(11,NULL,NULL,NULL,5,6);
/*!40000 ALTER TABLE `SilverStripe_Lessons_ArticlePage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SilverStripe_Lessons_ArticlePage_Categories`
--

DROP TABLE IF EXISTS `SilverStripe_Lessons_ArticlePage_Categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SilverStripe_Lessons_ArticlePage_Categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SilverStripe_Lessons_ArticlePageID` int(11) NOT NULL DEFAULT '0',
  `SilverStripe_Lessons_ArticleCategoryID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SilverStripe_Lessons_ArticlePageID` (`SilverStripe_Lessons_ArticlePageID`),
  KEY `SilverStripe_Lessons_ArticleCategoryID` (`SilverStripe_Lessons_ArticleCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SilverStripe_Lessons_ArticlePage_Categories`
--

LOCK TABLES `SilverStripe_Lessons_ArticlePage_Categories` WRITE;
/*!40000 ALTER TABLE `SilverStripe_Lessons_ArticlePage_Categories` DISABLE KEYS */;
INSERT INTO `SilverStripe_Lessons_ArticlePage_Categories` VALUES (1,11,2),(2,11,3),(3,10,1);
/*!40000 ALTER TABLE `SilverStripe_Lessons_ArticlePage_Categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SilverStripe_Lessons_ArticlePage_Live`
--

DROP TABLE IF EXISTS `SilverStripe_Lessons_ArticlePage_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SilverStripe_Lessons_ArticlePage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `Teaser` mediumtext,
  `Author` varchar(255) DEFAULT NULL,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `BrochureID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PhotoID` (`PhotoID`),
  KEY `BrochureID` (`BrochureID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SilverStripe_Lessons_ArticlePage_Live`
--

LOCK TABLES `SilverStripe_Lessons_ArticlePage_Live` WRITE;
/*!40000 ALTER TABLE `SilverStripe_Lessons_ArticlePage_Live` DISABLE KEYS */;
INSERT INTO `SilverStripe_Lessons_ArticlePage_Live` VALUES (10,'2017-06-14','Summary of the first article.','Uncle Cheese',3,4),(11,NULL,NULL,NULL,5,6);
/*!40000 ALTER TABLE `SilverStripe_Lessons_ArticlePage_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SilverStripe_Lessons_ArticlePage_Versions`
--

DROP TABLE IF EXISTS `SilverStripe_Lessons_ArticlePage_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SilverStripe_Lessons_ArticlePage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Date` date DEFAULT NULL,
  `Teaser` mediumtext,
  `Author` varchar(255) DEFAULT NULL,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `BrochureID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `PhotoID` (`PhotoID`),
  KEY `BrochureID` (`BrochureID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SilverStripe_Lessons_ArticlePage_Versions`
--

LOCK TABLES `SilverStripe_Lessons_ArticlePage_Versions` WRITE;
/*!40000 ALTER TABLE `SilverStripe_Lessons_ArticlePage_Versions` DISABLE KEYS */;
INSERT INTO `SilverStripe_Lessons_ArticlePage_Versions` VALUES (1,10,3,'2017-06-14','Summary of the first article.','Uncle Cheese',0,0),(2,10,4,'2017-06-14','Summary of the first article.','Uncle Cheese',3,4),(3,11,3,NULL,NULL,NULL,5,6);
/*!40000 ALTER TABLE `SilverStripe_Lessons_ArticlePage_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SilverStripe_Lessons_Property`
--

DROP TABLE IF EXISTS `SilverStripe_Lessons_Property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SilverStripe_Lessons_Property` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Lessons\\Property') DEFAULT 'SilverStripe\\Lessons\\Property',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `PricePerNight` decimal(9,2) NOT NULL DEFAULT '0.00',
  `Bedrooms` int(11) NOT NULL DEFAULT '0',
  `Bathrooms` int(11) NOT NULL DEFAULT '0',
  `FeaturedOnHomepage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `RegionID` int(11) NOT NULL DEFAULT '0',
  `PrimaryPhotoID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `AvailableStart` date DEFAULT NULL,
  `AvailableEnd` date DEFAULT NULL,
  `Description` mediumtext,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `RegionID` (`RegionID`),
  KEY `PrimaryPhotoID` (`PrimaryPhotoID`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SilverStripe_Lessons_Property`
--

LOCK TABLES `SilverStripe_Lessons_Property` WRITE;
/*!40000 ALTER TABLE `SilverStripe_Lessons_Property` DISABLE KEYS */;
INSERT INTO `SilverStripe_Lessons_Property` VALUES (101,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Sed fusce vehicula',700.00,2,1,0,1,22,1,'2018-09-16','2018-09-24',NULL),(102,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Sagittis elementum',100.00,3,1,0,4,14,1,'2018-07-20','2018-07-25',NULL),(103,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Tortor vehicula',600.00,2,1,1,3,20,1,'2017-11-17','2017-11-29',NULL),(104,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Cubilia efficitur',100.00,3,3,0,3,23,1,'2018-01-05','2018-01-11',NULL),(105,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Consectetur mollis fringilla dui',900.00,1,1,0,1,16,1,'2018-09-11','2018-09-21',NULL),(106,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Praesent placerat eu',100.00,5,2,0,2,17,1,'2018-09-10','2018-09-13',NULL),(107,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Sit fermentum congue',100.00,3,3,1,1,17,1,'2018-08-25','2018-09-07',NULL),(108,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Dictum aliquet netus',400.00,3,3,0,4,15,1,'2018-06-05','2018-06-08',NULL),(109,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Facilisis ullamcorper',500.00,3,3,1,3,14,1,'2018-06-15','2018-06-18',NULL),(110,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Ut proin magna, elit phasellus et',200.00,1,1,0,2,18,1,'2018-04-03','2018-04-08',NULL),(111,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Fringilla urna per',200.00,2,1,1,4,21,1,'2018-03-06','2018-03-08',NULL),(112,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Ipsum adipiscing torquent nam',300.00,3,3,0,3,13,1,'2018-05-18','2018-05-30',NULL),(113,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Quisque habitant',300.00,1,1,1,3,20,1,'2018-08-16','2018-08-16',NULL),(114,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Volutpat rhoncus, nec pulvinar',600.00,1,1,1,3,13,1,'2018-04-04','2018-04-07',NULL),(115,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Interdum cubilia commodo porta, interdum eget dictumst cras',200.00,3,1,1,1,17,1,'2018-08-31','2018-09-03',NULL),(116,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Venenatis class',100.00,5,2,0,2,23,1,'2017-10-29','2017-11-06',NULL),(117,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Leo phasellus inceptos',500.00,2,1,0,1,19,1,'2018-05-27','2018-05-29',NULL),(118,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Sit tincidunt ut felis',600.00,2,1,0,3,17,1,'2017-10-15','2017-10-28',NULL),(119,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Quis ex curae urna, convallis sollicitudin libero ad',300.00,5,2,1,2,14,1,'2018-01-03','2018-01-08',NULL),(120,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Viverra tortor vulputate odio, adipiscing dapibus vel aliquet',500.00,4,2,0,1,19,1,'2017-12-06','2017-12-06',NULL),(121,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Faucibus ornare, convallis risus',200.00,2,3,0,3,19,1,'2017-11-24','2017-12-07',NULL),(122,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Auctor augue dictumst conubia',100.00,1,2,1,1,19,1,'2017-12-16','2017-12-27',NULL),(123,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Malesuada fusce imperdiet',900.00,3,1,1,1,19,1,'2018-05-13','2018-05-13',NULL),(124,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Sapien varius pretium inceptos',400.00,1,2,0,4,17,1,'2018-03-17','2018-03-29',NULL),(125,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Vestibulum conubia tristique',100.00,5,2,1,3,16,1,'2018-03-18','2018-03-23',NULL),(126,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Tortor bibendum diam, velit quis pharetra',700.00,5,2,0,2,17,1,'2018-09-08','2018-09-10',NULL),(127,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Potenti laoreet, eu litora',200.00,5,2,1,4,16,1,'2018-01-28','2018-02-07',NULL),(128,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Convallis sagittis gravida',500.00,3,3,0,2,19,1,'2018-07-31','2018-07-31',NULL),(129,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Suspendisse netus',400.00,5,2,0,4,22,1,'2017-12-07','2017-12-08',NULL),(130,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Cursus dictumst, quis congue',800.00,1,3,1,4,15,1,'2018-03-13','2018-03-20',NULL),(131,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Sed tortor elementum',200.00,1,2,1,1,22,1,'2018-06-10','2018-06-13',NULL),(132,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Dictumst elementum',500.00,4,1,1,4,23,1,'2017-11-19','2017-11-28',NULL),(133,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Posuere sollicitudin, ipsum senectus',200.00,2,2,0,2,21,1,'2018-05-10','2018-05-17',NULL),(134,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Erat leo taciti',900.00,3,2,1,3,14,1,'2018-05-21','2018-05-29',NULL),(135,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Nibh convallis et libero',900.00,4,2,1,3,13,1,'2018-02-17','2018-02-24',NULL),(136,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Justo suspendisse pretium',700.00,5,3,1,4,21,1,'2017-10-03','2017-10-14',NULL),(137,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Donec laoreet',400.00,4,1,1,3,22,1,'2018-08-25','2018-08-31',NULL),(138,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Interdum integer ultricies consequat',500.00,5,2,0,2,20,1,'2018-03-31','2018-04-11',NULL),(139,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Fusce arcu vivamus, at facilisis ligula',100.00,3,1,0,4,15,1,'2018-07-19','2018-07-31',NULL),(140,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Erat convallis primis consequat',500.00,4,3,0,2,14,1,'2018-04-03','2018-04-15',NULL),(141,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Adipiscing eleifend quam ullamcorper',500.00,1,1,1,4,19,1,'2017-11-26','2017-12-08',NULL),(142,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Metus et, vel cras',500.00,2,1,0,2,16,1,'2018-01-04','2018-01-12',NULL),(143,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Dictum erat metus odio',600.00,4,2,1,4,21,1,'2018-08-12','2018-08-18',NULL),(144,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Sed habitasse fermentum',100.00,2,3,0,3,17,1,'2018-04-16','2018-04-22',NULL),(145,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Metus maximus, maecenas tempus',100.00,1,1,1,1,19,1,'2017-11-15','2017-11-15',NULL),(146,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Maecenas condimentum magna',500.00,4,1,0,2,17,1,'2017-10-06','2017-10-17',NULL),(147,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Id eleifend risus, facilisis tempor ante',600.00,3,3,1,3,17,1,'2018-06-19','2018-07-02',NULL),(148,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Viverra taciti enim',200.00,1,2,0,1,20,1,'2018-04-17','2018-04-23',NULL),(149,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Sociosqu vehicula',200.00,4,1,1,2,14,1,'2018-04-30','2018-05-04',NULL),(150,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Felis orci',200.00,5,2,1,1,14,1,'2018-01-13','2018-01-16',NULL),(151,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Convallis eros netus, condimentum himenaeos magna',300.00,2,1,1,4,21,1,'2018-06-13','2018-06-18',NULL),(152,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Ultricies fames',200.00,1,3,1,3,13,1,'2018-05-26','2018-05-26',NULL),(153,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Tortor nisi litora, litora conubia neque',400.00,1,3,1,2,18,1,'2017-11-30','2017-12-02',NULL),(154,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Euismod himenaeos donec, maecenas tincidunt fusce',100.00,4,3,1,1,16,1,'2018-08-23','2018-09-03',NULL),(155,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Lorem mattis nunc orci',100.00,4,1,1,2,17,1,'2017-09-27','2017-10-05',NULL),(156,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Lacinia eleifend eu',500.00,4,1,1,4,15,1,'2018-01-10','2018-01-17',NULL),(157,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Lobortis porttitor condimentum ad',800.00,2,3,0,1,16,1,'2018-03-08','2018-03-17',NULL),(158,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Nulla varius et class',200.00,2,3,1,2,18,1,'2018-02-05','2018-02-17',NULL),(159,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Suspendisse ante',700.00,5,3,0,1,16,1,'2018-03-15','2018-03-19',NULL),(160,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Viverra pulvinar cursus blandit',300.00,3,2,0,2,18,1,'2017-12-23','2017-12-23',NULL),(161,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Etiam velit',300.00,1,2,1,4,21,1,'2018-07-17','2018-07-19',NULL),(162,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Tortor turpis',100.00,1,2,0,3,13,1,'2018-01-18','2018-01-30',NULL),(163,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Etiam aliquam',400.00,3,3,1,2,13,1,'2017-11-15','2017-11-27',NULL),(164,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Eleifend cubilia enim',800.00,5,1,0,2,20,1,'2018-06-27','2018-07-07',NULL),(165,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Feugiat purus',300.00,1,2,1,3,22,1,'2018-01-28','2018-01-29',NULL),(166,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Nostra netus',800.00,4,1,1,4,20,1,'2018-03-10','2018-03-11',NULL),(167,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Mattis laoreet imperdiet',600.00,1,3,0,3,23,1,'2017-12-21','2017-12-27',NULL),(168,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Eleifend nunc tempor et',400.00,1,2,1,4,22,1,'2018-07-26','2018-08-07',NULL),(169,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Praesent auctor',700.00,5,2,0,2,22,1,'2018-03-04','2018-03-04',NULL),(170,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Integer sem, himenaeos blandit',500.00,1,2,1,3,22,1,'2018-06-05','2018-06-12',NULL),(171,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Erat augue vivamus fermentum',300.00,4,2,1,2,18,1,'2017-11-17','2017-11-23',NULL),(172,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Egestas posuere magna, justo nisi libero',800.00,3,2,0,2,20,1,'2018-05-16','2018-05-25',NULL),(173,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Et magna congue eros',600.00,4,3,1,4,21,1,'2018-07-01','2018-07-14',NULL),(174,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Sed phasellus nullam cras',400.00,5,3,1,1,18,1,'2018-08-19','2018-08-31',NULL),(175,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Venenatis ad diam',600.00,2,1,1,3,18,1,'2017-12-08','2017-12-15',NULL),(176,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Vestibulum congue, fusce donec',500.00,1,2,1,2,20,1,'2018-01-16','2018-01-29',NULL),(177,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Integer vel',400.00,5,2,0,3,19,1,'2018-09-04','2018-09-06',NULL),(178,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Mi primis',800.00,2,2,0,4,13,1,'2018-07-04','2018-07-04',NULL),(179,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Mi molestie commodo maximus',500.00,1,3,1,3,22,1,'2017-10-10','2017-10-19',NULL),(180,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Sit semper molestie eros',600.00,5,2,1,3,19,1,'2018-08-13','2018-08-14',NULL),(181,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Condimentum habitasse',500.00,3,3,1,4,13,1,'2018-01-06','2018-01-15',NULL),(182,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Sapien nunc duis',600.00,3,2,0,2,20,1,'2018-06-30','2018-07-02',NULL),(183,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Malesuada id',800.00,1,2,0,1,13,1,'2018-09-12','2018-09-21',NULL),(184,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','At suspendisse pretium',500.00,1,1,1,2,14,1,'2018-04-08','2018-04-19',NULL),(185,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Elit in',200.00,4,1,1,1,22,1,'2018-07-03','2018-07-05',NULL),(186,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Porttitor inceptos',200.00,4,1,1,2,19,1,'2017-12-22','2017-12-28',NULL),(187,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Id metus',100.00,1,3,1,1,17,1,'2018-08-18','2018-08-30',NULL),(188,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Integer aliquet habitant',200.00,5,3,1,2,19,1,'2018-06-26','2018-07-09',NULL),(189,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Tempor platea',200.00,3,1,1,2,23,1,'2017-10-17','2017-10-18',NULL),(190,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Scelerisque ultricies dictumst',800.00,3,2,0,4,14,1,'2017-10-14','2017-10-22',NULL),(191,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Ligula vehicula',500.00,3,3,1,3,21,1,'2017-10-23','2017-10-31',NULL),(192,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Viverra proin hendrerit',700.00,2,1,1,1,23,1,'2017-12-14','2017-12-20',NULL),(193,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Malesuada ut est ad, justo pretium aptent imperdiet, ac semper arcu accumsan',200.00,2,2,0,3,15,1,'2018-08-06','2018-08-11',NULL),(194,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Ante ornare sagittis donec',800.00,5,2,0,4,22,1,'2018-05-24','2018-06-02',NULL),(195,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Ornare sollicitudin gravida ad, sit euismod commodo vehicula',400.00,1,1,0,1,13,1,'2018-06-11','2018-06-11',NULL),(196,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Nulla orci',100.00,5,1,0,3,17,1,'2018-04-19','2018-04-22',NULL),(197,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:19','2017-09-25 15:34:35','Quis phasellus, et nostra',300.00,2,1,0,1,22,1,'2018-06-06','2018-06-08',NULL),(198,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:19','2017-09-25 15:34:35','Praesent eleifend tempor',300.00,2,2,1,2,17,1,'2018-07-05','2018-07-06',NULL),(199,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:19','2017-09-25 15:34:35','Egestas convallis enim bibendum',700.00,3,3,1,1,13,1,'2018-07-08','2018-07-20',NULL),(200,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:19','2017-09-25 15:34:35','Sed auctor tempus',200.00,4,2,0,4,13,1,'2018-05-01','2018-05-05',NULL);
/*!40000 ALTER TABLE `SilverStripe_Lessons_Property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SilverStripe_Lessons_Property_Live`
--

DROP TABLE IF EXISTS `SilverStripe_Lessons_Property_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SilverStripe_Lessons_Property_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Lessons\\Property') DEFAULT 'SilverStripe\\Lessons\\Property',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `PricePerNight` decimal(9,2) NOT NULL DEFAULT '0.00',
  `Bedrooms` int(11) NOT NULL DEFAULT '0',
  `Bathrooms` int(11) NOT NULL DEFAULT '0',
  `FeaturedOnHomepage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `RegionID` int(11) NOT NULL DEFAULT '0',
  `PrimaryPhotoID` int(11) NOT NULL DEFAULT '0',
  `AvailableStart` date DEFAULT NULL,
  `AvailableEnd` date DEFAULT NULL,
  `Description` mediumtext,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `RegionID` (`RegionID`),
  KEY `PrimaryPhotoID` (`PrimaryPhotoID`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SilverStripe_Lessons_Property_Live`
--

LOCK TABLES `SilverStripe_Lessons_Property_Live` WRITE;
/*!40000 ALTER TABLE `SilverStripe_Lessons_Property_Live` DISABLE KEYS */;
INSERT INTO `SilverStripe_Lessons_Property_Live` VALUES (101,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Sed fusce vehicula',700.00,2,1,0,1,1,22,'2018-09-16','2018-09-24',NULL),(102,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Sagittis elementum',100.00,3,1,0,1,4,14,'2018-07-20','2018-07-25',NULL),(103,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Tortor vehicula',600.00,2,1,1,1,3,20,'2017-11-17','2017-11-29',NULL),(104,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Cubilia efficitur',100.00,3,3,0,1,3,23,'2018-01-05','2018-01-11',NULL),(105,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Consectetur mollis fringilla dui',900.00,1,1,0,1,1,16,'2018-09-11','2018-09-21',NULL),(106,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Praesent placerat eu',100.00,5,2,0,1,2,17,'2018-09-10','2018-09-13',NULL),(107,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Sit fermentum congue',100.00,3,3,1,1,1,17,'2018-08-25','2018-09-07',NULL),(108,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Dictum aliquet netus',400.00,3,3,0,1,4,15,'2018-06-05','2018-06-08',NULL),(109,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Facilisis ullamcorper',500.00,3,3,1,1,3,14,'2018-06-15','2018-06-18',NULL),(110,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Ut proin magna, elit phasellus et',200.00,1,1,0,1,2,18,'2018-04-03','2018-04-08',NULL),(111,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Fringilla urna per',200.00,2,1,1,1,4,21,'2018-03-06','2018-03-08',NULL),(112,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Ipsum adipiscing torquent nam',300.00,3,3,0,1,3,13,'2018-05-18','2018-05-30',NULL),(113,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Quisque habitant',300.00,1,1,1,1,3,20,'2018-08-16','2018-08-16',NULL),(114,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Volutpat rhoncus, nec pulvinar',600.00,1,1,1,1,3,13,'2018-04-04','2018-04-07',NULL),(115,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Interdum cubilia commodo porta, interdum eget dictumst cras',200.00,3,1,1,1,1,17,'2018-08-31','2018-09-03',NULL),(116,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Venenatis class',100.00,5,2,0,1,2,23,'2017-10-29','2017-11-06',NULL),(117,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Leo phasellus inceptos',500.00,2,1,0,1,1,19,'2018-05-27','2018-05-29',NULL),(118,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Sit tincidunt ut felis',600.00,2,1,0,1,3,17,'2017-10-15','2017-10-28',NULL),(119,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Quis ex curae urna, convallis sollicitudin libero ad',300.00,5,2,1,1,2,14,'2018-01-03','2018-01-08',NULL),(120,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Viverra tortor vulputate odio, adipiscing dapibus vel aliquet',500.00,4,2,0,1,1,19,'2017-12-06','2017-12-06',NULL),(121,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Faucibus ornare, convallis risus',200.00,2,3,0,1,3,19,'2017-11-24','2017-12-07',NULL),(122,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Auctor augue dictumst conubia',100.00,1,2,1,1,1,19,'2017-12-16','2017-12-27',NULL),(123,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Malesuada fusce imperdiet',900.00,3,1,1,1,1,19,'2018-05-13','2018-05-13',NULL),(124,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Sapien varius pretium inceptos',400.00,1,2,0,1,4,17,'2018-03-17','2018-03-29',NULL),(125,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Vestibulum conubia tristique',100.00,5,2,1,1,3,16,'2018-03-18','2018-03-23',NULL),(126,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Tortor bibendum diam, velit quis pharetra',700.00,5,2,0,1,2,17,'2018-09-08','2018-09-10',NULL),(127,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Potenti laoreet, eu litora',200.00,5,2,1,1,4,16,'2018-01-28','2018-02-07',NULL),(128,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Convallis sagittis gravida',500.00,3,3,0,1,2,19,'2018-07-31','2018-07-31',NULL),(129,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Suspendisse netus',400.00,5,2,0,1,4,22,'2017-12-07','2017-12-08',NULL),(130,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Cursus dictumst, quis congue',800.00,1,3,1,1,4,15,'2018-03-13','2018-03-20',NULL),(131,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Sed tortor elementum',200.00,1,2,1,1,1,22,'2018-06-10','2018-06-13',NULL),(132,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Dictumst elementum',500.00,4,1,1,1,4,23,'2017-11-19','2017-11-28',NULL),(133,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Posuere sollicitudin, ipsum senectus',200.00,2,2,0,1,2,21,'2018-05-10','2018-05-17',NULL),(134,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Erat leo taciti',900.00,3,2,1,1,3,14,'2018-05-21','2018-05-29',NULL),(135,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Nibh convallis et libero',900.00,4,2,1,1,3,13,'2018-02-17','2018-02-24',NULL),(136,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Justo suspendisse pretium',700.00,5,3,1,1,4,21,'2017-10-03','2017-10-14',NULL),(137,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Donec laoreet',400.00,4,1,1,1,3,22,'2018-08-25','2018-08-31',NULL),(138,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Interdum integer ultricies consequat',500.00,5,2,0,1,2,20,'2018-03-31','2018-04-11',NULL),(139,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Fusce arcu vivamus, at facilisis ligula',100.00,3,1,0,1,4,15,'2018-07-19','2018-07-31',NULL),(140,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Erat convallis primis consequat',500.00,4,3,0,1,2,14,'2018-04-03','2018-04-15',NULL),(141,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Adipiscing eleifend quam ullamcorper',500.00,1,1,1,1,4,19,'2017-11-26','2017-12-08',NULL),(142,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Metus et, vel cras',500.00,2,1,0,1,2,16,'2018-01-04','2018-01-12',NULL),(143,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Dictum erat metus odio',600.00,4,2,1,1,4,21,'2018-08-12','2018-08-18',NULL),(144,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Sed habitasse fermentum',100.00,2,3,0,1,3,17,'2018-04-16','2018-04-22',NULL),(145,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Metus maximus, maecenas tempus',100.00,1,1,1,1,1,19,'2017-11-15','2017-11-15',NULL),(146,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Maecenas condimentum magna',500.00,4,1,0,1,2,17,'2017-10-06','2017-10-17',NULL),(147,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Id eleifend risus, facilisis tempor ante',600.00,3,3,1,1,3,17,'2018-06-19','2018-07-02',NULL),(148,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Viverra taciti enim',200.00,1,2,0,1,1,20,'2018-04-17','2018-04-23',NULL),(149,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Sociosqu vehicula',200.00,4,1,1,1,2,14,'2018-04-30','2018-05-04',NULL),(150,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Felis orci',200.00,5,2,1,1,1,14,'2018-01-13','2018-01-16',NULL),(151,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Convallis eros netus, condimentum himenaeos magna',300.00,2,1,1,1,4,21,'2018-06-13','2018-06-18',NULL),(152,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Ultricies fames',200.00,1,3,1,1,3,13,'2018-05-26','2018-05-26',NULL),(153,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Tortor nisi litora, litora conubia neque',400.00,1,3,1,1,2,18,'2017-11-30','2017-12-02',NULL),(154,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Euismod himenaeos donec, maecenas tincidunt fusce',100.00,4,3,1,1,1,16,'2018-08-23','2018-09-03',NULL),(155,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Lorem mattis nunc orci',100.00,4,1,1,1,2,17,'2017-09-27','2017-10-05',NULL),(156,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Lacinia eleifend eu',500.00,4,1,1,1,4,15,'2018-01-10','2018-01-17',NULL),(157,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Lobortis porttitor condimentum ad',800.00,2,3,0,1,1,16,'2018-03-08','2018-03-17',NULL),(158,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Nulla varius et class',200.00,2,3,1,1,2,18,'2018-02-05','2018-02-17',NULL),(159,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Suspendisse ante',700.00,5,3,0,1,1,16,'2018-03-15','2018-03-19',NULL),(160,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Viverra pulvinar cursus blandit',300.00,3,2,0,1,2,18,'2017-12-23','2017-12-23',NULL),(161,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Etiam velit',300.00,1,2,1,1,4,21,'2018-07-17','2018-07-19',NULL),(162,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Tortor turpis',100.00,1,2,0,1,3,13,'2018-01-18','2018-01-30',NULL),(163,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Etiam aliquam',400.00,3,3,1,1,2,13,'2017-11-15','2017-11-27',NULL),(164,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Eleifend cubilia enim',800.00,5,1,0,1,2,20,'2018-06-27','2018-07-07',NULL),(165,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Feugiat purus',300.00,1,2,1,1,3,22,'2018-01-28','2018-01-29',NULL),(166,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Nostra netus',800.00,4,1,1,1,4,20,'2018-03-10','2018-03-11',NULL),(167,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Mattis laoreet imperdiet',600.00,1,3,0,1,3,23,'2017-12-21','2017-12-27',NULL),(168,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Eleifend nunc tempor et',400.00,1,2,1,1,4,22,'2018-07-26','2018-08-07',NULL),(169,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Praesent auctor',700.00,5,2,0,1,2,22,'2018-03-04','2018-03-04',NULL),(170,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Integer sem, himenaeos blandit',500.00,1,2,1,1,3,22,'2018-06-05','2018-06-12',NULL),(171,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Erat augue vivamus fermentum',300.00,4,2,1,1,2,18,'2017-11-17','2017-11-23',NULL),(172,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Egestas posuere magna, justo nisi libero',800.00,3,2,0,1,2,20,'2018-05-16','2018-05-25',NULL),(173,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Et magna congue eros',600.00,4,3,1,1,4,21,'2018-07-01','2018-07-14',NULL),(174,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Sed phasellus nullam cras',400.00,5,3,1,1,1,18,'2018-08-19','2018-08-31',NULL),(175,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Venenatis ad diam',600.00,2,1,1,1,3,18,'2017-12-08','2017-12-15',NULL),(176,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Vestibulum congue, fusce donec',500.00,1,2,1,1,2,20,'2018-01-16','2018-01-29',NULL),(177,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Integer vel',400.00,5,2,0,1,3,19,'2018-09-04','2018-09-06',NULL),(178,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Mi primis',800.00,2,2,0,1,4,13,'2018-07-04','2018-07-04',NULL),(179,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Mi molestie commodo maximus',500.00,1,3,1,1,3,22,'2017-10-10','2017-10-19',NULL),(180,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Sit semper molestie eros',600.00,5,2,1,1,3,19,'2018-08-13','2018-08-14',NULL),(181,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Condimentum habitasse',500.00,3,3,1,1,4,13,'2018-01-06','2018-01-15',NULL),(182,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Sapien nunc duis',600.00,3,2,0,1,2,20,'2018-06-30','2018-07-02',NULL),(183,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Malesuada id',800.00,1,2,0,1,1,13,'2018-09-12','2018-09-21',NULL),(184,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','At suspendisse pretium',500.00,1,1,1,1,2,14,'2018-04-08','2018-04-19',NULL),(185,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Elit in',200.00,4,1,1,1,1,22,'2018-07-03','2018-07-05',NULL),(186,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Porttitor inceptos',200.00,4,1,1,1,2,19,'2017-12-22','2017-12-28',NULL),(187,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Id metus',100.00,1,3,1,1,1,17,'2018-08-18','2018-08-30',NULL),(188,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Integer aliquet habitant',200.00,5,3,1,1,2,19,'2018-06-26','2018-07-09',NULL),(189,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Tempor platea',200.00,3,1,1,1,2,23,'2017-10-17','2017-10-18',NULL),(190,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Scelerisque ultricies dictumst',800.00,3,2,0,1,4,14,'2017-10-14','2017-10-22',NULL),(191,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Ligula vehicula',500.00,3,3,1,1,3,21,'2017-10-23','2017-10-31',NULL),(192,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Viverra proin hendrerit',700.00,2,1,1,1,1,23,'2017-12-14','2017-12-20',NULL),(193,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Malesuada ut est ad, justo pretium aptent imperdiet, ac semper arcu accumsan',200.00,2,2,0,1,3,15,'2018-08-06','2018-08-11',NULL),(194,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Ante ornare sagittis donec',800.00,5,2,0,1,4,22,'2018-05-24','2018-06-02',NULL),(195,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Ornare sollicitudin gravida ad, sit euismod commodo vehicula',400.00,1,1,0,1,1,13,'2018-06-11','2018-06-11',NULL),(196,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Nulla orci',100.00,5,1,0,1,3,17,'2018-04-19','2018-04-22',NULL),(197,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:19','2017-09-25 15:34:35','Quis phasellus, et nostra',300.00,2,1,0,1,1,22,'2018-06-06','2018-06-08',NULL),(198,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:19','2017-09-25 15:34:35','Praesent eleifend tempor',300.00,2,2,1,1,2,17,'2018-07-05','2018-07-06',NULL),(199,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:19','2017-09-25 15:34:35','Egestas convallis enim bibendum',700.00,3,3,1,1,1,13,'2018-07-08','2018-07-20',NULL),(200,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:19','2017-09-25 15:34:35','Sed auctor tempus',200.00,4,2,0,1,4,13,'2018-05-01','2018-05-05',NULL);
/*!40000 ALTER TABLE `SilverStripe_Lessons_Property_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SilverStripe_Lessons_Property_Versions`
--

DROP TABLE IF EXISTS `SilverStripe_Lessons_Property_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SilverStripe_Lessons_Property_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SilverStripe\\Lessons\\Property') DEFAULT 'SilverStripe\\Lessons\\Property',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `PricePerNight` decimal(9,2) NOT NULL DEFAULT '0.00',
  `Bedrooms` int(11) NOT NULL DEFAULT '0',
  `Bathrooms` int(11) NOT NULL DEFAULT '0',
  `FeaturedOnHomepage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `RegionID` int(11) NOT NULL DEFAULT '0',
  `PrimaryPhotoID` int(11) NOT NULL DEFAULT '0',
  `AvailableStart` date DEFAULT NULL,
  `AvailableEnd` date DEFAULT NULL,
  `Description` mediumtext,
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ClassName` (`ClassName`),
  KEY `RegionID` (`RegionID`),
  KEY `PrimaryPhotoID` (`PrimaryPhotoID`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SilverStripe_Lessons_Property_Versions`
--

LOCK TABLES `SilverStripe_Lessons_Property_Versions` WRITE;
/*!40000 ALTER TABLE `SilverStripe_Lessons_Property_Versions` DISABLE KEYS */;
INSERT INTO `SilverStripe_Lessons_Property_Versions` VALUES (1,101,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Sed fusce vehicula',700.00,2,1,0,1,22,NULL,NULL,NULL),(2,102,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Sagittis elementum',100.00,3,1,0,4,14,NULL,NULL,NULL),(3,103,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Tortor vehicula',600.00,2,1,1,3,20,NULL,NULL,NULL),(4,104,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Cubilia efficitur',100.00,3,3,0,3,23,NULL,NULL,NULL),(5,105,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Consectetur mollis fringilla dui',900.00,1,1,0,1,16,NULL,NULL,NULL),(6,106,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Praesent placerat eu',100.00,5,2,0,2,17,NULL,NULL,NULL),(7,107,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Sit fermentum congue',100.00,3,3,1,1,17,NULL,NULL,NULL),(8,108,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Dictum aliquet netus',400.00,3,3,0,4,15,NULL,NULL,NULL),(9,109,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Facilisis ullamcorper',500.00,3,3,1,3,14,NULL,NULL,NULL),(10,110,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Ut proin magna, elit phasellus et',200.00,1,1,0,2,18,NULL,NULL,NULL),(11,111,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Fringilla urna per',200.00,2,1,1,4,21,NULL,NULL,NULL),(12,112,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Ipsum adipiscing torquent nam',300.00,3,3,0,3,13,NULL,NULL,NULL),(13,113,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:14','2017-09-25 15:34:34','Quisque habitant',300.00,1,1,1,3,20,NULL,NULL,NULL),(14,114,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Volutpat rhoncus, nec pulvinar',600.00,1,1,1,3,13,NULL,NULL,NULL),(15,115,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Interdum cubilia commodo porta, interdum eget dictumst cras',200.00,3,1,1,1,17,NULL,NULL,NULL),(16,116,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Venenatis class',100.00,5,2,0,2,23,NULL,NULL,NULL),(17,117,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Leo phasellus inceptos',500.00,2,1,0,1,19,NULL,NULL,NULL),(18,118,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Sit tincidunt ut felis',600.00,2,1,0,3,17,NULL,NULL,NULL),(19,119,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Quis ex curae urna, convallis sollicitudin libero ad',300.00,5,2,1,2,14,NULL,NULL,NULL),(20,120,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Viverra tortor vulputate odio, adipiscing dapibus vel aliquet',500.00,4,2,0,1,19,NULL,NULL,NULL),(21,121,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:34','Faucibus ornare, convallis risus',200.00,2,3,0,3,19,NULL,NULL,NULL),(22,122,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Auctor augue dictumst conubia',100.00,1,2,1,1,19,NULL,NULL,NULL),(23,123,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Malesuada fusce imperdiet',900.00,3,1,1,1,19,NULL,NULL,NULL),(24,124,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Sapien varius pretium inceptos',400.00,1,2,0,4,17,NULL,NULL,NULL),(25,125,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Vestibulum conubia tristique',100.00,5,2,1,3,16,NULL,NULL,NULL),(26,126,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Tortor bibendum diam, velit quis pharetra',700.00,5,2,0,2,17,NULL,NULL,NULL),(27,127,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Potenti laoreet, eu litora',200.00,5,2,1,4,16,NULL,NULL,NULL),(28,128,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Convallis sagittis gravida',500.00,3,3,0,2,19,NULL,NULL,NULL),(29,129,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Suspendisse netus',400.00,5,2,0,4,22,NULL,NULL,NULL),(30,130,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Cursus dictumst, quis congue',800.00,1,3,1,4,15,NULL,NULL,NULL),(31,131,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Sed tortor elementum',200.00,1,2,1,1,22,NULL,NULL,NULL),(32,132,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Dictumst elementum',500.00,4,1,1,4,23,NULL,NULL,NULL),(33,133,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Posuere sollicitudin, ipsum senectus',200.00,2,2,0,2,21,NULL,NULL,NULL),(34,134,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:15','2017-09-25 15:34:35','Erat leo taciti',900.00,3,2,1,3,14,NULL,NULL,NULL),(35,135,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Nibh convallis et libero',900.00,4,2,1,3,13,NULL,NULL,NULL),(36,136,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Justo suspendisse pretium',700.00,5,3,1,4,21,NULL,NULL,NULL),(37,137,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Donec laoreet',400.00,4,1,1,3,22,NULL,NULL,NULL),(38,138,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Interdum integer ultricies consequat',500.00,5,2,0,2,20,NULL,NULL,NULL),(39,139,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Fusce arcu vivamus, at facilisis ligula',100.00,3,1,0,4,15,NULL,NULL,NULL),(40,140,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Erat convallis primis consequat',500.00,4,3,0,2,14,NULL,NULL,NULL),(41,141,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Adipiscing eleifend quam ullamcorper',500.00,1,1,1,4,19,NULL,NULL,NULL),(42,142,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Metus et, vel cras',500.00,2,1,0,2,16,NULL,NULL,NULL),(43,143,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Dictum erat metus odio',600.00,4,2,1,4,21,NULL,NULL,NULL),(44,144,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Sed habitasse fermentum',100.00,2,3,0,3,17,NULL,NULL,NULL),(45,145,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Metus maximus, maecenas tempus',100.00,1,1,1,1,19,NULL,NULL,NULL),(46,146,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Maecenas condimentum magna',500.00,4,1,0,2,17,NULL,NULL,NULL),(47,147,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Id eleifend risus, facilisis tempor ante',600.00,3,3,1,3,17,NULL,NULL,NULL),(48,148,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Viverra taciti enim',200.00,1,2,0,1,20,NULL,NULL,NULL),(49,149,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Sociosqu vehicula',200.00,4,1,1,2,14,NULL,NULL,NULL),(50,150,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Felis orci',200.00,5,2,1,1,14,NULL,NULL,NULL),(51,151,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Convallis eros netus, condimentum himenaeos magna',300.00,2,1,1,4,21,NULL,NULL,NULL),(52,152,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Ultricies fames',200.00,1,3,1,3,13,NULL,NULL,NULL),(53,153,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Tortor nisi litora, litora conubia neque',400.00,1,3,1,2,18,NULL,NULL,NULL),(54,154,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Euismod himenaeos donec, maecenas tincidunt fusce',100.00,4,3,1,1,16,NULL,NULL,NULL),(55,155,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:16','2017-09-25 15:34:35','Lorem mattis nunc orci',100.00,4,1,1,2,17,NULL,NULL,NULL),(56,156,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Lacinia eleifend eu',500.00,4,1,1,4,15,NULL,NULL,NULL),(57,157,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Lobortis porttitor condimentum ad',800.00,2,3,0,1,16,NULL,NULL,NULL),(58,158,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Nulla varius et class',200.00,2,3,1,2,18,NULL,NULL,NULL),(59,159,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Suspendisse ante',700.00,5,3,0,1,16,NULL,NULL,NULL),(60,160,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Viverra pulvinar cursus blandit',300.00,3,2,0,2,18,NULL,NULL,NULL),(61,161,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Etiam velit',300.00,1,2,1,4,21,NULL,NULL,NULL),(62,162,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Tortor turpis',100.00,1,2,0,3,13,NULL,NULL,NULL),(63,163,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Etiam aliquam',400.00,3,3,1,2,13,NULL,NULL,NULL),(64,164,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Eleifend cubilia enim',800.00,5,1,0,2,20,NULL,NULL,NULL),(65,165,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Feugiat purus',300.00,1,2,1,3,22,NULL,NULL,NULL),(66,166,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Nostra netus',800.00,4,1,1,4,20,NULL,NULL,NULL),(67,167,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Mattis laoreet imperdiet',600.00,1,3,0,3,23,NULL,NULL,NULL),(68,168,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Eleifend nunc tempor et',400.00,1,2,1,4,22,NULL,NULL,NULL),(69,169,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Praesent auctor',700.00,5,2,0,2,22,NULL,NULL,NULL),(70,170,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Integer sem, himenaeos blandit',500.00,1,2,1,3,22,NULL,NULL,NULL),(71,171,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Erat augue vivamus fermentum',300.00,4,2,1,2,18,NULL,NULL,NULL),(72,172,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Egestas posuere magna, justo nisi libero',800.00,3,2,0,2,20,NULL,NULL,NULL),(73,173,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Et magna congue eros',600.00,4,3,1,4,21,NULL,NULL,NULL),(74,174,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Sed phasellus nullam cras',400.00,5,3,1,1,18,NULL,NULL,NULL),(75,175,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Venenatis ad diam',600.00,2,1,1,3,18,NULL,NULL,NULL),(76,176,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:17','2017-09-25 15:34:35','Vestibulum congue, fusce donec',500.00,1,2,1,2,20,NULL,NULL,NULL),(77,177,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Integer vel',400.00,5,2,0,3,19,NULL,NULL,NULL),(78,178,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Mi primis',800.00,2,2,0,4,13,NULL,NULL,NULL),(79,179,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Mi molestie commodo maximus',500.00,1,3,1,3,22,NULL,NULL,NULL),(80,180,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Sit semper molestie eros',600.00,5,2,1,3,19,NULL,NULL,NULL),(81,181,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Condimentum habitasse',500.00,3,3,1,4,13,NULL,NULL,NULL),(82,182,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Sapien nunc duis',600.00,3,2,0,2,20,NULL,NULL,NULL),(83,183,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Malesuada id',800.00,1,2,0,1,13,NULL,NULL,NULL),(84,184,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','At suspendisse pretium',500.00,1,1,1,2,14,NULL,NULL,NULL),(85,185,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Elit in',200.00,4,1,1,1,22,NULL,NULL,NULL),(86,186,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Porttitor inceptos',200.00,4,1,1,2,19,NULL,NULL,NULL),(87,187,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Id metus',100.00,1,3,1,1,17,NULL,NULL,NULL),(88,188,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Integer aliquet habitant',200.00,5,3,1,2,19,NULL,NULL,NULL),(89,189,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Tempor platea',200.00,3,1,1,2,23,NULL,NULL,NULL),(90,190,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Scelerisque ultricies dictumst',800.00,3,2,0,4,14,NULL,NULL,NULL),(91,191,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Ligula vehicula',500.00,3,3,1,3,21,NULL,NULL,NULL),(92,192,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Viverra proin hendrerit',700.00,2,1,1,1,23,NULL,NULL,NULL),(93,193,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Malesuada ut est ad, justo pretium aptent imperdiet, ac semper arcu accumsan',200.00,2,2,0,3,15,NULL,NULL,NULL),(94,194,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Ante ornare sagittis donec',800.00,5,2,0,4,22,NULL,NULL,NULL),(95,195,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Ornare sollicitudin gravida ad, sit euismod commodo vehicula',400.00,1,1,0,1,13,NULL,NULL,NULL),(96,196,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Nulla orci',100.00,5,1,0,3,17,NULL,NULL,NULL),(97,197,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:18','2017-09-25 15:34:35','Quis phasellus, et nostra',300.00,2,1,0,1,22,NULL,NULL,NULL),(98,198,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:19','2017-09-25 15:34:35','Praesent eleifend tempor',300.00,2,2,1,2,17,NULL,NULL,NULL),(99,199,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:19','2017-09-25 15:34:35','Egestas convallis enim bibendum',700.00,3,3,1,1,13,NULL,NULL,NULL),(100,200,1,1,1,1,'SilverStripe\\Lessons\\Property','2017-09-26 12:01:19','2017-09-25 15:34:35','Sed auctor tempus',200.00,4,2,0,4,13,NULL,NULL,NULL);
/*!40000 ALTER TABLE `SilverStripe_Lessons_Property_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SilverStripe_Lessons_Region`
--

DROP TABLE IF EXISTS `SilverStripe_Lessons_Region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SilverStripe_Lessons_Region` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Lessons\\Region') DEFAULT 'SilverStripe\\Lessons\\Region',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Description` mediumtext,
  `Version` int(11) NOT NULL DEFAULT '0',
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `RegionsPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `PhotoID` (`PhotoID`),
  KEY `RegionsPageID` (`RegionsPageID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SilverStripe_Lessons_Region`
--

LOCK TABLES `SilverStripe_Lessons_Region` WRITE;
/*!40000 ALTER TABLE `SilverStripe_Lessons_Region` DISABLE KEYS */;
INSERT INTO `SilverStripe_Lessons_Region` VALUES (1,'SilverStripe\\Lessons\\Region','2017-09-26 12:27:46','2017-09-14 15:26:25','The Northeast','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque massa felis, hendrerit non tempor quis, hendrerit non sem. Praesent ornare arcu mattis libero sollicitudin, vel dignissim enim placerat. Proin nec risus nulla. Pellentesque quis nulla vitae lorem porta cursus. Aliquam egestas mi ut sem molestie feugiat. Donec maximus facilisis nibh, ultricies placerat lacus cursus eget. Aliquam facilisis est et est bibendum, sed convallis neque cursus. Cras posuere accumsan nibh nec interdum. Vestibulum quam ligula, dignissim vitae tincidunt sit amet, efficitur eleifend libero. Duis quis faucibus tortor. Mauris et scelerisque orci. Phasellus eu neque viverra, pulvinar urna sit amet, hendrerit felis. Morbi id finibus arcu. Etiam nulla felis, auctor quis ultrices in, iaculis sed libero. Phasellus commodo libero vel elit mattis, ac consectetur leo euismod. Nullam feugiat eget nunc tristique vehicula.</p><p>Aliquam leo augue, tempor scelerisque nisi at, gravida pharetra leo. Phasellus tincidunt velit lectus, ac ullamcorper augue bibendum eu. Nulla eu mauris purus. Praesent purus turpis, iaculis in ligula sed, lobortis ornare leo. Suspendisse tincidunt, dolor at feugiat ornare, magna nibh blandit turpis, eget placerat nunc mauris quis ante. Phasellus est ante, commodo et massa at, aliquam bibendum nunc. Nulla facilisi. Nulla sagittis est vitae diam fringilla, vitae posuere orci dictum.</p>',2,11,8),(2,'SilverStripe\\Lessons\\Region','2017-09-26 12:28:06','2017-09-14 15:26:48','The Southeast','<p>Maecenas fringilla nisl eu orci molestie, euismod pretium velit egestas. Aenean ut tortor luctus, vehicula mauris sit amet, mollis libero. Phasellus sit amet tortor et odio semper congue id eu velit. Aliquam cursus pharetra lectus, et tristique felis elementum id. Vestibulum vel porttitor nisl. Duis blandit nibh risus, id sodales magna consectetur sit amet. Maecenas eget congue lorem. Donec quis magna neque. Aliquam in cursus tortor.</p><p>Integer eget fringilla sem, quis faucibus dui. Fusce pretium nunc sit amet magna faucibus, faucibus ornare diam bibendum. Aenean tristique maximus justo, semper interdum odio pellentesque vel. Aenean et condimentum nibh, in congue enim. Aenean erat lorem, rutrum nec lorem vel, euismod dignissim odio. Nullam vitae leo purus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec tincidunt sapien at molestie venenatis. In vehicula faucibus arcu. Maecenas eget dolor ullamcorper, ultricies diam condimentum, rhoncus enim. Aliquam rutrum odio turpis. Aenean eu ligula odio. Maecenas non facilisis enim. Maecenas cursus a sapien non feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>',2,10,8),(3,'SilverStripe\\Lessons\\Region','2017-09-26 12:28:42','2017-09-14 15:27:19','The Southwest','<p>Praesent a sagittis diam. Mauris fermentum urna eget molestie tincidunt. Praesent in pharetra urna, eget faucibus ligula. Donec velit augue, tincidunt vitae nulla a, rhoncus blandit odio. Suspendisse rutrum metus non leo scelerisque, vel dictum ipsum maximus. Pellentesque eu eros imperdiet, sollicitudin orci vel, aliquet odio. In feugiat scelerisque augue quis mollis. Donec sit amet mi eu arcu sodales fringilla nec vitae nisi. Fusce pretium, urna non consectetur pulvinar, enim justo porttitor ligula, vitae malesuada mauris nisl ut sem. Praesent nibh nunc, porta in pharetra a, pretium tincidunt tortor. Quisque ac dictum arcu. Quisque lacinia rutrum consectetur. Fusce tristique turpis metus, ut tristique enim eleifend a. Curabitur imperdiet dapibus felis in mollis. In blandit a augue volutpat venenatis.</p><p>Sed viverra id dui a dictum. Ut id mauris elementum, mollis urna ac, ultrices arcu. Vivamus ac nibh non turpis convallis mattis. Integer vitae volutpat mi. Quisque mi lectus, ornare eu lacinia nec, tincidunt id orci. Nam dictum risus nisl, a pulvinar quam tempor sit amet. Suspendisse eu orci elit. Maecenas cursus fringilla felis, eu dapibus magna blandit ut. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer sollicitudin ipsum quis enim luctus lobortis. Phasellus at pharetra justo.</p><p>Fusce at magna turpis. Vivamus varius posuere tortor. Donec fermentum velit eget nisl mattis sollicitudin. Suspendisse sagittis augue in lectus finibus pulvinar. Donec placerat tellus at accumsan cursus. Vestibulum nec rhoncus odio, sed feugiat lectus. Phasellus euismod pretium finibus. In rutrum nisl efficitur dapibus malesuada. Duis et eros tempor mauris tristique feugiat. Nulla quis mi sed lacus volutpat commodo.</p>',2,8,8),(4,'SilverStripe\\Lessons\\Region','2017-09-26 12:29:14','2017-09-14 15:27:38','The Northwest','<p>Cras ultricies sagittis ante, et sodales eros tempor id. Maecenas et dapibus velit. Nunc lobortis commodo blandit. Proin ullamcorper dignissim nisi quis posuere. Praesent augue elit, vulputate eget odio eu, ultrices venenatis leo. Donec finibus sem id est imperdiet, id imperdiet felis tempor. Quisque feugiat purus a feugiat suscipit. Phasellus ornare id turpis et suscipit. Nulla scelerisque ipsum porta, vestibulum enim ac, semper lacus. In varius et leo sit amet lobortis. Mauris elementum ut felis eget accumsan. Morbi dictum nunc eget sem aliquam, id tempus velit placerat. Vestibulum scelerisque ligula tristique arcu varius dapibus.</p><p>Nulla faucibus ante ut urna laoreet sodales. Nam congue arcu eu elit varius, vitae aliquet lacus tempus. Sed et leo hendrerit, aliquam ipsum a, aliquet massa. Suspendisse pellentesque, massa sed viverra pharetra, turpis orci dapibus lorem, eu sodales tortor sapien ac justo. Nullam lacus ex, commodo eu odio vel, pellentesque gravida nibh. Donec ut fringilla felis. Sed id metus id neque euismod euismod. Aliquam eget dui eleifend, tincidunt orci et, tempor nisl. Nunc a odio libero. Suspendisse mollis erat id pellentesque ullamcorper. Fusce nec magna eu nunc efficitur finibus.</p><p>Nunc vel vehicula nunc, non lacinia elit. Proin gravida tristique odio non gravida. Sed molestie fermentum congue. Vivamus vestibulum aliquet nunc sit amet finibus. Ut rhoncus mauris at felis aliquet, mollis euismod massa egestas. Cras maximus in velit et auctor. Vivamus in venenatis eros. Etiam rhoncus erat auctor velit eleifend eleifend. Fusce lectus erat, blandit et felis vitae, sagittis sodales erat. Donec non congue est. Etiam commodo ante ac dui viverra, volutpat tincidunt felis venenatis. Sed a nisi sed lorem porta gravida tincidunt et ex.</p>',2,9,8);
/*!40000 ALTER TABLE `SilverStripe_Lessons_Region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SilverStripe_Lessons_Region_Live`
--

DROP TABLE IF EXISTS `SilverStripe_Lessons_Region_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SilverStripe_Lessons_Region_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\Lessons\\Region') DEFAULT 'SilverStripe\\Lessons\\Region',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Description` mediumtext,
  `Version` int(11) NOT NULL DEFAULT '0',
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `RegionsPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `PhotoID` (`PhotoID`),
  KEY `RegionsPageID` (`RegionsPageID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SilverStripe_Lessons_Region_Live`
--

LOCK TABLES `SilverStripe_Lessons_Region_Live` WRITE;
/*!40000 ALTER TABLE `SilverStripe_Lessons_Region_Live` DISABLE KEYS */;
INSERT INTO `SilverStripe_Lessons_Region_Live` VALUES (1,'SilverStripe\\Lessons\\Region','2017-09-26 12:27:46','2017-09-14 15:26:25','The Northeast','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque massa felis, hendrerit non tempor quis, hendrerit non sem. Praesent ornare arcu mattis libero sollicitudin, vel dignissim enim placerat. Proin nec risus nulla. Pellentesque quis nulla vitae lorem porta cursus. Aliquam egestas mi ut sem molestie feugiat. Donec maximus facilisis nibh, ultricies placerat lacus cursus eget. Aliquam facilisis est et est bibendum, sed convallis neque cursus. Cras posuere accumsan nibh nec interdum. Vestibulum quam ligula, dignissim vitae tincidunt sit amet, efficitur eleifend libero. Duis quis faucibus tortor. Mauris et scelerisque orci. Phasellus eu neque viverra, pulvinar urna sit amet, hendrerit felis. Morbi id finibus arcu. Etiam nulla felis, auctor quis ultrices in, iaculis sed libero. Phasellus commodo libero vel elit mattis, ac consectetur leo euismod. Nullam feugiat eget nunc tristique vehicula.</p><p>Aliquam leo augue, tempor scelerisque nisi at, gravida pharetra leo. Phasellus tincidunt velit lectus, ac ullamcorper augue bibendum eu. Nulla eu mauris purus. Praesent purus turpis, iaculis in ligula sed, lobortis ornare leo. Suspendisse tincidunt, dolor at feugiat ornare, magna nibh blandit turpis, eget placerat nunc mauris quis ante. Phasellus est ante, commodo et massa at, aliquam bibendum nunc. Nulla facilisi. Nulla sagittis est vitae diam fringilla, vitae posuere orci dictum.</p>',2,11,8),(2,'SilverStripe\\Lessons\\Region','2017-09-26 12:28:06','2017-09-14 15:26:48','The Southeast','<p>Maecenas fringilla nisl eu orci molestie, euismod pretium velit egestas. Aenean ut tortor luctus, vehicula mauris sit amet, mollis libero. Phasellus sit amet tortor et odio semper congue id eu velit. Aliquam cursus pharetra lectus, et tristique felis elementum id. Vestibulum vel porttitor nisl. Duis blandit nibh risus, id sodales magna consectetur sit amet. Maecenas eget congue lorem. Donec quis magna neque. Aliquam in cursus tortor.</p><p>Integer eget fringilla sem, quis faucibus dui. Fusce pretium nunc sit amet magna faucibus, faucibus ornare diam bibendum. Aenean tristique maximus justo, semper interdum odio pellentesque vel. Aenean et condimentum nibh, in congue enim. Aenean erat lorem, rutrum nec lorem vel, euismod dignissim odio. Nullam vitae leo purus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec tincidunt sapien at molestie venenatis. In vehicula faucibus arcu. Maecenas eget dolor ullamcorper, ultricies diam condimentum, rhoncus enim. Aliquam rutrum odio turpis. Aenean eu ligula odio. Maecenas non facilisis enim. Maecenas cursus a sapien non feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>',2,10,8),(3,'SilverStripe\\Lessons\\Region','2017-09-26 12:28:42','2017-09-14 15:27:19','The Southwest','<p>Praesent a sagittis diam. Mauris fermentum urna eget molestie tincidunt. Praesent in pharetra urna, eget faucibus ligula. Donec velit augue, tincidunt vitae nulla a, rhoncus blandit odio. Suspendisse rutrum metus non leo scelerisque, vel dictum ipsum maximus. Pellentesque eu eros imperdiet, sollicitudin orci vel, aliquet odio. In feugiat scelerisque augue quis mollis. Donec sit amet mi eu arcu sodales fringilla nec vitae nisi. Fusce pretium, urna non consectetur pulvinar, enim justo porttitor ligula, vitae malesuada mauris nisl ut sem. Praesent nibh nunc, porta in pharetra a, pretium tincidunt tortor. Quisque ac dictum arcu. Quisque lacinia rutrum consectetur. Fusce tristique turpis metus, ut tristique enim eleifend a. Curabitur imperdiet dapibus felis in mollis. In blandit a augue volutpat venenatis.</p><p>Sed viverra id dui a dictum. Ut id mauris elementum, mollis urna ac, ultrices arcu. Vivamus ac nibh non turpis convallis mattis. Integer vitae volutpat mi. Quisque mi lectus, ornare eu lacinia nec, tincidunt id orci. Nam dictum risus nisl, a pulvinar quam tempor sit amet. Suspendisse eu orci elit. Maecenas cursus fringilla felis, eu dapibus magna blandit ut. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer sollicitudin ipsum quis enim luctus lobortis. Phasellus at pharetra justo.</p><p>Fusce at magna turpis. Vivamus varius posuere tortor. Donec fermentum velit eget nisl mattis sollicitudin. Suspendisse sagittis augue in lectus finibus pulvinar. Donec placerat tellus at accumsan cursus. Vestibulum nec rhoncus odio, sed feugiat lectus. Phasellus euismod pretium finibus. In rutrum nisl efficitur dapibus malesuada. Duis et eros tempor mauris tristique feugiat. Nulla quis mi sed lacus volutpat commodo.</p>',2,8,8),(4,'SilverStripe\\Lessons\\Region','2017-09-26 12:29:14','2017-09-14 15:27:38','The Northwest','<p>Cras ultricies sagittis ante, et sodales eros tempor id. Maecenas et dapibus velit. Nunc lobortis commodo blandit. Proin ullamcorper dignissim nisi quis posuere. Praesent augue elit, vulputate eget odio eu, ultrices venenatis leo. Donec finibus sem id est imperdiet, id imperdiet felis tempor. Quisque feugiat purus a feugiat suscipit. Phasellus ornare id turpis et suscipit. Nulla scelerisque ipsum porta, vestibulum enim ac, semper lacus. In varius et leo sit amet lobortis. Mauris elementum ut felis eget accumsan. Morbi dictum nunc eget sem aliquam, id tempus velit placerat. Vestibulum scelerisque ligula tristique arcu varius dapibus.</p><p>Nulla faucibus ante ut urna laoreet sodales. Nam congue arcu eu elit varius, vitae aliquet lacus tempus. Sed et leo hendrerit, aliquam ipsum a, aliquet massa. Suspendisse pellentesque, massa sed viverra pharetra, turpis orci dapibus lorem, eu sodales tortor sapien ac justo. Nullam lacus ex, commodo eu odio vel, pellentesque gravida nibh. Donec ut fringilla felis. Sed id metus id neque euismod euismod. Aliquam eget dui eleifend, tincidunt orci et, tempor nisl. Nunc a odio libero. Suspendisse mollis erat id pellentesque ullamcorper. Fusce nec magna eu nunc efficitur finibus.</p><p>Nunc vel vehicula nunc, non lacinia elit. Proin gravida tristique odio non gravida. Sed molestie fermentum congue. Vivamus vestibulum aliquet nunc sit amet finibus. Ut rhoncus mauris at felis aliquet, mollis euismod massa egestas. Cras maximus in velit et auctor. Vivamus in venenatis eros. Etiam rhoncus erat auctor velit eleifend eleifend. Fusce lectus erat, blandit et felis vitae, sagittis sodales erat. Donec non congue est. Etiam commodo ante ac dui viverra, volutpat tincidunt felis venenatis. Sed a nisi sed lorem porta gravida tincidunt et ex.</p>',2,9,8);
/*!40000 ALTER TABLE `SilverStripe_Lessons_Region_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SilverStripe_Lessons_Region_Versions`
--

DROP TABLE IF EXISTS `SilverStripe_Lessons_Region_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SilverStripe_Lessons_Region_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SilverStripe\\Lessons\\Region') DEFAULT 'SilverStripe\\Lessons\\Region',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Description` mediumtext,
  `PhotoID` int(11) NOT NULL DEFAULT '0',
  `RegionsPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ClassName` (`ClassName`),
  KEY `PhotoID` (`PhotoID`),
  KEY `RegionsPageID` (`RegionsPageID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SilverStripe_Lessons_Region_Versions`
--

LOCK TABLES `SilverStripe_Lessons_Region_Versions` WRITE;
/*!40000 ALTER TABLE `SilverStripe_Lessons_Region_Versions` DISABLE KEYS */;
INSERT INTO `SilverStripe_Lessons_Region_Versions` VALUES (1,1,1,1,1,1,'SilverStripe\\Lessons\\Region','2017-09-14 15:26:25','2017-09-14 15:26:25','The Northeast','This is the description of the northeast.',11,8),(2,2,1,1,1,1,'SilverStripe\\Lessons\\Region','2017-09-14 15:26:48','2017-09-14 15:26:48','The Southeast','This is a description of the southeast.',10,8),(3,3,1,1,1,1,'SilverStripe\\Lessons\\Region','2017-09-14 15:27:19','2017-09-14 15:27:19','The Southwest','This is the description of the southwest.',8,8),(4,4,1,1,1,1,'SilverStripe\\Lessons\\Region','2017-09-14 15:27:38','2017-09-14 15:27:38','The Northwest','This is the description of the northwest.',9,8),(5,1,2,1,1,1,'SilverStripe\\Lessons\\Region','2017-09-26 12:27:46','2017-09-14 15:26:25','The Northeast','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque massa felis, hendrerit non tempor quis, hendrerit non sem. Praesent ornare arcu mattis libero sollicitudin, vel dignissim enim placerat. Proin nec risus nulla. Pellentesque quis nulla vitae lorem porta cursus. Aliquam egestas mi ut sem molestie feugiat. Donec maximus facilisis nibh, ultricies placerat lacus cursus eget. Aliquam facilisis est et est bibendum, sed convallis neque cursus. Cras posuere accumsan nibh nec interdum. Vestibulum quam ligula, dignissim vitae tincidunt sit amet, efficitur eleifend libero. Duis quis faucibus tortor. Mauris et scelerisque orci. Phasellus eu neque viverra, pulvinar urna sit amet, hendrerit felis. Morbi id finibus arcu. Etiam nulla felis, auctor quis ultrices in, iaculis sed libero. Phasellus commodo libero vel elit mattis, ac consectetur leo euismod. Nullam feugiat eget nunc tristique vehicula.</p><p>Aliquam leo augue, tempor scelerisque nisi at, gravida pharetra leo. Phasellus tincidunt velit lectus, ac ullamcorper augue bibendum eu. Nulla eu mauris purus. Praesent purus turpis, iaculis in ligula sed, lobortis ornare leo. Suspendisse tincidunt, dolor at feugiat ornare, magna nibh blandit turpis, eget placerat nunc mauris quis ante. Phasellus est ante, commodo et massa at, aliquam bibendum nunc. Nulla facilisi. Nulla sagittis est vitae diam fringilla, vitae posuere orci dictum.</p>',11,8),(6,2,2,1,1,1,'SilverStripe\\Lessons\\Region','2017-09-26 12:28:06','2017-09-14 15:26:48','The Southeast','<p>Maecenas fringilla nisl eu orci molestie, euismod pretium velit egestas. Aenean ut tortor luctus, vehicula mauris sit amet, mollis libero. Phasellus sit amet tortor et odio semper congue id eu velit. Aliquam cursus pharetra lectus, et tristique felis elementum id. Vestibulum vel porttitor nisl. Duis blandit nibh risus, id sodales magna consectetur sit amet. Maecenas eget congue lorem. Donec quis magna neque. Aliquam in cursus tortor.</p><p>Integer eget fringilla sem, quis faucibus dui. Fusce pretium nunc sit amet magna faucibus, faucibus ornare diam bibendum. Aenean tristique maximus justo, semper interdum odio pellentesque vel. Aenean et condimentum nibh, in congue enim. Aenean erat lorem, rutrum nec lorem vel, euismod dignissim odio. Nullam vitae leo purus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec tincidunt sapien at molestie venenatis. In vehicula faucibus arcu. Maecenas eget dolor ullamcorper, ultricies diam condimentum, rhoncus enim. Aliquam rutrum odio turpis. Aenean eu ligula odio. Maecenas non facilisis enim. Maecenas cursus a sapien non feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>',10,8),(7,3,2,1,1,1,'SilverStripe\\Lessons\\Region','2017-09-26 12:28:42','2017-09-14 15:27:19','The Southwest','<p>Praesent a sagittis diam. Mauris fermentum urna eget molestie tincidunt. Praesent in pharetra urna, eget faucibus ligula. Donec velit augue, tincidunt vitae nulla a, rhoncus blandit odio. Suspendisse rutrum metus non leo scelerisque, vel dictum ipsum maximus. Pellentesque eu eros imperdiet, sollicitudin orci vel, aliquet odio. In feugiat scelerisque augue quis mollis. Donec sit amet mi eu arcu sodales fringilla nec vitae nisi. Fusce pretium, urna non consectetur pulvinar, enim justo porttitor ligula, vitae malesuada mauris nisl ut sem. Praesent nibh nunc, porta in pharetra a, pretium tincidunt tortor. Quisque ac dictum arcu. Quisque lacinia rutrum consectetur. Fusce tristique turpis metus, ut tristique enim eleifend a. Curabitur imperdiet dapibus felis in mollis. In blandit a augue volutpat venenatis.</p><p>Sed viverra id dui a dictum. Ut id mauris elementum, mollis urna ac, ultrices arcu. Vivamus ac nibh non turpis convallis mattis. Integer vitae volutpat mi. Quisque mi lectus, ornare eu lacinia nec, tincidunt id orci. Nam dictum risus nisl, a pulvinar quam tempor sit amet. Suspendisse eu orci elit. Maecenas cursus fringilla felis, eu dapibus magna blandit ut. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer sollicitudin ipsum quis enim luctus lobortis. Phasellus at pharetra justo.</p><p>Fusce at magna turpis. Vivamus varius posuere tortor. Donec fermentum velit eget nisl mattis sollicitudin. Suspendisse sagittis augue in lectus finibus pulvinar. Donec placerat tellus at accumsan cursus. Vestibulum nec rhoncus odio, sed feugiat lectus. Phasellus euismod pretium finibus. In rutrum nisl efficitur dapibus malesuada. Duis et eros tempor mauris tristique feugiat. Nulla quis mi sed lacus volutpat commodo.</p>',8,8),(8,4,2,1,1,1,'SilverStripe\\Lessons\\Region','2017-09-26 12:29:14','2017-09-14 15:27:38','The Northwest','<p>Cras ultricies sagittis ante, et sodales eros tempor id. Maecenas et dapibus velit. Nunc lobortis commodo blandit. Proin ullamcorper dignissim nisi quis posuere. Praesent augue elit, vulputate eget odio eu, ultrices venenatis leo. Donec finibus sem id est imperdiet, id imperdiet felis tempor. Quisque feugiat purus a feugiat suscipit. Phasellus ornare id turpis et suscipit. Nulla scelerisque ipsum porta, vestibulum enim ac, semper lacus. In varius et leo sit amet lobortis. Mauris elementum ut felis eget accumsan. Morbi dictum nunc eget sem aliquam, id tempus velit placerat. Vestibulum scelerisque ligula tristique arcu varius dapibus.</p><p>Nulla faucibus ante ut urna laoreet sodales. Nam congue arcu eu elit varius, vitae aliquet lacus tempus. Sed et leo hendrerit, aliquam ipsum a, aliquet massa. Suspendisse pellentesque, massa sed viverra pharetra, turpis orci dapibus lorem, eu sodales tortor sapien ac justo. Nullam lacus ex, commodo eu odio vel, pellentesque gravida nibh. Donec ut fringilla felis. Sed id metus id neque euismod euismod. Aliquam eget dui eleifend, tincidunt orci et, tempor nisl. Nunc a odio libero. Suspendisse mollis erat id pellentesque ullamcorper. Fusce nec magna eu nunc efficitur finibus.</p><p>Nunc vel vehicula nunc, non lacinia elit. Proin gravida tristique odio non gravida. Sed molestie fermentum congue. Vivamus vestibulum aliquet nunc sit amet finibus. Ut rhoncus mauris at felis aliquet, mollis euismod massa egestas. Cras maximus in velit et auctor. Vivamus in venenatis eros. Etiam rhoncus erat auctor velit eleifend eleifend. Fusce lectus erat, blandit et felis vitae, sagittis sodales erat. Donec non congue est. Etiam commodo ante ac dui viverra, volutpat tincidunt felis venenatis. Sed a nisi sed lorem porta gravida tincidunt et ex.</p>',9,8);
/*!40000 ALTER TABLE `SilverStripe_Lessons_Region_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteConfig`
--

DROP TABLE IF EXISTS `SiteConfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteConfig` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\SiteConfig\\SiteConfig') CHARACTER SET utf8 DEFAULT 'SilverStripe\\SiteConfig\\SiteConfig',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Tagline` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'Anyone',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  `CanCreateTopLevelType` enum('LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  `FacebookLink` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TwitterLink` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `GoogleLink` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `YoutubeLink` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `FooterContent` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteConfig`
--

LOCK TABLES `SiteConfig` WRITE;
/*!40000 ALTER TABLE `SiteConfig` DISABLE KEYS */;
INSERT INTO `SiteConfig` VALUES (1,'SilverStripe\\SiteConfig\\SiteConfig','2018-11-29 14:22:45','2018-10-10 15:08:06','Your Site Name','your tagline here','Anyone','LoggedInUsers','LoggedInUsers','facebook.com/example','twitter.com/example','google.com/example','youtube.com/example','Footer content string yay');
/*!40000 ALTER TABLE `SiteConfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteConfig_CreateTopLevelGroups`
--

DROP TABLE IF EXISTS `SiteConfig_CreateTopLevelGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteConfig_CreateTopLevelGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteConfig_CreateTopLevelGroups`
--

LOCK TABLES `SiteConfig_CreateTopLevelGroups` WRITE;
/*!40000 ALTER TABLE `SiteConfig_CreateTopLevelGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteConfig_CreateTopLevelGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteConfig_EditorGroups`
--

DROP TABLE IF EXISTS `SiteConfig_EditorGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteConfig_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteConfig_EditorGroups`
--

LOCK TABLES `SiteConfig_EditorGroups` WRITE;
/*!40000 ALTER TABLE `SiteConfig_EditorGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteConfig_EditorGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteConfig_ViewerGroups`
--

DROP TABLE IF EXISTS `SiteConfig_ViewerGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteConfig_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteConfig_ViewerGroups`
--

LOCK TABLES `SiteConfig_ViewerGroups` WRITE;
/*!40000 ALTER TABLE `SiteConfig_ViewerGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteConfig_ViewerGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTree`
--

DROP TABLE IF EXISTS `SiteTree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\CMS\\Model\\SiteTree','Page','ArticleHolder','ArticlePage','HomePage','PropertySearchPage','RegionsPage','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage') CHARACTER SET utf8 DEFAULT 'Page',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Version` int(11) NOT NULL DEFAULT '0',
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTree`
--

LOCK TABLES `SiteTree` WRITE;
/*!40000 ALTER TABLE `SiteTree` DISABLE KEYS */;
INSERT INTO `SiteTree` VALUES (1,'HomePage','2018-12-06 14:25:19','2018-10-10 15:08:06','Inherit','Inherit',6,'home','Home',NULL,'<h1>Lorem ipsum</h1><p>dolor sit amet, consectetur adipiscing elit. Sed et hendrerit libero. Praesent et ex mattis, viverra risus eu, fringilla orci. Nullam egestas felis sollicitudin, tristique sapien in, malesuada dui. Duis pulvinar magna a tempor pharetra. Etiam id urna urna. Ut vestibulum convallis hendrerit. Sed tincidunt, orci eu egestas dignissim, nunc neque fringilla diam, ac dapibus nunc nulla in nisl. Proin dui enim, pharetra non pellentesque et, ultricies in leo. Phasellus ac semper est, at porta sem. Integer in lobortis velit, ut interdum purus. Proin consectetur neque sed neque faucibus, ut scelerisque nulla maximus. Mauris libero odio, fringilla consequat tristique id, vulputate a nisl.</p><p>Donec posuere, neque iaculis posuere eleifend, odio erat feugiat metus, sed maximus nisl tortor sed dolor. In hac habitasse platea dictumst. Nam velit tortor, iaculis id purus in, sollicitudin tempor ligula. Morbi lacinia lobortis metus. Proin suscipit orci quis libero vestibulum interdum. Etiam aliquet euismod est non sollicitudin. Pellentesque non egestas lectus. Curabitur sollicitudin risus id mauris malesuada, eget molestie mi euismod. Donec sem velit, aliquam non hendrerit eget, auctor eget lectus. Praesent aliquet rutrum augue, ut ultrices lacus sagittis ac. Donec et pharetra massa. Nam ac neque vehicula, volutpat tellus sed, ultricies erat. Duis fermentum mattis erat vel feugiat. Quisque sollicitudin urna sit amet massa egestas pellentesque. Cras mattis dapibus euismod. Nulla facilisi.</p>',NULL,NULL,1,1,1,0,0,NULL,0),(2,'Page','2018-12-06 14:25:48','2018-10-10 15:08:06','Inherit','Inherit',4,'about-us','About Us',NULL,'<p>usce dapibus leo in lobortis auctor. Quisque molestie vel dolor in venenatis. Sed tristique tristique neque eu fermentum. Maecenas a erat accumsan, hendrerit augue quis, iaculis nulla. Aenean in diam mi. Nam quis dui a lectus ultrices cursus et ac ipsum. Mauris bibendum turpis nisi, vel aliquet purus pharetra nec. Sed non est urna. Vivamus justo nisl, luctus a ligula a, luctus feugiat velit. Ut viverra, augue id tempor imperdiet, purus arcu venenatis felis, eleifend volutpat ex dolor et neque. Etiam tincidunt convallis condimentum. Etiam nisi dolor, bibendum maximus volutpat sed, faucibus quis eros.</p><p>Donec gravida nibh quis tellus viverra finibus. Nulla facilisi. In vitae faucibus sapien, vel tincidunt ante. In hac habitasse platea dictumst. Suspendisse potenti. Donec fermentum lacus turpis, ac ornare metus malesuada sit amet. Ut pulvinar, massa ultricies euismod eleifend, dui ipsum commodo sapien, non posuere turpis justo eu nunc. Donec felis ipsum, mattis venenatis tempus quis, elementum non erat. Mauris a sem orci. Aenean eu mattis arcu. Fusce mollis fermentum neque ut accumsan.</p><p>Donec nec augue nibh. Integer non ultricies erat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In hac habitasse platea dictumst. Sed congue enim hendrerit accumsan consectetur. Ut quis augue sapien. Nulla ultricies risus at lorem ultricies dictum. Mauris volutpat quis sapien vitae imperdiet. Nam eu efficitur lectus. Aliquam velit eros, vulputate et facilisis id, ornare ut elit.</p>',NULL,NULL,1,1,2,0,0,NULL,0),(4,'SilverStripe\\ErrorPage\\ErrorPage','2018-10-10 15:08:06','2018-10-10 15:08:06','Inherit','Inherit',2,'page-not-found','Page not found',NULL,'<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>',NULL,NULL,0,0,4,0,0,NULL,0),(5,'SilverStripe\\ErrorPage\\ErrorPage','2018-10-10 15:08:07','2018-10-10 15:08:07','Inherit','Inherit',2,'server-error','Server error',NULL,'<p>Sorry, there was a problem handling your request.</p>',NULL,NULL,0,0,5,0,0,NULL,0),(6,'Page','2018-10-10 16:28:51','2018-10-10 16:28:09','Inherit','Inherit',3,'detail-about-us','Detail About Us',NULL,'<p>This is some content in a&nbsp;sub page of \'about-us\'</p>',NULL,NULL,1,1,1,0,0,NULL,2),(8,'ArticleHolder','2019-01-18 10:30:15','2018-10-10 17:24:03','Inherit','Inherit',11,'travel-guides','Travel Guides',NULL,'<p>Primary text for \'Travel Guides\' page</p>',NULL,NULL,1,1,3,0,0,NULL,0),(9,'ArticlePage','2019-01-18 14:14:15','2018-10-10 17:25:14','Inherit','Inherit',19,'article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(12,'Page','2018-10-11 10:04:27','2018-10-11 10:04:11','Inherit','Inherit',3,'more-details-1','More Details 1',NULL,'<p>More Details 1 content</p>',NULL,NULL,1,1,2,0,0,NULL,2),(13,'Page','2018-10-11 10:04:55','2018-10-11 10:04:40','Inherit','Inherit',3,'more-details-2','More Details 2',NULL,'<p>More Details 2 content</p>',NULL,NULL,1,1,3,0,0,NULL,2),(14,'ArticlePage','2019-01-18 14:14:31','2018-10-11 16:18:59','Inherit','Inherit',11,'article-item-2','Article Item 2',NULL,'<p>Article Item 2 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(15,'ArticlePage','2018-10-12 14:28:09','2018-10-11 16:19:25','Inherit','Inherit',7,'article-item-3','Article Item 3',NULL,'<p>Article Item 3 Content</p>',NULL,NULL,0,1,3,0,0,NULL,8),(16,'ArticlePage','2018-10-12 14:28:22','2018-10-11 16:19:48','Inherit','Inherit',9,'article-item-4','Article Item 4',NULL,'<p>Article Item 4 Content</p>',NULL,NULL,0,1,4,0,0,NULL,8),(17,'RegionsPage','2018-10-12 11:51:33','2018-10-12 10:47:21','Inherit','Inherit',5,'regions','Regions',NULL,'<p>Regions default content area</p>',NULL,NULL,1,1,6,0,0,NULL,0),(18,'PropertySearchPage','2019-01-17 10:36:18','2019-01-17 10:35:58','Inherit','Inherit',3,'find-a-rental','Find a Rental',NULL,'<p>Find a Rental content</p>',NULL,NULL,1,1,7,0,0,NULL,0);
/*!40000 ALTER TABLE `SiteTree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTreeLink`
--

DROP TABLE IF EXISTS `SiteTreeLink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTreeLink` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\CMS\\Model\\SiteTreeLink') CHARACTER SET utf8 DEFAULT 'SilverStripe\\CMS\\Model\\SiteTreeLink',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `LinkedID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `ParentClass` enum('ArticleCategory','ArticleComment','Property','Region','SilverStripe\\Assets\\File','SilverStripe\\SiteConfig\\SiteConfig','SilverStripe\\Versioned\\ChangeSet','SilverStripe\\Versioned\\ChangeSetItem','SilverStripe\\Assets\\Shortcodes\\FileLink','SilverStripe\\CMS\\Model\\SiteTree','SilverStripe\\CMS\\Model\\SiteTreeLink','SilverStripe\\Security\\Group','SilverStripe\\Security\\LoginAttempt','SilverStripe\\Security\\Member','SilverStripe\\Security\\MemberPassword','SilverStripe\\Security\\Permission','SilverStripe\\Security\\PermissionRole','SilverStripe\\Security\\PermissionRoleCode','SilverStripe\\Security\\RememberLoginHash','SilverStripe\\Assets\\Folder','SilverStripe\\Assets\\Image','Page','ArticleHolder','ArticlePage','HomePage','PropertySearchPage','RegionsPage','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage') CHARACTER SET utf8 DEFAULT 'ArticleCategory',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `LinkedID` (`LinkedID`),
  KEY `Parent` (`ParentID`,`ParentClass`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTreeLink`
--

LOCK TABLES `SiteTreeLink` WRITE;
/*!40000 ALTER TABLE `SiteTreeLink` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTreeLink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTree_EditorGroups`
--

DROP TABLE IF EXISTS `SiteTree_EditorGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTree_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTree_EditorGroups`
--

LOCK TABLES `SiteTree_EditorGroups` WRITE;
/*!40000 ALTER TABLE `SiteTree_EditorGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTree_EditorGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTree_ImageTracking`
--

DROP TABLE IF EXISTS `SiteTree_ImageTracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTree_ImageTracking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `FileID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `FileID` (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTree_ImageTracking`
--

LOCK TABLES `SiteTree_ImageTracking` WRITE;
/*!40000 ALTER TABLE `SiteTree_ImageTracking` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTree_ImageTracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTree_LinkTracking`
--

DROP TABLE IF EXISTS `SiteTree_LinkTracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTree_LinkTracking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `ChildID` (`ChildID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTree_LinkTracking`
--

LOCK TABLES `SiteTree_LinkTracking` WRITE;
/*!40000 ALTER TABLE `SiteTree_LinkTracking` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTree_LinkTracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTree_Live`
--

DROP TABLE IF EXISTS `SiteTree_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTree_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SilverStripe\\CMS\\Model\\SiteTree','Page','ArticleHolder','ArticlePage','HomePage','PropertySearchPage','RegionsPage','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage') CHARACTER SET utf8 DEFAULT 'Page',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Version` int(11) NOT NULL DEFAULT '0',
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTree_Live`
--

LOCK TABLES `SiteTree_Live` WRITE;
/*!40000 ALTER TABLE `SiteTree_Live` DISABLE KEYS */;
INSERT INTO `SiteTree_Live` VALUES (1,'HomePage','2018-12-06 14:25:19','2018-10-10 15:08:06','Inherit','Inherit',6,'home','Home',NULL,'<h1>Lorem ipsum</h1><p>dolor sit amet, consectetur adipiscing elit. Sed et hendrerit libero. Praesent et ex mattis, viverra risus eu, fringilla orci. Nullam egestas felis sollicitudin, tristique sapien in, malesuada dui. Duis pulvinar magna a tempor pharetra. Etiam id urna urna. Ut vestibulum convallis hendrerit. Sed tincidunt, orci eu egestas dignissim, nunc neque fringilla diam, ac dapibus nunc nulla in nisl. Proin dui enim, pharetra non pellentesque et, ultricies in leo. Phasellus ac semper est, at porta sem. Integer in lobortis velit, ut interdum purus. Proin consectetur neque sed neque faucibus, ut scelerisque nulla maximus. Mauris libero odio, fringilla consequat tristique id, vulputate a nisl.</p><p>Donec posuere, neque iaculis posuere eleifend, odio erat feugiat metus, sed maximus nisl tortor sed dolor. In hac habitasse platea dictumst. Nam velit tortor, iaculis id purus in, sollicitudin tempor ligula. Morbi lacinia lobortis metus. Proin suscipit orci quis libero vestibulum interdum. Etiam aliquet euismod est non sollicitudin. Pellentesque non egestas lectus. Curabitur sollicitudin risus id mauris malesuada, eget molestie mi euismod. Donec sem velit, aliquam non hendrerit eget, auctor eget lectus. Praesent aliquet rutrum augue, ut ultrices lacus sagittis ac. Donec et pharetra massa. Nam ac neque vehicula, volutpat tellus sed, ultricies erat. Duis fermentum mattis erat vel feugiat. Quisque sollicitudin urna sit amet massa egestas pellentesque. Cras mattis dapibus euismod. Nulla facilisi.</p>',NULL,NULL,1,1,1,0,0,NULL,0),(2,'Page','2018-12-06 14:25:48','2018-10-10 15:08:06','Inherit','Inherit',4,'about-us','About Us',NULL,'<p>usce dapibus leo in lobortis auctor. Quisque molestie vel dolor in venenatis. Sed tristique tristique neque eu fermentum. Maecenas a erat accumsan, hendrerit augue quis, iaculis nulla. Aenean in diam mi. Nam quis dui a lectus ultrices cursus et ac ipsum. Mauris bibendum turpis nisi, vel aliquet purus pharetra nec. Sed non est urna. Vivamus justo nisl, luctus a ligula a, luctus feugiat velit. Ut viverra, augue id tempor imperdiet, purus arcu venenatis felis, eleifend volutpat ex dolor et neque. Etiam tincidunt convallis condimentum. Etiam nisi dolor, bibendum maximus volutpat sed, faucibus quis eros.</p><p>Donec gravida nibh quis tellus viverra finibus. Nulla facilisi. In vitae faucibus sapien, vel tincidunt ante. In hac habitasse platea dictumst. Suspendisse potenti. Donec fermentum lacus turpis, ac ornare metus malesuada sit amet. Ut pulvinar, massa ultricies euismod eleifend, dui ipsum commodo sapien, non posuere turpis justo eu nunc. Donec felis ipsum, mattis venenatis tempus quis, elementum non erat. Mauris a sem orci. Aenean eu mattis arcu. Fusce mollis fermentum neque ut accumsan.</p><p>Donec nec augue nibh. Integer non ultricies erat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In hac habitasse platea dictumst. Sed congue enim hendrerit accumsan consectetur. Ut quis augue sapien. Nulla ultricies risus at lorem ultricies dictum. Mauris volutpat quis sapien vitae imperdiet. Nam eu efficitur lectus. Aliquam velit eros, vulputate et facilisis id, ornare ut elit.</p>',NULL,NULL,1,1,2,0,0,NULL,0),(4,'SilverStripe\\ErrorPage\\ErrorPage','2018-10-10 15:08:06','2018-10-10 15:08:06','Inherit','Inherit',2,'page-not-found','Page not found',NULL,'<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>',NULL,NULL,0,0,4,0,0,NULL,0),(5,'SilverStripe\\ErrorPage\\ErrorPage','2018-10-10 15:08:07','2018-10-10 15:08:07','Inherit','Inherit',2,'server-error','Server error',NULL,'<p>Sorry, there was a problem handling your request.</p>',NULL,NULL,0,0,5,0,0,NULL,0),(6,'Page','2018-10-10 16:28:51','2018-10-10 16:28:09','Inherit','Inherit',3,'detail-about-us','Detail About Us',NULL,'<p>This is some content in a&nbsp;sub page of \'about-us\'</p>',NULL,NULL,1,1,1,0,0,NULL,2),(8,'ArticleHolder','2019-01-18 10:30:15','2018-10-10 17:24:03','Inherit','Inherit',11,'travel-guides','Travel Guides',NULL,'<p>Primary text for \'Travel Guides\' page</p>',NULL,NULL,1,1,3,0,0,NULL,0),(9,'ArticlePage','2019-01-18 14:14:15','2018-10-10 17:25:14','Inherit','Inherit',19,'article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(12,'Page','2018-10-11 10:04:27','2018-10-11 10:04:11','Inherit','Inherit',3,'more-details-1','More Details 1',NULL,'<p>More Details 1 content</p>',NULL,NULL,1,1,2,0,0,NULL,2),(13,'Page','2018-10-11 10:04:55','2018-10-11 10:04:40','Inherit','Inherit',3,'more-details-2','More Details 2',NULL,'<p>More Details 2 content</p>',NULL,NULL,1,1,3,0,0,NULL,2),(14,'ArticlePage','2019-01-18 14:14:31','2018-10-11 16:18:59','Inherit','Inherit',11,'article-item-2','Article Item 2',NULL,'<p>Article Item 2 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(15,'ArticlePage','2018-10-11 16:42:46','2018-10-11 16:19:25','Inherit','Inherit',7,'article-item-3','Article Item 3',NULL,'<p>Article Item 3 Content</p>',NULL,NULL,0,1,3,0,0,NULL,8),(16,'ArticlePage','2018-10-11 16:57:48','2018-10-11 16:19:48','Inherit','Inherit',9,'article-item-4','Article Item 4',NULL,'<p>Article Item 4 Content</p>',NULL,NULL,0,1,4,0,0,NULL,8),(17,'RegionsPage','2018-10-12 10:50:07','2018-10-12 10:47:21','Inherit','Inherit',5,'regions','Regions',NULL,'<p>Regions default content area</p>',NULL,NULL,1,1,6,0,0,NULL,0),(18,'PropertySearchPage','2019-01-17 10:36:18','2019-01-17 10:35:58','Inherit','Inherit',3,'find-a-rental','Find a Rental',NULL,'<p>Find a Rental content</p>',NULL,NULL,1,1,7,0,0,NULL,0);
/*!40000 ALTER TABLE `SiteTree_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTree_Versions`
--

DROP TABLE IF EXISTS `SiteTree_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTree_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `WasDraft` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SilverStripe\\CMS\\Model\\SiteTree','Page','ArticleHolder','ArticlePage','HomePage','PropertySearchPage','RegionsPage','SilverStripe\\ErrorPage\\ErrorPage','SilverStripe\\CMS\\Model\\RedirectorPage','SilverStripe\\CMS\\Model\\VirtualPage','','GalleryHolder','GalleryPage') CHARACTER SET utf8 DEFAULT 'Page',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTree_Versions`
--

LOCK TABLES `SiteTree_Versions` WRITE;
/*!40000 ALTER TABLE `SiteTree_Versions` DISABLE KEYS */;
INSERT INTO `SiteTree_Versions` VALUES (1,1,1,0,0,1,0,0,'Page','2018-10-10 15:08:06','2018-10-10 15:08:06','Inherit','Inherit','home','Home',NULL,'<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href=\"admin/\">the CMS</a>.</p><p>You can now access the <a href=\"http://docs.silverstripe.org\">developer documentation</a>, or begin the <a href=\"http://www.silverstripe.org/learn/lessons\">SilverStripe lessons</a>.</p>',NULL,NULL,1,1,1,0,0,NULL,0),(2,1,2,1,0,1,0,0,'Page','2018-10-10 15:08:06','2018-10-10 15:08:06','Inherit','Inherit','home','Home',NULL,'<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href=\"admin/\">the CMS</a>.</p><p>You can now access the <a href=\"http://docs.silverstripe.org\">developer documentation</a>, or begin the <a href=\"http://www.silverstripe.org/learn/lessons\">SilverStripe lessons</a>.</p>',NULL,NULL,1,1,1,0,0,NULL,0),(3,2,1,0,0,1,0,0,'Page','2018-10-10 15:08:06','2018-10-10 15:08:06','Inherit','Inherit','about-us','About Us',NULL,'<p>You can fill this page out with your own content, or delete it and create your own pages.</p>',NULL,NULL,1,1,2,0,0,NULL,0),(4,2,2,1,0,1,0,0,'Page','2018-10-10 15:08:06','2018-10-10 15:08:06','Inherit','Inherit','about-us','About Us',NULL,'<p>You can fill this page out with your own content, or delete it and create your own pages.</p>',NULL,NULL,1,1,2,0,0,NULL,0),(5,3,1,0,0,1,0,0,'Page','2018-10-10 15:08:06','2018-10-10 15:08:06','Inherit','Inherit','contact-us','Contact Us',NULL,'<p>You can fill this page out with your own content, or delete it and create your own pages.</p>',NULL,NULL,1,1,3,0,0,NULL,0),(6,3,2,1,0,1,0,0,'Page','2018-10-10 15:08:06','2018-10-10 15:08:06','Inherit','Inherit','contact-us','Contact Us',NULL,'<p>You can fill this page out with your own content, or delete it and create your own pages.</p>',NULL,NULL,1,1,3,0,0,NULL,0),(7,4,1,0,0,1,0,0,'SilverStripe\\ErrorPage\\ErrorPage','2018-10-10 15:08:06','2018-10-10 15:08:06','Inherit','Inherit','page-not-found','Page not found',NULL,'<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>',NULL,NULL,0,0,4,0,0,NULL,0),(8,4,2,1,0,1,0,0,'SilverStripe\\ErrorPage\\ErrorPage','2018-10-10 15:08:06','2018-10-10 15:08:06','Inherit','Inherit','page-not-found','Page not found',NULL,'<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>',NULL,NULL,0,0,4,0,0,NULL,0),(9,5,1,0,0,1,0,0,'SilverStripe\\ErrorPage\\ErrorPage','2018-10-10 15:08:07','2018-10-10 15:08:07','Inherit','Inherit','server-error','Server error',NULL,'<p>Sorry, there was a problem handling your request.</p>',NULL,NULL,0,0,5,0,0,NULL,0),(10,5,2,1,0,1,0,0,'SilverStripe\\ErrorPage\\ErrorPage','2018-10-10 15:08:07','2018-10-10 15:08:07','Inherit','Inherit','server-error','Server error',NULL,'<p>Sorry, there was a problem handling your request.</p>',NULL,NULL,0,0,5,0,0,NULL,0),(11,6,1,0,0,1,1,0,'Page','2018-10-10 16:28:09','2018-10-10 16:28:09','Inherit','Inherit','new-page','New Page',NULL,NULL,NULL,NULL,1,1,1,0,0,NULL,2),(12,6,2,0,0,1,1,0,'Page','2018-10-10 16:28:51','2018-10-10 16:28:09','Inherit','Inherit','detail-about-us','Detail About Us',NULL,'<p>This is some content in a&nbsp;sub page of \'about-us\'</p>',NULL,NULL,1,1,1,0,0,NULL,2),(13,6,3,1,0,1,1,1,'Page','2018-10-10 16:28:51','2018-10-10 16:28:09','Inherit','Inherit','detail-about-us','Detail About Us',NULL,'<p>This is some content in a&nbsp;sub page of \'about-us\'</p>',NULL,NULL,1,1,1,0,0,NULL,2),(14,1,3,0,0,1,1,0,'HomePage','2018-10-10 16:47:24','2018-10-10 15:08:06','Inherit','Inherit','home','Home',NULL,'<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href=\"admin/\">the CMS</a>.</p><p>You can now access the <a href=\"http://docs.silverstripe.org\">developer documentation</a>, or begin the <a href=\"http://www.silverstripe.org/learn/lessons\">SilverStripe lessons</a>.</p>',NULL,NULL,1,1,1,0,0,NULL,0),(15,1,4,1,0,1,1,1,'HomePage','2018-10-10 16:47:24','2018-10-10 15:08:06','Inherit','Inherit','home','Home',NULL,'<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href=\"admin/\">the CMS</a>.</p><p>You can now access the <a href=\"http://docs.silverstripe.org\">developer documentation</a>, or begin the <a href=\"http://www.silverstripe.org/learn/lessons\">SilverStripe lessons</a>.</p>',NULL,NULL,1,1,1,0,0,NULL,0),(16,7,1,0,0,1,1,0,'Page','2018-10-10 16:55:03','2018-10-10 16:55:03','Inherit','Inherit','new-page','New Page',NULL,NULL,NULL,NULL,1,1,6,0,0,NULL,0),(17,7,2,1,1,1,1,1,'Page','2018-10-10 16:56:37','2018-10-10 16:55:03','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(18,3,3,1,1,1,1,1,'Page','2018-10-10 17:23:41','2018-10-10 15:08:06','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(19,8,1,0,0,1,1,0,'GalleryHolder','2018-10-10 17:24:03','2018-10-10 17:24:03','Inherit','Inherit','new-gallery-holder','New Gallery Holder',NULL,NULL,NULL,NULL,1,1,6,0,0,NULL,0),(20,8,2,0,0,1,1,0,'GalleryHolder','2018-10-10 17:24:23','2018-10-10 17:24:03','Inherit','Inherit','gallery','Gallery',NULL,'<p>Primary text for \'Gallery\' page</p>',NULL,NULL,1,1,6,0,0,NULL,0),(21,8,3,1,0,1,1,1,'GalleryHolder','2018-10-10 17:24:23','2018-10-10 17:24:03','Inherit','Inherit','gallery','Gallery',NULL,'<p>Primary text for \'Gallery\' page</p>',NULL,NULL,1,1,6,0,0,NULL,0),(22,9,1,0,0,1,1,0,'GalleryPage','2018-10-10 17:25:14','2018-10-10 17:25:14','Inherit','Inherit','new-gallery-page','New Gallery Page',NULL,NULL,NULL,NULL,1,1,1,0,0,NULL,8),(23,9,2,0,0,1,1,0,'GalleryPage','2018-10-10 17:25:39','2018-10-10 17:25:14','Inherit','Inherit','gallery-item-1','Gallery Item 1',NULL,'<p>Gallery Item 1 Content</p>',NULL,NULL,1,1,1,0,0,NULL,8),(24,9,3,1,0,1,1,1,'GalleryPage','2018-10-10 17:25:39','2018-10-10 17:25:14','Inherit','Inherit','gallery-item-1','Gallery Item 1',NULL,'<p>Gallery Item 1 Content</p>',NULL,NULL,1,1,1,0,0,NULL,8),(25,10,1,0,0,1,1,0,'GalleryPage','2018-10-10 17:25:55','2018-10-10 17:25:55','Inherit','Inherit','new-gallery-page','New Gallery Page',NULL,NULL,NULL,NULL,1,1,2,0,0,NULL,8),(26,10,2,0,0,1,1,0,'GalleryPage','2018-10-10 17:26:14','2018-10-10 17:25:55','Inherit','Inherit','gallery-item-2','Gallery Item 2',NULL,'<p>Gallery Item 2 content</p>',NULL,NULL,1,1,2,0,0,NULL,8),(27,10,3,1,0,1,1,1,'GalleryPage','2018-10-10 17:26:14','2018-10-10 17:25:55','Inherit','Inherit','gallery-item-2','Gallery Item 2',NULL,'<p>Gallery Item 2 content</p>',NULL,NULL,1,1,2,0,0,NULL,8),(28,11,1,0,0,1,1,0,'GalleryPage','2018-10-10 17:26:33','2018-10-10 17:26:33','Inherit','Inherit','new-gallery-page','New Gallery Page',NULL,NULL,NULL,NULL,1,1,3,0,0,NULL,8),(29,11,2,0,0,1,1,0,'GalleryPage','2018-10-10 17:26:47','2018-10-10 17:26:33','Inherit','Inherit','gallery-item-3','Gallery Item 3',NULL,'<p>Gallery Item 3 content</p>',NULL,NULL,1,1,3,0,0,NULL,8),(30,11,3,1,0,1,1,1,'GalleryPage','2018-10-10 17:26:47','2018-10-10 17:26:33','Inherit','Inherit','gallery-item-3','Gallery Item 3',NULL,'<p>Gallery Item 3 content</p>',NULL,NULL,1,1,3,0,0,NULL,8),(31,9,4,0,0,1,1,0,'GalleryPage','2018-10-10 17:40:41','2018-10-10 17:25:14','Inherit','Inherit','gallery-item-1','Gallery Item 1',NULL,'<p>Gallery Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(32,9,5,1,0,1,1,1,'GalleryPage','2018-10-10 17:40:42','2018-10-10 17:25:14','Inherit','Inherit','gallery-item-1','Gallery Item 1',NULL,'<p>Gallery Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(33,10,4,0,0,1,1,0,'GalleryPage','2018-10-10 17:41:00','2018-10-10 17:25:55','Inherit','Inherit','gallery-item-2','Gallery Item 2',NULL,'<p>Gallery Item 2 content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(34,10,5,1,0,1,1,1,'GalleryPage','2018-10-10 17:41:00','2018-10-10 17:25:55','Inherit','Inherit','gallery-item-2','Gallery Item 2',NULL,'<p>Gallery Item 2 content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(35,11,4,0,0,1,1,0,'GalleryPage','2018-10-10 17:41:18','2018-10-10 17:26:33','Inherit','Inherit','gallery-item-3','Gallery Item 3',NULL,'<p>Gallery Item 3 content</p>',NULL,NULL,0,1,3,0,0,NULL,8),(36,11,5,1,0,1,1,1,'GalleryPage','2018-10-10 17:41:18','2018-10-10 17:26:33','Inherit','Inherit','gallery-item-3','Gallery Item 3',NULL,'<p>Gallery Item 3 content</p>',NULL,NULL,0,1,3,0,0,NULL,8),(37,12,1,0,0,1,1,0,'Page','2018-10-11 10:04:11','2018-10-11 10:04:11','Inherit','Inherit','new-page','New Page',NULL,NULL,NULL,NULL,1,1,2,0,0,NULL,2),(38,12,2,0,0,1,1,0,'Page','2018-10-11 10:04:27','2018-10-11 10:04:11','Inherit','Inherit','more-details-1','More Details 1',NULL,'<p>More Details 1 content</p>',NULL,NULL,1,1,2,0,0,NULL,2),(39,12,3,1,0,1,1,1,'Page','2018-10-11 10:04:27','2018-10-11 10:04:11','Inherit','Inherit','more-details-1','More Details 1',NULL,'<p>More Details 1 content</p>',NULL,NULL,1,1,2,0,0,NULL,2),(40,13,1,0,0,1,1,0,'Page','2018-10-11 10:04:40','2018-10-11 10:04:40','Inherit','Inherit','new-page','New Page',NULL,NULL,NULL,NULL,1,1,3,0,0,NULL,2),(41,13,2,0,0,1,1,0,'Page','2018-10-11 10:04:55','2018-10-11 10:04:40','Inherit','Inherit','more-details-2','More Details 2',NULL,'<p>More Details 2 content</p>',NULL,NULL,1,1,3,0,0,NULL,2),(42,13,3,1,0,1,1,1,'Page','2018-10-11 10:04:55','2018-10-11 10:04:40','Inherit','Inherit','more-details-2','More Details 2',NULL,'<p>More Details 2 content</p>',NULL,NULL,1,1,3,0,0,NULL,2),(43,8,4,0,0,1,1,0,'SilverStripe\\CMS\\Model\\SiteTree','2018-10-11 10:48:21','2018-10-10 17:24:03','Inherit','Inherit','articles','Articles',NULL,'<p>Primary text for \'Articles\' page</p>',NULL,NULL,1,1,6,0,0,NULL,0),(44,8,5,1,0,1,1,1,'SilverStripe\\CMS\\Model\\SiteTree','2018-10-11 10:48:21','2018-10-10 17:24:03','Inherit','Inherit','articles','Articles',NULL,'<p>Primary text for \'Articles\' page</p>',NULL,NULL,1,1,6,0,0,NULL,0),(45,8,6,0,0,1,1,0,'ArticleHolder','2018-10-11 10:48:43','2018-10-10 17:24:03','Inherit','Inherit','articles','Articles',NULL,'<p>Primary text for \'Articles\' page</p>',NULL,NULL,1,1,6,0,0,NULL,0),(46,8,7,1,0,1,1,1,'ArticleHolder','2018-10-11 10:48:43','2018-10-10 17:24:03','Inherit','Inherit','articles','Articles',NULL,'<p>Primary text for \'Articles\' page</p>',NULL,NULL,1,1,6,0,0,NULL,0),(47,11,6,1,1,1,1,1,'SilverStripe\\CMS\\Model\\SiteTree','2018-10-11 10:48:55','2018-10-10 17:26:33','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(48,10,6,1,1,1,1,1,'SilverStripe\\CMS\\Model\\SiteTree','2018-10-11 10:49:05','2018-10-10 17:25:55','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(49,9,6,0,0,1,1,0,'ArticlePage','2018-10-11 10:49:48','2018-10-10 17:25:14','Inherit','Inherit','gallery-item-1','Gallery Item 1',NULL,'<p>Gallery Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(50,9,7,1,0,1,1,1,'ArticlePage','2018-10-11 10:49:48','2018-10-10 17:25:14','Inherit','Inherit','gallery-item-1','Gallery Item 1',NULL,'<p>Gallery Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(51,9,8,0,0,1,1,0,'ArticlePage','2018-10-11 10:50:35','2018-10-10 17:25:14','Inherit','Inherit','article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(52,9,9,1,0,1,1,1,'ArticlePage','2018-10-11 10:50:35','2018-10-10 17:25:14','Inherit','Inherit','article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(53,8,8,0,0,1,1,0,'ArticleHolder','2018-10-11 11:23:47','2018-10-10 17:24:03','Inherit','Inherit','articles','Articles',NULL,'<p>Primary text for \'Articles\' page</p>',NULL,NULL,1,1,3,0,0,NULL,0),(54,9,10,0,0,1,1,0,'ArticlePage','2018-10-11 11:35:37','2018-10-10 17:25:14','Inherit','Inherit','article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(55,9,11,1,0,1,1,1,'ArticlePage','2018-10-11 11:35:37','2018-10-10 17:25:14','Inherit','Inherit','article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(56,9,12,0,0,1,1,0,'ArticlePage','2018-10-11 15:24:02','2018-10-10 17:25:14','Inherit','Inherit','article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(57,9,13,1,0,1,1,1,'ArticlePage','2018-10-11 15:24:02','2018-10-10 17:25:14','Inherit','Inherit','article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(58,8,9,1,0,1,1,1,'ArticleHolder','2018-10-11 15:24:35','2018-10-10 17:24:03','Inherit','Inherit','articles','Articles',NULL,'<p>Primary text for \'Articles\' page</p>',NULL,NULL,1,1,3,0,0,NULL,0),(59,9,14,0,0,1,1,0,'ArticlePage','2018-10-11 15:31:47','2018-10-10 17:25:14','Inherit','Inherit','article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(60,9,15,1,0,1,1,1,'ArticlePage','2018-10-11 15:31:47','2018-10-10 17:25:14','Inherit','Inherit','article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(61,9,16,0,0,1,1,0,'ArticlePage','2018-10-11 15:48:39','2018-10-10 17:25:14','Inherit','Inherit','article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(62,9,17,1,0,1,1,1,'ArticlePage','2018-10-11 15:48:39','2018-10-10 17:25:14','Inherit','Inherit','article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(63,14,1,0,0,1,1,0,'ArticlePage','2018-10-11 16:18:59','2018-10-11 16:18:59','Inherit','Inherit','article-item-2','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(64,14,2,0,0,1,1,0,'ArticlePage','2018-10-11 16:19:16','2018-10-11 16:18:59','Inherit','Inherit','article-item-2','Article Item 1','Article Item 2','<p>Article Item 2 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(65,14,3,1,0,1,1,1,'ArticlePage','2018-10-11 16:19:16','2018-10-11 16:18:59','Inherit','Inherit','article-item-2','Article Item 1','Article Item 2','<p>Article Item 2 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(66,15,1,0,0,1,1,0,'ArticlePage','2018-10-11 16:19:25','2018-10-11 16:19:25','Inherit','Inherit','article-item-3','Article Item 1','Article Item 2','<p>Article Item 2 Content</p>',NULL,NULL,0,1,3,0,0,NULL,8),(67,15,2,0,0,1,1,0,'ArticlePage','2018-10-11 16:19:39','2018-10-11 16:19:25','Inherit','Inherit','article-item-3','Article Item 1','Article Item 3','<p>Article Item 3 Content</p>',NULL,NULL,0,1,3,0,0,NULL,8),(68,15,3,1,0,1,1,1,'ArticlePage','2018-10-11 16:19:39','2018-10-11 16:19:25','Inherit','Inherit','article-item-3','Article Item 1','Article Item 3','<p>Article Item 3 Content</p>',NULL,NULL,0,1,3,0,0,NULL,8),(69,16,1,0,0,1,1,0,'ArticlePage','2018-10-11 16:19:48','2018-10-11 16:19:48','Inherit','Inherit','article-item-4','Article Item 1','Article Item 3','<p>Article Item 3 Content</p>',NULL,NULL,0,1,4,0,0,NULL,8),(70,16,2,0,0,1,1,0,'ArticlePage','2018-10-11 16:20:00','2018-10-11 16:19:48','Inherit','Inherit','article-item-4','Article Item 1','Article Item 4','<p>Article Item 4 Content</p>',NULL,NULL,0,1,4,0,0,NULL,8),(71,16,3,1,0,1,1,1,'ArticlePage','2018-10-11 16:20:00','2018-10-11 16:19:48','Inherit','Inherit','article-item-4','Article Item 1','Article Item 4','<p>Article Item 4 Content</p>',NULL,NULL,0,1,4,0,0,NULL,8),(72,14,4,0,0,1,1,0,'ArticlePage','2018-10-11 16:26:12','2018-10-11 16:18:59','Inherit','Inherit','article-item-2','Article Item 2',NULL,'<p>Article Item 2 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(73,14,5,1,0,1,1,1,'ArticlePage','2018-10-11 16:26:12','2018-10-11 16:18:59','Inherit','Inherit','article-item-2','Article Item 2',NULL,'<p>Article Item 2 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(74,15,4,0,0,1,1,0,'ArticlePage','2018-10-11 16:26:22','2018-10-11 16:19:25','Inherit','Inherit','article-item-3','Article Item 3',NULL,'<p>Article Item 3 Content</p>',NULL,NULL,0,1,3,0,0,NULL,8),(75,15,5,1,0,1,1,1,'ArticlePage','2018-10-11 16:26:22','2018-10-11 16:19:25','Inherit','Inherit','article-item-3','Article Item 3',NULL,'<p>Article Item 3 Content</p>',NULL,NULL,0,1,3,0,0,NULL,8),(76,16,4,0,0,1,1,0,'ArticlePage','2018-10-11 16:26:34','2018-10-11 16:19:48','Inherit','Inherit','article-item-4','Article Item 4',NULL,'<p>Article Item 4 Content</p>',NULL,NULL,0,1,4,0,0,NULL,8),(77,16,5,1,0,1,1,1,'ArticlePage','2018-10-11 16:26:34','2018-10-11 16:19:48','Inherit','Inherit','article-item-4','Article Item 4',NULL,'<p>Article Item 4 Content</p>',NULL,NULL,0,1,4,0,0,NULL,8),(78,14,6,0,0,1,1,0,'ArticlePage','2018-10-11 16:42:30','2018-10-11 16:18:59','Inherit','Inherit','article-item-2','Article Item 2',NULL,'<p>Article Item 2 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(79,14,7,1,0,1,1,1,'ArticlePage','2018-10-11 16:42:30','2018-10-11 16:18:59','Inherit','Inherit','article-item-2','Article Item 2',NULL,'<p>Article Item 2 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(80,15,6,0,0,1,1,0,'ArticlePage','2018-10-11 16:42:46','2018-10-11 16:19:25','Inherit','Inherit','article-item-3','Article Item 3',NULL,'<p>Article Item 3 Content</p>',NULL,NULL,0,1,3,0,0,NULL,8),(81,15,7,1,0,1,1,1,'ArticlePage','2018-10-11 16:42:46','2018-10-11 16:19:25','Inherit','Inherit','article-item-3','Article Item 3',NULL,'<p>Article Item 3 Content</p>',NULL,NULL,0,1,3,0,0,NULL,8),(82,16,6,0,0,1,1,0,'ArticlePage','2018-10-11 16:43:03','2018-10-11 16:19:48','Inherit','Inherit','article-item-4','Article Item 4',NULL,'<p>Article Item 4 Content</p>',NULL,NULL,0,1,4,0,0,NULL,8),(83,16,7,1,0,1,1,1,'ArticlePage','2018-10-11 16:43:03','2018-10-11 16:19:48','Inherit','Inherit','article-item-4','Article Item 4',NULL,'<p>Article Item 4 Content</p>',NULL,NULL,0,1,4,0,0,NULL,8),(84,16,8,0,0,1,1,0,'ArticlePage','2018-10-11 16:57:48','2018-10-11 16:19:48','Inherit','Inherit','article-item-4','Article Item 4',NULL,'<p>Article Item 4 Content</p>',NULL,NULL,0,1,4,0,0,NULL,8),(85,16,9,1,0,1,1,1,'ArticlePage','2018-10-11 16:57:48','2018-10-11 16:19:48','Inherit','Inherit','article-item-4','Article Item 4',NULL,'<p>Article Item 4 Content</p>',NULL,NULL,0,1,4,0,0,NULL,8),(86,14,8,0,0,1,1,0,'ArticlePage','2018-10-11 16:58:00','2018-10-11 16:18:59','Inherit','Inherit','article-item-2','Article Item 2',NULL,'<p>Article Item 2 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(87,14,9,1,0,1,1,1,'ArticlePage','2018-10-11 16:58:00','2018-10-11 16:18:59','Inherit','Inherit','article-item-2','Article Item 2',NULL,'<p>Article Item 2 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(88,17,1,0,0,1,1,0,'','2018-10-12 10:47:21','2018-10-12 10:47:21','Inherit','Inherit','new-regions-page','New Regions Page',NULL,NULL,NULL,NULL,1,1,6,0,0,NULL,0),(89,17,2,0,0,1,1,0,'','2018-10-12 10:47:47','2018-10-12 10:47:21','Inherit','Inherit','regions','Regions',NULL,'<p>Regions default content area</p>',NULL,NULL,1,1,6,0,0,NULL,0),(90,17,3,1,0,1,1,1,'SilverStripe\\CMS\\Model\\SiteTree','2018-10-12 10:47:47','2018-10-12 10:47:21','Inherit','Inherit','regions','Regions',NULL,'<p>Regions default content area</p>',NULL,NULL,1,1,6,0,0,NULL,0),(91,17,4,0,0,1,1,0,'RegionsPage','2018-10-12 10:50:07','2018-10-12 10:47:21','Inherit','Inherit','regions','Regions',NULL,'<p>Regions default content area</p>',NULL,NULL,1,1,6,0,0,NULL,0),(92,17,5,1,0,1,1,1,'RegionsPage','2018-10-12 10:50:07','2018-10-12 10:47:21','Inherit','Inherit','regions','Regions',NULL,'<p>Regions default content area</p>',NULL,NULL,1,1,6,0,0,NULL,0),(93,1,5,0,0,1,1,0,'HomePage','2018-12-06 14:25:19','2018-10-10 15:08:06','Inherit','Inherit','home','Home',NULL,'<h1>Lorem ipsum</h1><p>dolor sit amet, consectetur adipiscing elit. Sed et hendrerit libero. Praesent et ex mattis, viverra risus eu, fringilla orci. Nullam egestas felis sollicitudin, tristique sapien in, malesuada dui. Duis pulvinar magna a tempor pharetra. Etiam id urna urna. Ut vestibulum convallis hendrerit. Sed tincidunt, orci eu egestas dignissim, nunc neque fringilla diam, ac dapibus nunc nulla in nisl. Proin dui enim, pharetra non pellentesque et, ultricies in leo. Phasellus ac semper est, at porta sem. Integer in lobortis velit, ut interdum purus. Proin consectetur neque sed neque faucibus, ut scelerisque nulla maximus. Mauris libero odio, fringilla consequat tristique id, vulputate a nisl.</p><p>Donec posuere, neque iaculis posuere eleifend, odio erat feugiat metus, sed maximus nisl tortor sed dolor. In hac habitasse platea dictumst. Nam velit tortor, iaculis id purus in, sollicitudin tempor ligula. Morbi lacinia lobortis metus. Proin suscipit orci quis libero vestibulum interdum. Etiam aliquet euismod est non sollicitudin. Pellentesque non egestas lectus. Curabitur sollicitudin risus id mauris malesuada, eget molestie mi euismod. Donec sem velit, aliquam non hendrerit eget, auctor eget lectus. Praesent aliquet rutrum augue, ut ultrices lacus sagittis ac. Donec et pharetra massa. Nam ac neque vehicula, volutpat tellus sed, ultricies erat. Duis fermentum mattis erat vel feugiat. Quisque sollicitudin urna sit amet massa egestas pellentesque. Cras mattis dapibus euismod. Nulla facilisi.</p>',NULL,NULL,1,1,1,0,0,NULL,0),(94,1,6,1,0,1,1,1,'HomePage','2018-12-06 14:25:19','2018-10-10 15:08:06','Inherit','Inherit','home','Home',NULL,'<h1>Lorem ipsum</h1><p>dolor sit amet, consectetur adipiscing elit. Sed et hendrerit libero. Praesent et ex mattis, viverra risus eu, fringilla orci. Nullam egestas felis sollicitudin, tristique sapien in, malesuada dui. Duis pulvinar magna a tempor pharetra. Etiam id urna urna. Ut vestibulum convallis hendrerit. Sed tincidunt, orci eu egestas dignissim, nunc neque fringilla diam, ac dapibus nunc nulla in nisl. Proin dui enim, pharetra non pellentesque et, ultricies in leo. Phasellus ac semper est, at porta sem. Integer in lobortis velit, ut interdum purus. Proin consectetur neque sed neque faucibus, ut scelerisque nulla maximus. Mauris libero odio, fringilla consequat tristique id, vulputate a nisl.</p><p>Donec posuere, neque iaculis posuere eleifend, odio erat feugiat metus, sed maximus nisl tortor sed dolor. In hac habitasse platea dictumst. Nam velit tortor, iaculis id purus in, sollicitudin tempor ligula. Morbi lacinia lobortis metus. Proin suscipit orci quis libero vestibulum interdum. Etiam aliquet euismod est non sollicitudin. Pellentesque non egestas lectus. Curabitur sollicitudin risus id mauris malesuada, eget molestie mi euismod. Donec sem velit, aliquam non hendrerit eget, auctor eget lectus. Praesent aliquet rutrum augue, ut ultrices lacus sagittis ac. Donec et pharetra massa. Nam ac neque vehicula, volutpat tellus sed, ultricies erat. Duis fermentum mattis erat vel feugiat. Quisque sollicitudin urna sit amet massa egestas pellentesque. Cras mattis dapibus euismod. Nulla facilisi.</p>',NULL,NULL,1,1,1,0,0,NULL,0),(95,2,3,0,0,1,1,0,'Page','2018-12-06 14:25:48','2018-10-10 15:08:06','Inherit','Inherit','about-us','About Us',NULL,'<p>usce dapibus leo in lobortis auctor. Quisque molestie vel dolor in venenatis. Sed tristique tristique neque eu fermentum. Maecenas a erat accumsan, hendrerit augue quis, iaculis nulla. Aenean in diam mi. Nam quis dui a lectus ultrices cursus et ac ipsum. Mauris bibendum turpis nisi, vel aliquet purus pharetra nec. Sed non est urna. Vivamus justo nisl, luctus a ligula a, luctus feugiat velit. Ut viverra, augue id tempor imperdiet, purus arcu venenatis felis, eleifend volutpat ex dolor et neque. Etiam tincidunt convallis condimentum. Etiam nisi dolor, bibendum maximus volutpat sed, faucibus quis eros.</p><p>Donec gravida nibh quis tellus viverra finibus. Nulla facilisi. In vitae faucibus sapien, vel tincidunt ante. In hac habitasse platea dictumst. Suspendisse potenti. Donec fermentum lacus turpis, ac ornare metus malesuada sit amet. Ut pulvinar, massa ultricies euismod eleifend, dui ipsum commodo sapien, non posuere turpis justo eu nunc. Donec felis ipsum, mattis venenatis tempus quis, elementum non erat. Mauris a sem orci. Aenean eu mattis arcu. Fusce mollis fermentum neque ut accumsan.</p><p>Donec nec augue nibh. Integer non ultricies erat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In hac habitasse platea dictumst. Sed congue enim hendrerit accumsan consectetur. Ut quis augue sapien. Nulla ultricies risus at lorem ultricies dictum. Mauris volutpat quis sapien vitae imperdiet. Nam eu efficitur lectus. Aliquam velit eros, vulputate et facilisis id, ornare ut elit.</p>',NULL,NULL,1,1,2,0,0,NULL,0),(96,2,4,1,0,1,1,1,'Page','2018-12-06 14:25:48','2018-10-10 15:08:06','Inherit','Inherit','about-us','About Us',NULL,'<p>usce dapibus leo in lobortis auctor. Quisque molestie vel dolor in venenatis. Sed tristique tristique neque eu fermentum. Maecenas a erat accumsan, hendrerit augue quis, iaculis nulla. Aenean in diam mi. Nam quis dui a lectus ultrices cursus et ac ipsum. Mauris bibendum turpis nisi, vel aliquet purus pharetra nec. Sed non est urna. Vivamus justo nisl, luctus a ligula a, luctus feugiat velit. Ut viverra, augue id tempor imperdiet, purus arcu venenatis felis, eleifend volutpat ex dolor et neque. Etiam tincidunt convallis condimentum. Etiam nisi dolor, bibendum maximus volutpat sed, faucibus quis eros.</p><p>Donec gravida nibh quis tellus viverra finibus. Nulla facilisi. In vitae faucibus sapien, vel tincidunt ante. In hac habitasse platea dictumst. Suspendisse potenti. Donec fermentum lacus turpis, ac ornare metus malesuada sit amet. Ut pulvinar, massa ultricies euismod eleifend, dui ipsum commodo sapien, non posuere turpis justo eu nunc. Donec felis ipsum, mattis venenatis tempus quis, elementum non erat. Mauris a sem orci. Aenean eu mattis arcu. Fusce mollis fermentum neque ut accumsan.</p><p>Donec nec augue nibh. Integer non ultricies erat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In hac habitasse platea dictumst. Sed congue enim hendrerit accumsan consectetur. Ut quis augue sapien. Nulla ultricies risus at lorem ultricies dictum. Mauris volutpat quis sapien vitae imperdiet. Nam eu efficitur lectus. Aliquam velit eros, vulputate et facilisis id, ornare ut elit.</p>',NULL,NULL,1,1,2,0,0,NULL,0),(97,18,1,0,0,1,1,0,'PropertySearchPage','2019-01-17 10:35:58','2019-01-17 10:35:58','Inherit','Inherit','new-property-search-page','New Property Search Page',NULL,NULL,NULL,NULL,1,1,7,0,0,NULL,0),(98,18,2,0,0,1,1,0,'PropertySearchPage','2019-01-17 10:36:18','2019-01-17 10:35:58','Inherit','Inherit','find-a-rental','Find a Rental',NULL,'<p>Find a Rental content</p>',NULL,NULL,1,1,7,0,0,NULL,0),(99,18,3,1,0,1,1,1,'PropertySearchPage','2019-01-17 10:36:18','2019-01-17 10:35:58','Inherit','Inherit','find-a-rental','Find a Rental',NULL,'<p>Find a Rental content</p>',NULL,NULL,1,1,7,0,0,NULL,0),(100,19,1,0,0,1,1,0,'Page','2019-01-18 10:24:00','2019-01-18 10:24:00','Inherit','Inherit','new-page','New Page',NULL,NULL,NULL,NULL,1,1,8,0,0,NULL,0),(101,19,2,0,0,1,1,0,'Page','2019-01-18 10:24:24','2019-01-18 10:24:00','Inherit','Inherit','travel-guides','Travel Guides',NULL,'<p>Travel Guides $Content</p>',NULL,NULL,1,1,8,0,0,NULL,0),(102,19,3,1,0,1,1,1,'Page','2019-01-18 10:24:24','2019-01-18 10:24:00','Inherit','Inherit','travel-guides','Travel Guides',NULL,'<p>Travel Guides $Content</p>',NULL,NULL,1,1,8,0,0,NULL,0),(103,19,4,1,1,1,1,1,'Page','2019-01-18 10:29:43','2019-01-18 10:24:00','Inherit','Inherit',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,NULL,0),(104,8,10,0,0,1,1,0,'ArticleHolder','2019-01-18 10:30:14','2018-10-10 17:24:03','Inherit','Inherit','travel-guides','Travel Guides',NULL,'<p>Primary text for \'Travel Guides\' page</p>',NULL,NULL,1,1,3,0,0,NULL,0),(105,8,11,1,0,1,1,1,'ArticleHolder','2019-01-18 10:30:15','2018-10-10 17:24:03','Inherit','Inherit','travel-guides','Travel Guides',NULL,'<p>Primary text for \'Travel Guides\' page</p>',NULL,NULL,1,1,3,0,0,NULL,0),(106,9,18,0,0,1,1,0,'ArticlePage','2019-01-18 14:14:14','2018-10-10 17:25:14','Inherit','Inherit','article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(107,9,19,1,0,1,1,1,'ArticlePage','2019-01-18 14:14:15','2018-10-10 17:25:14','Inherit','Inherit','article-item-1','Article Item 1',NULL,'<p>Article Item 1 Content</p>',NULL,NULL,0,1,1,0,0,NULL,8),(108,14,10,0,0,1,1,0,'ArticlePage','2019-01-18 14:14:30','2018-10-11 16:18:59','Inherit','Inherit','article-item-2','Article Item 2',NULL,'<p>Article Item 2 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8),(109,14,11,1,0,1,1,1,'ArticlePage','2019-01-18 14:14:31','2018-10-11 16:18:59','Inherit','Inherit','article-item-2','Article Item 2',NULL,'<p>Article Item 2 Content</p>',NULL,NULL,0,1,2,0,0,NULL,8);
/*!40000 ALTER TABLE `SiteTree_Versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteTree_ViewerGroups`
--

DROP TABLE IF EXISTS `SiteTree_ViewerGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteTree_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteTree_ViewerGroups`
--

LOCK TABLES `SiteTree_ViewerGroups` WRITE;
/*!40000 ALTER TABLE `SiteTree_ViewerGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `SiteTree_ViewerGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VirtualPage`
--

DROP TABLE IF EXISTS `VirtualPage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VirtualPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VirtualPage`
--

LOCK TABLES `VirtualPage` WRITE;
/*!40000 ALTER TABLE `VirtualPage` DISABLE KEYS */;
/*!40000 ALTER TABLE `VirtualPage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VirtualPage_Live`
--

DROP TABLE IF EXISTS `VirtualPage_Live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VirtualPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VirtualPage_Live`
--

LOCK TABLES `VirtualPage_Live` WRITE;
/*!40000 ALTER TABLE `VirtualPage_Live` DISABLE KEYS */;
/*!40000 ALTER TABLE `VirtualPage_Live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VirtualPage_Versions`
--

DROP TABLE IF EXISTS `VirtualPage_Versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VirtualPage_Versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VirtualPage_Versions`
--

LOCK TABLES `VirtualPage_Versions` WRITE;
/*!40000 ALTER TABLE `VirtualPage_Versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `VirtualPage_Versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-18  1:16:19
